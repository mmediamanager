/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include "mm-gtk-application-store.h"

#include "libmmanager/mm-type-builtins.h"
#include "libmmanager/mm-types.h"
#include "libmmanager/mm-application.h"
#include "libmmanager/mm-manager.h"
#include "libmmanager/mm-category.h"
#include <gtk/gtk.h>
#include <glib/gi18n.h>

/* our parent's model iface */
static GtkTreeModelIface parent_iface = { 0, };
static void mm_gtk_application_store_tree_model_iface_init (GtkTreeModelIface *iface);
static void value_set_gicon_from_mm_type (GValue *val, MMApplicationType type);

G_DEFINE_TYPE_EXTENDED (MMGtkApplicationStore, mm_gtk_application_store,
                        GTK_TYPE_TREE_STORE, 0,
                        G_IMPLEMENT_INTERFACE (GTK_TYPE_TREE_MODEL,
                                               mm_gtk_application_store_tree_model_iface_init));

static void
mm_gtk_application_store_finalize (GObject *object)
{
  if (G_OBJECT_CLASS (mm_gtk_application_store_parent_class)->finalize)
    G_OBJECT_CLASS (mm_gtk_application_store_parent_class)->finalize (object);
}

static void
mm_gtk_application_store_class_init (MMGtkApplicationStoreClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mm_gtk_application_store_finalize;
}

static void
insert_categories_for_app (MMGtkApplicationStore *self,
                           GtkTreeIter iter,
                           MMApplication *app)
{
  GList *categories, *l;
  GError *error = NULL;
  GtkTreeIter child;
  
  /* at the third level of the tree we insert all the categories */
  categories = mm_application_get_categories (app, &error);
  if (error) {
    g_critical ("MM-GTK: failed to build the ApplicationStore: %s",
                error->message);
    return;
  }
  for (l = categories; l; l = l->next) {
    gtk_tree_store_append (GTK_TREE_STORE (self), &child, &iter);
    gtk_tree_store_set (GTK_TREE_STORE (self), &child, MM_GTK_APP_STORE_CAT_COLUMN,
                        l->data, -1);
  }
}

static void
insert_applications_for_type (MMGtkApplicationStore *self,
                              GtkTreeIter iter,
                              MMManager *manager,
                              MMApplicationType type)
{
  GList *applications, *l;
  GtkTreeIter child;

  /* at the second level of the tree we insert all the applications */
  applications = mm_manager_get_application_list_for_type (manager, type);
  for (l = applications; l; l = l->next) {
    gtk_tree_store_append (GTK_TREE_STORE (self), &child, &iter);
    gtk_tree_store_set (GTK_TREE_STORE (self), &child, MM_GTK_APP_STORE_APP_COLUMN,
                        l->data, -1);
    insert_categories_for_app (self, child, l->data);
  }
}

static void
populate_store (MMGtkApplicationStore *self)
{
  MMManager *manager;
  GtkTreeIter iter;
  MMApplicationType app_types[3] = {
    MM_APPLICATION_MUSIC,
    MM_APPLICATION_PHOTO,
    MM_APPLICATION_VIDEO
  };
  int idx;

  manager = mm_manager_get ();
  /* at the first level, we insert all the types */
  for (idx = 0; idx < 3; idx++) {
    gtk_tree_store_append (GTK_TREE_STORE (self), &iter, NULL);
    gtk_tree_store_set (GTK_TREE_STORE (self), &iter,
                        MM_GTK_APP_STORE_TYPE_COLUMN, app_types[idx],
                        -1);
    insert_applications_for_type (self, iter, manager, app_types[idx]);
  }
}

static void
mm_gtk_application_store_init (MMGtkApplicationStore *self)
{
  GType types [] = { MM_TYPE_MAPPLICATION_TYPE,
                     MM_TYPE_APPLICATION,
                     MM_TYPE_CATEGORY
  };

  gtk_tree_store_set_column_types (GTK_TREE_STORE (self), 3, types);
  populate_store (self);
}

/* retreive an object from our parent's data storage,
 * unset the value when done. */
static GValue *
mm_gtk_application_store_get_object (MMGtkApplicationStore *self, GtkTreeIter *iter,
                                     gint column)
{
  GValue *value = g_new0 (GValue, 1);

  /* validate our parameters */
  g_return_val_if_fail (MM_GTK_IS_APPLICATION_STORE (self), NULL);
  g_return_val_if_fail (iter != NULL, NULL);

  /* retreive the object using our parent's interface */
  parent_iface.get_value (GTK_TREE_MODEL (self), iter, column, value);

  return value;
}

static GType
impl_get_column_type (GtkTreeModel *self, gint column)
{
  GType types [] = {
    MM_TYPE_MAPPLICATION_TYPE,    
    MM_TYPE_APPLICATION,
    MM_TYPE_CATEGORY,
    G_TYPE_ICON,
    G_TYPE_STRING
  };

  g_return_val_if_fail (MM_GTK_IS_APPLICATION_STORE (self), G_TYPE_INVALID);
  g_return_val_if_fail (column >= 0 && column < MM_GTK_APP_STORE_N_COLUMNS, G_TYPE_INVALID);

  return types[column];
}

static void
value_set_gicon_from_cat (GValue *val,
                          MMCategory *cat)
{
  GIcon *icon;

  icon = mm_category_get_icon (cat);
  if (!icon || !G_IS_ICON (icon)) {
    /* fallback to a generic mimetype icon */

    MMApplicationType type;
    MMApplication *application;

    application = mm_category_get_application (cat);
    g_object_get (application, "supported-type", &type, NULL);
    value_set_gicon_from_mm_type (val, type);
    g_object_unref (application);
  } else {
    g_value_take_object (val, icon);
  }
}

static void
get_value_for_category (GtkTreeModel *self, GtkTreeIter *iter,
                        gint column, GValue *value)
{
  MMCategory *cat;
  GValue *obj;
  GIcon *icon;

  obj = mm_gtk_application_store_get_object (MM_GTK_APPLICATION_STORE (self),
                                             iter, MM_GTK_APP_STORE_CAT_COLUMN);
  cat = MM_CATEGORY (g_value_dup_object (obj));
  g_value_unset (obj);

  switch (column) {
    case MM_GTK_APP_STORE_CAT_COLUMN:
      /* return the object itself */
      g_value_set_object (value, cat);
      break;
    case MM_GTK_APP_STORE_APP_COLUMN:
      g_value_take_object (value, mm_category_get_application (cat));
      break;
    case MM_GTK_APP_STORE_GICON_COLUMN:
      value_set_gicon_from_cat (value, cat);
      break;
    case MM_GTK_APP_STORE_NAME_COLUMN:
      g_value_set_string (value, mm_category_get_name (cat));
      break;
    case MM_GTK_APP_STORE_TYPE_COLUMN:
      g_critical ("MM-GTK: impossible to get this kind of value, something in the"
                  "view is wrong!");
      break;
    default:
      g_assert_not_reached ();
      break;
  }

  g_object_unref (cat);  
}

static void
value_set_gicon_from_app (GValue *val, MMApplication *app)
{
  GAppInfo *app_info;
  GIcon *icon;

  app_info = mm_application_get_app_info (app);
  icon = g_app_info_get_icon (app_info);
  if (!icon || !G_IS_ICON (icon)) {
    /* fallback to a default app icon */
    icon = g_themed_icon_new ("application-default-icon");
  }

  g_value_take_object (val, icon);

  g_object_unref (app_info);
}

static void
value_set_name_from_app (GValue *value, MMApplication *app)
{
  GAppInfo *app_info;

  app_info = mm_application_get_app_info (app);
  g_value_set_string (value, g_app_info_get_name (app_info));

  g_object_unref (app_info);
}

static void
value_set_supp_type_from_app (GValue *val, MMApplication *app)
{
  MMApplicationType supp_type;

  g_object_get (app, "supported-type", &supp_type, NULL);

  g_value_set_enum (val, supp_type);
}

static void
get_value_for_application (GtkTreeModel *self, GtkTreeIter *iter,
                           gint column, GValue *value)
{
  MMApplication *app;
  GValue *obj;

  obj = mm_gtk_application_store_get_object (MM_GTK_APPLICATION_STORE (self),
                                             iter, MM_GTK_APP_STORE_APP_COLUMN);
  app = MM_APPLICATION (g_value_dup_object (obj));
  g_value_unset (obj);

  switch (column) {
    case MM_GTK_APP_STORE_APP_COLUMN:
      /* return the object itself */
      g_value_set_object (value, app);
      break;
    case MM_GTK_APP_STORE_GICON_COLUMN:
      value_set_gicon_from_app (value, app);
      break;
    case MM_GTK_APP_STORE_NAME_COLUMN:
      value_set_name_from_app (value, app);
      break;
    case MM_GTK_APP_STORE_TYPE_COLUMN:
      value_set_supp_type_from_app (value, app);
      break;
    case MM_GTK_APP_STORE_CAT_COLUMN:
      g_critical ("MM-GTK: impossible to get this kind of value, something in the"
                  "view is wrong!");
    default:
      g_assert_not_reached ();
      break;
  }

  g_object_unref (app);  
}

static void
value_set_name_from_mm_type (GValue *val,
                             MMApplicationType type)
{
  static const char *messages[] = {
    N_("Photos"),
    N_("Music"),
    N_("Videos")
  };

  g_value_set_string (val, messages[type-1]);
}

static void
value_set_gicon_from_mm_type (GValue *val,
                              MMApplicationType type)
{
  GIcon *icon;

  switch (type) {
    case MM_APPLICATION_MUSIC:
      icon = g_themed_icon_new ("audio-x-generic");
      break;
    case MM_APPLICATION_VIDEO:
      icon = g_themed_icon_new ("video-x-generic");
      break;
    case MM_APPLICATION_PHOTO:
      icon = g_themed_icon_new ("image-x-generic");
      break;
    default:
      g_assert_not_reached ();
      break;
  }

  g_value_take_object (val, icon);
}

static void
get_value_for_mm_type (GtkTreeModel *self, GtkTreeIter *iter,
                       gint column, GValue *value)
{
  MMApplicationType type;
  GValue *obj;

  obj = mm_gtk_application_store_get_object (MM_GTK_APPLICATION_STORE (self),
                                             iter, MM_GTK_APP_STORE_TYPE_COLUMN);
  type = g_value_get_enum (obj);

  switch (column) {
    case MM_GTK_APP_STORE_TYPE_COLUMN:
      /* return the value itself */
      g_value_copy (obj, value);
      break;
    case MM_GTK_APP_STORE_GICON_COLUMN:
      value_set_gicon_from_mm_type (value, type);
      break;
    case MM_GTK_APP_STORE_NAME_COLUMN:
      value_set_name_from_mm_type (value, type);
      break;
    case MM_GTK_APP_STORE_CAT_COLUMN:
    case MM_GTK_APP_STORE_APP_COLUMN:
      g_critical ("MM-GTK: impossible to get this kind of value, something in the"
                  "view is wrong!");
      break;
    default:
      g_assert_not_reached ();
      break;
  }

  g_value_unset (obj);
}

static void
impl_get_value (GtkTreeModel *self, GtkTreeIter *iter,
                gint column, GValue *value)
{
  int depth;

  g_return_if_fail (MM_GTK_IS_APPLICATION_STORE (self));
  g_return_if_fail (iter != NULL);
  g_return_if_fail (column >= 0 && column < MM_GTK_APP_STORE_N_COLUMNS);
  g_return_if_fail (value != NULL);

  value = g_value_init (value, impl_get_column_type (self, column));
  depth = gtk_tree_store_iter_depth (GTK_TREE_STORE (self), iter);
  if (depth == 0) {
    get_value_for_mm_type (self, iter, column, value);
  } else if (depth == 1) {
    get_value_for_application (self, iter, column, value);
  } else if (depth == 2) {
    get_value_for_category (self, iter, column, value);
  } else {
    g_assert_not_reached ();
  }
}

static int
impl_get_n_columns (GtkTreeModel *self)
{
  g_return_val_if_fail (MM_GTK_IS_APPLICATION_STORE (self), 0);

  /* we have four columns:
   * - media type
   * - application
   * - category
   * - gicon
   * - name
   */
  return MM_GTK_APP_STORE_N_COLUMNS;
}

static void
mm_gtk_application_store_tree_model_iface_init (GtkTreeModelIface *iface)
{
  /* save a copy of our parent's iface */
  parent_iface = *iface;

  /* override the iface methods */
  iface->get_n_columns = impl_get_n_columns;
  iface->get_column_type = impl_get_column_type;
  iface->get_value = impl_get_value;
}

/* public methods */

/**
 * mm_gtk_application_store_new:
 *
 * Builds a #GtkTreeStore that hierarchically groups all the #MMApplicationType,
 * #MMApplication and #MMCategory objects in a #GtkTreeModel fashion.
 *
 * Return value: an already populated #MMGtkApplicationStore.
 */

MMGtkApplicationStore*
mm_gtk_application_store_new (void)
{
  return g_object_new (MM_GTK_TYPE_APPLICATION_STORE, NULL);
}
