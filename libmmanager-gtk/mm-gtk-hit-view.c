/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "mm-gtk-hit-view.h"
#include "mm-gtk-hit-store.h"

#include "libmmanager/mm-hit-collection.h"

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>

G_DEFINE_TYPE (MMGtkHitView, mm_gtk_hit_view, GTK_TYPE_TREE_VIEW)

#define MM_GTK_HIT_VIEW_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_GTK_TYPE_HIT_VIEW, MMGtkHitViewPrivate))

struct _MMGtkHitViewPrivate {
  MMGtkHitStore *hit_store;
  MMHitCollection *collection;
};

static void
mm_gtk_hit_view_class_init (MMGtkHitViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (MMGtkHitViewPrivate));
}

static void
mm_gtk_hit_view_init (MMGtkHitView *self)
{
  MMGtkHitViewPrivate *details = self->details = MM_GTK_HIT_VIEW_GET_PRIVATE (self);
  GtkTreeViewColumn *column;
  GtkCellRenderer *pix_renderer;
  GtkCellRenderer *text_renderer;

  details->hit_store = mm_gtk_hit_store_new ();
  gtk_tree_view_set_model (GTK_TREE_VIEW (self), GTK_TREE_MODEL (details->hit_store));

  pix_renderer = gtk_cell_renderer_pixbuf_new ();
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("File"));
  gtk_tree_view_column_pack_start (column, pix_renderer, FALSE);
  gtk_tree_view_column_set_attributes (column, pix_renderer,
                                       "gicon", MM_GTK_HIT_STORE_ICON_COL,
                                       NULL);
  text_renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, text_renderer, TRUE);
  gtk_tree_view_column_set_attributes (column, text_renderer,
                                       "text", MM_GTK_HIT_STORE_NAME_COL,
                                       NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (self), column);
}

static void
set_collection (MMGtkHitView *self,
                MMHitCollection *collection)
{
  mm_gtk_hit_store_set_collection (self->details->hit_store,
                                   collection);
}

/* public methods */

/**
 * mm_gtk_hit_view_new:
 * @collection: a #MMHitCollection.
 *
 * Builds a #GtkTreeView that shows a #MMHitCollection.
 *
 * Return value: a #MMGtkHitView.
 */

MMGtkHitView*
mm_gtk_hit_view_new (MMHitCollection *collection)
{
  MMGtkHitView *self;

  self = MM_GTK_HIT_VIEW (g_object_new (MM_GTK_TYPE_HIT_VIEW, NULL));
  set_collection (self, collection);

  return self;
}
