/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "mm-gtk-application-view.h"
#include "mm-gtk-application-store.h"

#include "libmmanager/mm-application.h"
#include "libmmanager/mm-category.h"
#include "libmmanager/mm-type-builtins.h"

#include <gtk/gtk.h>

#define MM_GTK_APPLICATION_VIEW_GET_PRIVATE(o) \
    (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_GTK_TYPE_APPLICATION_VIEW, MMGtkApplicationViewPrivate))

struct _MMGtkApplicationViewPrivate {
  MMGtkApplicationStore *store;
};

enum {
  APPLICATION_SELECTED,
  CATEGORY_SELECTED,
  MMTYPE_SELECTED,
  LAST_SIGNAL
};

static guint view_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (MMGtkApplicationView, mm_gtk_application_view, GTK_TYPE_TREE_VIEW);

static void
view_selection_changed_cb (GtkTreeSelection *selection,
                           MMGtkApplicationView *view)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  GValue val = { 0, };
  int depth;

  gtk_tree_selection_get_selected (selection, &model, &iter);
  depth = gtk_tree_store_iter_depth (GTK_TREE_STORE (model),
                                     &iter);
  if (depth == 0) {
    MMApplicationType type;

    /* we're selecting an application type */
    gtk_tree_model_get_value (model, &iter,
                              MM_GTK_APP_STORE_TYPE_COLUMN,
                              &val);
    type = g_value_get_enum (&val);

    g_signal_emit (view, view_signals[MMTYPE_SELECTED],
                   0, type);
  } else if (depth == 1) {
    MMApplication *app;

    /* we're selecting an application */
    gtk_tree_model_get_value (model, &iter,
                              MM_GTK_APP_STORE_APP_COLUMN,
                              &val);
    app = g_value_dup_object (&val);
    g_signal_emit (view, view_signals[APPLICATION_SELECTED],
                   0, app);
  } else if (depth == 2) {
    MMCategory *cat;

    /* we're selecting a category */
    gtk_tree_model_get_value (model, &iter,
                              MM_GTK_APP_STORE_CAT_COLUMN,
                              &val);
    cat = g_value_dup_object (&val);
    g_signal_emit (view, view_signals[CATEGORY_SELECTED],
                   0, cat);
  }

  g_value_unset (&val);
}

static GObject *
impl_constructor (GType type, guint n_construct_properties,
                  GObjectConstructParam *construct_params)
{
  GObject *object;
  MMGtkApplicationView *view;
  MMGtkApplicationViewPrivate *details;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *col;
  GtkTreeSelection *selection;

  object = G_OBJECT_CLASS (mm_gtk_application_view_parent_class)->constructor (type, n_construct_properties,
                                                                               construct_params);
  view = MM_GTK_APPLICATION_VIEW (object);
  details = MM_GTK_APPLICATION_VIEW_GET_PRIVATE (view);

  details->store = mm_gtk_application_store_new ();
  gtk_tree_view_set_model (GTK_TREE_VIEW (view), GTK_TREE_MODEL (details->store));

  renderer = gtk_cell_renderer_pixbuf_new ();
  col = gtk_tree_view_column_new_with_attributes ("gicon",
                                                  renderer,
                                                  "gicon", MM_GTK_APP_STORE_GICON_COLUMN,
                                                  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (view), col);

  renderer = gtk_cell_renderer_text_new ();
  col = gtk_tree_view_column_new_with_attributes ("app-name",
                                                  renderer,
                                                  "text", MM_GTK_APP_STORE_NAME_COLUMN,
                                                  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (view), col);

  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (view), FALSE);
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
  gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);
  g_signal_connect_object (selection, "changed",
                           G_CALLBACK (view_selection_changed_cb), view, 0);

  return object;
}

static void
mm_gtk_application_view_class_init (MMGtkApplicationViewClass *klass)
{
  GObjectClass *oclass = G_OBJECT_CLASS (klass);
  oclass->constructor = impl_constructor;

  /**
   * MMGtkApplicationView::application-selected:
   * @view: the view.
   * @application: the application that was selected.
   *
   * Gets emitted when the user selects a #MMApplication inside the
   * treeview.
   */

  view_signals[APPLICATION_SELECTED] =
    g_signal_new ("application_selected",
                  G_OBJECT_CLASS_TYPE (oclass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (MMGtkApplicationViewClass, application_selected),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE,
                  1,
                  MM_TYPE_APPLICATION);

  /**
   * MMGtkApplicationView::category-selected:
   * @view: the view.
   * @category: the category that was selected.
   *
   * Gets emitted when the user selects a #MMCategory inside the treeview.
   */

  view_signals[CATEGORY_SELECTED] =
    g_signal_new ("category_selected",
                  G_OBJECT_CLASS_TYPE (oclass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (MMGtkApplicationViewClass, category_selected),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE,
                  1,
                  MM_TYPE_CATEGORY);

  /**
   * MMGtkApplicationView::mmtype-selected:
   * @view: the view.
   * @mm_type: the #MMApplicationType that was selected.
   *
   * Gets emitted when the user selects a #MMApplicationType inside the
   * treeview.
   */

  view_signals[MMTYPE_SELECTED] =
    g_signal_new ("mmtype_selected",
                  G_OBJECT_CLASS_TYPE (oclass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (MMGtkApplicationViewClass, mmtype_selected),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__ENUM,
                  G_TYPE_NONE,
                  1,
                  MM_TYPE_MAPPLICATION_TYPE);

  g_type_class_add_private (klass, sizeof (MMGtkApplicationViewPrivate));
}

static void
mm_gtk_application_view_init (MMGtkApplicationView *self)
{
  /* do nothing */
}

/* public methods */

/**
 * mm_gtk_application_view_new:
 *
 * Builds a #GtkTreeView containing all the information about
 * #MMApplication and #MMCategory objects registered in the library.
 *
 * Return value: an already filled ready-to-use #MMGtkApplicationView.
 */

MMGtkApplicationView*
mm_gtk_application_view_new (void)
{
  return g_object_new (MM_GTK_TYPE_APPLICATION_VIEW, NULL);
}
