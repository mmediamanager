/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_GTK_FILTER_BUILDER__
#define __MM_GTK_FILTER_BUILDER__

#include <glib-object.h>
#include <gtk/gtk.h>

#include "libmmanager/mm-category.h"

#define MM_GTK_TYPE_FILTER_BUILDER mm_gtk_filter_builder_get_type()
#define MM_GTK_FILTER_BUILDER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MM_GTK_TYPE_FILTER_BUILDER, MMGtkFilterBuilder))
#define MM_GTK_FILTER_BUILDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MM_GTK_TYPE_FILTER_BUILDER, MMGtkFilterBuilderClass))
#define MM_GTK_IS_FILTER_BUILDER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MM_GTK_TYPE_FILTER_BUILDER))
#define MM_GTK_IS_FILTER_BUILDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MM_GTK_TYPE_FILTER_BUILDER))
#define MM_GTK_FILTER_BUILDER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MM_GTK_TYPE_FILTER_BUILDER, MMGtkFilterBuilderClass))

/**
 * SECTION:mm-gtk-filter-builder
 * @short_description: widget to build #MMFilter<!-- -->s.
 *
 * #MMGtkFilterBuilder is a #GtkWidget that allow you to build #MMFilter objects
 * graphically. You need a #MMCategory to build it.
 */
 
/**
 * MMGtkFilterBuilder:
 *
 * A #GtkWidget that allows the user to build custom #MMFilter<!-- -->s.
 */

typedef struct _MMGtkFilterBuilder MMGtkFilterBuilder;
typedef struct _MMGtkFilterBuilderClass MMGtkFilterBuilderClass;
typedef struct _MMGtkFilterBuilderPrivate MMGtkFilterBuilderPrivate;

struct _MMGtkFilterBuilder {
  GtkVBox parent;
  MMGtkFilterBuilderPrivate *details;
};

struct _MMGtkFilterBuilderClass {
  GtkVBoxClass parent_class;
};

GType               mm_gtk_filter_builder_get_type   (void);
MMGtkFilterBuilder* mm_gtk_filter_builder_new        (MMCategory *category);
MMFilter*           mm_gtk_filter_builder_get_filter (MMGtkFilterBuilder *self);

#endif /* __MM_GTK_FILTER_BUILDER__ */
