/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_GTK_ATTRIBUTE_STORE_H__
#define __MM_GTK_ATTRIBUTE_STORE_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#include <libmmanager/mm-attribute.h>
#include <libmmanager/mm-attribute-manager.h>

#define MM_GTK_TYPE_ATTRIBUTE_STORE mm_gtk_attribute_store_get_type()
#define MM_GTK_ATTRIBUTE_STORE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MM_GTK_TYPE_ATTRIBUTE_STORE, MMGtkAttributeStore))
#define MM_GTK_ATTRIBUTE_STORE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MM_GTK_TYPE_ATTRIBUTE_STORE, MMGtkAttributeStoreClass))
#define MM_GTK_IS_ATTRIBUTE_STORE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MM_GTK_TYPE_ATTRIBUTE_STORE))
#define MM_GTK_IS_ATTRIBUTE_STORE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MM_GTK_TYPE_ATTRIBUTE_STORE))
#define MM_GTK_ATTRIBUTE_STORE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MM_GTK_TYPE_ATTRIBUTE_STORE, MMGtkAttributeStoreClass))

/**
 * SECTION:mm-gtk-attribute-store
 * @short_description: #GtkListStore that holds attributes.
 *
 * #MMGtkAttributeStore is a #GtkListStore that holds attributes, with one
 * column for each attribute property.
 */

/**
 * MMGtkAttributeStore:
 *
 * #GtkListStore implementation to store #MMAttribute structs.
 */

typedef struct _MMGtkAttributeStore MMGtkAttributeStore;
typedef struct _MMGtkAttributeStoreClass MMGtkAttributeStoreClass;

struct _MMGtkAttributeStore {
  GtkListStore parent;
};

struct _MMGtkAttributeStoreClass {
  GtkListStoreClass parent_class;
};

enum {
  MM_GTK_ATTR_STORE_ATTR_COL,
  MM_GTK_ATTR_STORE_ID_COL,
  MM_GTK_ATTR_STORE_NAME_COL,
  MM_GTK_ATTR_STORE_DESC_COL,
  MM_GTK_ATTR_STORE_GTYPE_COL,
  MM_GTK_ATTR_STORE_N_COL
};

GType                mm_gtk_attribute_store_get_type (void);
MMGtkAttributeStore* mm_gtk_attribute_store_new      (void);
void                 mm_gtk_attribute_store_add_attribute
                                           (MMGtkAttributeStore *self,
                                            MMAttribute         *attr);
void                 mm_gtk_attribute_store_set_from_attribute_manager
                                           (MMGtkAttributeStore *self,
                                            MMAttributeManager  *attr_manager);

#endif /* __MM_GTK_ATTRIBUTE_STORE__ */
