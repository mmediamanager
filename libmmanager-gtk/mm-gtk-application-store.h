/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_GTK_APPLICATION_STORE__
#define __MM_GTK_APPLICATION_STORE__

#include <glib-object.h>
#include <gtk/gtk.h>

#define MM_GTK_TYPE_APPLICATION_STORE mm_gtk_application_store_get_type()
#define MM_GTK_APPLICATION_STORE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MM_GTK_TYPE_APPLICATION_STORE, MMGtkApplicationStore))
#define MM_GTK_APPLICATION_STORE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MM_GTK_TYPE_APPLICATION_STORE, MMGtkApplicationStoreClass))
#define MM_GTK_IS_APPLICATION_STORE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MM_GTK_TYPE_APPLICATION_STORE))
#define MM_GTK_IS_APPLICATION_STORE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MM_GTK_TYPE_APPLICATION_STORE))
#define MM_GTK_APPLICATION_STORE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MM_GTK_TYPE_APPLICATION_STORE, MMGtkApplicationStoreClass))

/**
 * SECTION:mm-gtk-application-store
 * @short_description: a #GtkListStore to hold applications.
 *
 * #MMGtkApplicationStore is a #GtkTreeStore subclass, which holds
 * #MMApplicationType, #MMApplication and #MMCategory objects respectively
 * at depth 0, 1 and 2.
 */
 
/**
 * MMGtkApplicationStore:
 *
 * A #GtkTreeStore which holds libmmanager basic objects.
 */

typedef struct _MMGtkApplicationStore MMGtkApplicationStore;
typedef struct _MMGtkApplicationStoreClass MMGtkApplicationStoreClass;

struct _MMGtkApplicationStore {
  GtkTreeStore parent;
};

struct _MMGtkApplicationStoreClass {
  GtkTreeStoreClass parent_class;
};

enum {
  MM_GTK_APP_STORE_TYPE_COLUMN,
  MM_GTK_APP_STORE_APP_COLUMN,
  MM_GTK_APP_STORE_CAT_COLUMN,
  MM_GTK_APP_STORE_GICON_COLUMN,
  MM_GTK_APP_STORE_NAME_COLUMN,
  MM_GTK_APP_STORE_N_COLUMNS
};

GType                  mm_gtk_application_store_get_type (void);
MMGtkApplicationStore* mm_gtk_application_store_new      (void);

#endif /* _MM_GTK_APPLICATION_STORE */
