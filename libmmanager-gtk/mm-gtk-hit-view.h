/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_GTK_HIT_VIEW_H__
#define __MM_GTK_HIT_VIEW_H__

#include <libmmanager/mm-hit-collection.h>

#include <glib-object.h>
#include <gtk/gtk.h>

#define MM_GTK_TYPE_HIT_VIEW mm_gtk_hit_view_get_type()
#define MM_GTK_HIT_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MM_GTK_TYPE_HIT_VIEW, MMGtkHitView))
#define MM_GTK_HIT_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MM_GTK_TYPE_HIT_VIEW, MMGtkHitViewClass))
#define MM_GTK_IS_HIT_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MM_GTK_TYPE_HIT_VIEW))
#define MM_GTK_IS_HIT_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MM_GTK_TYPE_HIT_VIEW))
#define MM_GTK_HIT_VIEW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MM_GTK_TYPE_HIT_VIEW, MMGtkHitViewClass))

/**
 * SECTION:mm-gtk-hit-view
 * @short_description: displays a #MMGtkHitStore.
 *
 * #MMGtkHitView is a #GtkTreeView that can display in a widget a
 * #MMGtkHitStore.
 */
 
/**
 * MMGtkHitView:
 *
 * Displays a #MMGtkHitStore in a #GtkTreeView.
 */

typedef struct _MMGtkHitView MMGtkHitView;
typedef struct _MMGtkHitViewClass MMGtkHitViewClass;
typedef struct _MMGtkHitViewPrivate MMGtkHitViewPrivate;

struct _MMGtkHitView {
  GtkTreeView parent;
  MMGtkHitViewPrivate *details;
};

struct _MMGtkHitViewClass {
  GtkTreeViewClass parent_class;
};

GType         mm_gtk_hit_view_get_type (void);
MMGtkHitView* mm_gtk_hit_view_new      (MMHitCollection *collection);

#endif /* __MM_GTK_HIT_VIEW_H__ */
