/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include "mm-gtk-filter-builder.h"
#include "mm-gtk-attribute-store.h"

#include "libmmanager/mm-attribute-manager.h"
#include "libmmanager/mm-attribute-base-manager.h"
#include "libmmanager/mm-types.h"
#include "libmmanager/mm-string-utils.h"
#include "libmmanager/mm-filter-param.h"
#include "libmmanager/mm-attribute.h"

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gi18n.h>

G_DEFINE_TYPE (MMGtkFilterBuilder, mm_gtk_filter_builder, GTK_TYPE_VBOX);

#define MM_GTK_FILTER_BUILDER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_GTK_TYPE_FILTER_BUILDER, MMGtkFilterBuilderPrivate))

struct _MMGtkFilterBuilderPrivate {
  GtkListStore *filter_store;
  MMGtkAttributeStore *attribute_store;
  GtkWidget *filter_view;

  GHashTable *attr_managers;

  GtkWidget *cat_combo;
  GtkWidget *attr_combo;
  GtkWidget *logic_combo;
  GtkWidget *value_entry;
};

static const char * logic_strs [] = {
  N_("equal"),
  N_("greather than"),
  N_("less than"),
  N_("greater or equal than"),
  N_("less or equal than"),
};

static void
mm_gtk_filter_builder_finalize (GObject *obj)
{
  MMGtkFilterBuilder *self = MM_GTK_FILTER_BUILDER (obj);
  g_hash_table_destroy (self->details->attr_managers);

  G_OBJECT_CLASS (mm_gtk_filter_builder_parent_class)->finalize (obj);
}

static void
mm_gtk_filter_builder_class_init (MMGtkFilterBuilderClass *klass)
{
  GObjectClass *oclass = G_OBJECT_CLASS (klass);

  oclass->finalize = mm_gtk_filter_builder_finalize;

  g_type_class_add_private (klass, sizeof (MMGtkFilterBuilderPrivate));
}

static void
populate_logic_combo (GtkWidget *combo)
{
  int idx;

  for (idx = 0; idx < G_N_ELEMENTS (logic_strs); idx++) {
    gtk_combo_box_append_text (GTK_COMBO_BOX (combo), logic_strs[idx]);
  }

  gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 0);
}

static MMComparisionOperator
logic_combo_get_selected (GtkComboBox *combo)
{
  gint idx;

  idx = gtk_combo_box_get_active (combo);
  return (MMComparisionOperator) (idx + 1);
}

static const char *
comparision_op_to_string (MMComparisionOperator op)
{
  return logic_strs[op - 1];
}

static void
cat_combo_changed_cb (GtkComboBox *combo,
                      MMGtkFilterBuilder *self)
{
  gint idx;
  MMAttributeManager *attr_manager;

  idx = gtk_combo_box_get_active (combo);
  attr_manager = g_hash_table_lookup (self->details->attr_managers, 
                                      &idx);
  mm_gtk_attribute_store_set_from_attribute_manager (self->details->attribute_store,
                                                     attr_manager);
  gtk_combo_box_set_model (GTK_COMBO_BOX (self->details->attr_combo),
                           GTK_TREE_MODEL (self->details->attribute_store));
  gtk_combo_box_set_active (GTK_COMBO_BOX (self->details->attr_combo), 0);
}

static void
add_filter_button_clicked_cb (GtkButton *button,
                              MMGtkFilterBuilder *self)
{
  MMFilterParam *fp;
  MMAttribute *attribute;
  MMComparisionOperator op;
  GtkTreeIter attr_iter, fp_iter;
  GValue val = { 0, };
  const char * val_string;
  gboolean res;

  /* we need to build a filter-param object with the selected information */
  gtk_combo_box_get_active_iter (GTK_COMBO_BOX (self->details->attr_combo),
                                 &attr_iter);
  gtk_tree_model_get (GTK_TREE_MODEL (self->details->attribute_store),
                      &attr_iter, MM_GTK_ATTR_STORE_ATTR_COL,
                      &attribute, -1);
  op = logic_combo_get_selected (GTK_COMBO_BOX (self->details->logic_combo));
  val_string = gtk_entry_get_text (GTK_ENTRY (self->details->value_entry));
  res = mm_gvalue_from_string (val_string, mm_attribute_get_value_type (attribute),
                               &val);
  /* TODO: the attribute manager should provide a method to validate values
   *       for the relevant attribute.
   */
  if (res) {
    fp = mm_filter_param_new (attribute, &val, op);
    gtk_list_store_append (self->details->filter_store, &fp_iter);
    gtk_list_store_set (self->details->filter_store, &fp_iter,
                        0, fp, -1);
  }
}

static void
filter_param_logic_cell_data_func (GtkTreeViewColumn *column,
                                   GtkCellRenderer *cell,
                                   GtkTreeModel *model,
                                   GtkTreeIter *iter,
                                   gpointer data)
{
  MMFilterParam *fp;
  const char *logic_name;

  gtk_tree_model_get (model, iter, 0, &fp, -1);
  logic_name = comparision_op_to_string (mm_filter_param_get_operator (fp));
  g_object_set (cell, "text", logic_name, NULL);

  g_object_unref (fp);
}

static void
filter_param_value_cell_data_func (GtkTreeViewColumn *column,
                                   GtkCellRenderer *cell,
                                   GtkTreeModel *model,
                                   GtkTreeIter *iter,
                                   gpointer data)
{
  MMFilterParam *fp;
  char *value_str;
  GValue *val;

  gtk_tree_model_get (model, iter, 0, &fp, -1);
  val = mm_filter_param_get_value (fp);
  value_str = mm_gvalue_to_string (val);
  g_object_set (cell, "text", value_str, NULL);

  g_free (value_str);
  g_object_unref (fp);
}

static void
filter_param_attr_cell_data_func (GtkTreeViewColumn *column,
                                  GtkCellRenderer *cell,
                                  GtkTreeModel *model,
                                  GtkTreeIter *iter,
                                  gpointer data)
{
  MMFilterParam *fp;
  const char *attr_name;

  gtk_tree_model_get (model, iter, 0, &fp, -1);
  attr_name = mm_attribute_get_name (mm_filter_param_get_attribute (fp));
  g_object_set (cell, "text", attr_name, NULL);

  g_object_unref (fp);
}

static void
mm_gtk_filter_builder_init (MMGtkFilterBuilder *self)
{
  MMGtkFilterBuilderPrivate *details = self->details = 
    MM_GTK_FILTER_BUILDER_GET_PRIVATE (self);
  GtkWidget *filter_view;
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
  GtkWidget *table;
  GtkWidget *w;

  details->attribute_store = mm_gtk_attribute_store_new ();
  details->attr_managers = g_hash_table_new (g_int_hash, g_int_equal);

  /* tree view */
  details->filter_store = gtk_list_store_new (1, MM_TYPE_FILTER_PARAM);
  filter_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (details->filter_store));
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Attribute"));
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func (column, renderer,
                                           filter_param_attr_cell_data_func,
                                           NULL, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (filter_view), column);
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_title (column, _("Logic"));
  gtk_tree_view_column_set_cell_data_func (column, renderer,
                                           filter_param_logic_cell_data_func,
                                           NULL, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (filter_view), column);
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_title (column, _("Value"));
  gtk_tree_view_column_set_cell_data_func (column, renderer,
                                           filter_param_value_cell_data_func,
                                           NULL, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (filter_view), column);
  details->filter_view = filter_view;
  gtk_box_pack_start (GTK_BOX (self), GTK_WIDGET (filter_view),
                      FALSE, FALSE, 12);
  gtk_widget_show (filter_view);

  /* table */
  table = gtk_table_new (3, 4, FALSE);

  /* attribute category */
  w = gtk_label_new_with_mnemonic (_("Attribute _category:"));
  gtk_table_attach (GTK_TABLE (table), w,
                    0, 1, 0, 1,
                    0, 0, 0, 0);
  details->cat_combo = gtk_combo_box_new_text ();
  g_signal_connect (details->cat_combo, "changed",
                    G_CALLBACK (cat_combo_changed_cb), self);
  gtk_table_attach (GTK_TABLE (table), details->cat_combo,
                    1, 2, 0, 1,
                    0, 0, 0, 0);

  /* attribute name */
  w = gtk_label_new_with_mnemonic (_("Attribute _name:"));
  gtk_table_attach (GTK_TABLE (table), w,
                    2, 3, 0, 1,
                    0, 0, 0, 0);
  details->attr_combo = gtk_combo_box_new ();
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (details->attr_combo),
                              renderer, TRUE);
  gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (details->attr_combo),
                                 renderer,
                                 "text", MM_GTK_ATTR_STORE_NAME_COL);
  gtk_table_attach (GTK_TABLE (table), details->attr_combo,
                    3, 4, 0, 1,
                    0, 0, 0, 0);
  
  /* logic */
  w = gtk_label_new_with_mnemonic (_("_Logic:"));
  gtk_table_attach (GTK_TABLE (table), w,
                    0, 1, 1, 2,
                    0, 0, 0, 0);
  details->logic_combo = gtk_combo_box_new_text ();
  gtk_table_attach (GTK_TABLE (table), details->logic_combo,
                    1, 2, 1, 2,
                    0, 0, 0, 0);

  /* value */
  w = gtk_label_new_with_mnemonic (_("_Value:"));
  gtk_table_attach (GTK_TABLE (table), w,
                    2, 3, 1, 2,
                    0, 0, 0, 0);
  details->value_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), details->value_entry,
                    3, 4, 1, 2,
                    0, 0, 0, 0);

  /* add filter button */
  w = gtk_button_new_from_stock (GTK_STOCK_ADD);
  g_signal_connect (w, "clicked",
                    G_CALLBACK (add_filter_button_clicked_cb), self);
  gtk_table_attach (GTK_TABLE (table), w,
                    3, 4, 2, 3,
                    0, 0, 0, 0);
  gtk_box_pack_start (GTK_BOX (self), table,
                      FALSE, FALSE, 12);
  gtk_widget_show_all (table);

  populate_logic_combo (details->logic_combo);
}

static void
populate_from_category (MMGtkFilterBuilder *self, MMCategory *cat)
{
  MMApplicationType type;
  MMAttributeManager *attr_manager;
  gint key = 0;

  /* TODO: we should take care in some way that applications may want to define
   *       a custom AttributeManager. The API here in libmmanager-gtk is already
   *       setup for that, but this needs an API addition in MMApplication.
   */

  g_return_if_fail (MM_GTK_IS_FILTER_BUILDER (self));
  g_return_if_fail (MM_IS_CATEGORY (cat));

  /* TODO: when I will create Photo, Video and Music attribute managers,
   *       here is the place for adding those custom attributes.
   *
   */

  attr_manager = mm_attribute_base_manager_get ();
  mm_gtk_attribute_store_set_from_attribute_manager (self->details->attribute_store,
                                                     attr_manager);
  gtk_combo_box_append_text (GTK_COMBO_BOX (self->details->cat_combo),
                             _("Base attributes"));
  g_hash_table_insert (self->details->attr_managers, &key, attr_manager);

  /* this should trigger population in the attr combo box */
  gtk_combo_box_set_active (GTK_COMBO_BOX (self->details->cat_combo), 0);
}

/* public methods */

/**
 * mm_gtk_filter_builder_new:
 * @category: a #MMCategory.
 *
 * Builds a #GtkWidget that allows to create #MMFilter objects for attributes
 * supported by @category.
 *
 * Return value: a new #MMGtkFilterBuilder.
 */

MMGtkFilterBuilder *
mm_gtk_filter_builder_new (MMCategory *category)
{
  MMGtkFilterBuilder *self;

  self = MM_GTK_FILTER_BUILDER (g_object_new (MM_GTK_TYPE_FILTER_BUILDER, NULL));
  populate_from_category (self, category);

  return self;
}

/**
 * mm_gtk_filter_builder_get_filter:
 * @self: a #MMGtkFilterBuilder.
 *
 * Gets the #MMFilter for all the conditions specified within @self.
 *
 * Return value: a new #MMFilter, which might be empty if no condition has
 * been specified inside @self.
 */

MMFilter *
mm_gtk_filter_builder_get_filter (MMGtkFilterBuilder *self)
{
  MMFilter *filter;
  GtkTreeIter iter;
  gboolean next = TRUE;
  MMFilterParam *fp;
  gboolean res;

  g_return_val_if_fail (MM_GTK_IS_FILTER_BUILDER (self), NULL);

  filter = mm_filter_new ();
  res = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (self->details->filter_store),
                                       &iter);
  if (!res) {
    goto out;
  }

  while (next) {
    gtk_tree_model_get (GTK_TREE_MODEL (self->details->filter_store),
                        &iter, 0, &fp, -1);
    mm_filter_add_filtering_param (filter, fp);
    next = gtk_tree_model_iter_next (GTK_TREE_MODEL (self->details->filter_store),
                                     &iter);
    g_object_unref (fp);
  }

out:
  return filter;
}
