/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include "mm-gtk-hit-store.h"

#include "libmmanager/mm-attribute-manager.h"
#include "libmmanager/mm-attribute-base-manager.h"
#include "libmmanager/mm-attribute.h"
#include "libmmanager/mm-hit.h"
#include "libmmanager/mm-hit-collection.h"

#include <gtk/gtk.h>
#include <glib.h>
#include <glib-object.h>

/* our parent's model iface */
static GtkTreeModelIface parent_iface = { 0, };
static void mm_gtk_hit_store_tree_model_iface_init (GtkTreeModelIface *iface);

G_DEFINE_TYPE_EXTENDED (MMGtkHitStore, mm_gtk_hit_store,
                        GTK_TYPE_LIST_STORE, 0,
                        G_IMPLEMENT_INTERFACE (GTK_TYPE_TREE_MODEL,
                                               mm_gtk_hit_store_tree_model_iface_init));

static void
mm_gtk_hit_store_class_init (MMGtkHitStoreClass *klass)
{
  /* do nothing */
}

static void
mm_gtk_hit_store_init (MMGtkHitStore *self)
{
  GType types [] = { MM_TYPE_HIT };

  gtk_list_store_set_column_types (GTK_LIST_STORE (self), 1, types);
}

static void
populate_from_hit_collection (MMGtkHitStore *self,
                              MMHitCollection *collection)
{
  MMHit *hit = NULL;
  GtkTreeIter iter;

  /* just append the hit collection to the model */
  while ((hit = mm_hit_collection_get_next_hit (collection)) != NULL) {
    gtk_list_store_append (GTK_LIST_STORE (self), &iter);
    gtk_list_store_set (GTK_LIST_STORE (self), &iter,
                        MM_GTK_HIT_STORE_HIT_COL, hit, -1);
    /* the store keeps a reference of the object */
    g_object_unref (hit);
  }
}

/* interface implementation */

static GType
impl_get_column_type (GtkTreeModel *self, gint column)
{
  GType types [] = {
    MM_TYPE_HIT,
    G_TYPE_FILE,
    G_TYPE_STRING,
    G_TYPE_ICON
  };

  g_return_val_if_fail (MM_GTK_IS_HIT_STORE (self), G_TYPE_INVALID);
  g_return_val_if_fail (column >= 0 && column < MM_GTK_HIT_STORE_N_COL, G_TYPE_INVALID);

  return types[column];
}

static int
impl_get_n_columns (GtkTreeModel *self)
{
  g_return_val_if_fail (MM_GTK_IS_HIT_STORE (self), 0);

  /* we have four columns:
   * - hit
   * - gfile
   * - name
   * - gicon
   */
  return MM_GTK_HIT_STORE_N_COL;
}

static MMHit *
get_hit (GtkTreeModel *self, GtkTreeIter *iter)
{
  GValue val = { 0, };
  MMHit *hit;

  /* validate our parameters */
  g_return_val_if_fail (MM_GTK_IS_HIT_STORE (self), NULL);
  g_return_val_if_fail (iter != NULL, NULL);

  /* retreive the object using our parent's interface */
  parent_iface.get_value (self, iter, MM_GTK_HIT_STORE_HIT_COL, &val);

  hit = MM_HIT (g_value_dup_object (&val));
  g_value_unset (&val);

  return hit;
}

static GFile *
get_gfile_from_hit (MMHit *hit)
{
  GFile *file;
  GHashTable *pairs;
  GValue *uri_val;
  MMAttribute *uri_attr;
  const char *uri;
  
  pairs = mm_hit_get_values (hit,
                             MM_ATTRIBUTE_BASE_URI);
  uri_attr = mm_attribute_manager_lookup_attribute (mm_attribute_base_manager_get (),
                                                    MM_ATTRIBUTE_BASE_URI);
  uri_val = g_hash_table_lookup (pairs, uri_attr);
  uri = g_value_get_string (uri_val);
  file = g_file_new_for_uri (uri);

  g_hash_table_destroy (pairs);

  return file;
}

static void
value_set_gicon_from_hit (GValue *val, MMHit *hit)
{
  /* TODO: maybe we should support custom icons in some way... */
  GFile *file;
  GFileInfo *info;
  GIcon *icon;

  file = get_gfile_from_hit (hit);
  info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_ICON,
                            0, NULL, NULL);
  if (info) {
    icon = g_file_info_get_icon (info);
    g_object_unref (info);
  } else {
    /* fallback */
    icon = g_themed_icon_new ("document");
  }

  g_value_take_object (val, icon);
  g_object_unref (file);
}

static void
value_set_name_from_hit (GValue *val, MMHit *hit)
{
  GHashTable *pairs;
  MMAttribute *name_attr;
  GValue *name_val;

  pairs = mm_hit_get_values (hit,
                             MM_ATTRIBUTE_BASE_NAME);
  name_attr = mm_attribute_manager_lookup_attribute (mm_attribute_base_manager_get (),
                                                     MM_ATTRIBUTE_BASE_NAME);
  name_val = g_hash_table_lookup (pairs, name_attr);

  if (name_val) {
    g_value_copy (name_val, val);
  } else {
    /* fallback */
    GFile *file;
    GFileInfo *info;

    file = get_gfile_from_hit (hit);
    info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
                              0, NULL, NULL);
    if (info) {
      g_value_set_string (val, g_file_info_get_display_name (info));
      g_object_unref (info);
    }
    g_object_unref (file);
  }

  g_hash_table_destroy (pairs);
}

static void
value_set_gfile_from_hit (GValue *val, MMHit *hit)
{
  GFile *file;

  file = get_gfile_from_hit (hit);
  g_value_take_object (val, file);
}

static void
impl_get_value (GtkTreeModel *self, GtkTreeIter *iter,
                gint column, GValue *value)
{
  MMHit *hit;

  g_return_if_fail (MM_GTK_IS_HIT_STORE (self));
  g_return_if_fail (iter != NULL);
  g_return_if_fail (column >= 0 && column < MM_GTK_HIT_STORE_N_COL);
  g_return_if_fail (value != NULL);

  hit = get_hit (self, iter);
  value = g_value_init (value, impl_get_column_type (self, column));

  switch (column) {
    case MM_GTK_HIT_STORE_HIT_COL:
      g_value_set_object (value, hit);
      break;
    case MM_GTK_HIT_STORE_FILE_COL:
      value_set_gfile_from_hit (value, hit);
      break;
    case MM_GTK_HIT_STORE_NAME_COL:
      value_set_name_from_hit (value, hit);
      break;
    case MM_GTK_HIT_STORE_ICON_COL:
      value_set_gicon_from_hit (value, hit);
      break;
    default:
      g_assert_not_reached ();
      break;
  }

  g_object_unref (hit);
}

static void
mm_gtk_hit_store_tree_model_iface_init (GtkTreeModelIface *iface)
{
  /* save a copy of our parent's iface */
  parent_iface = *iface;

  /* override the iface methods */
  iface->get_n_columns = impl_get_n_columns;
  iface->get_column_type = impl_get_column_type;
  iface->get_value = impl_get_value;
}

/* public methods */

/**
 * mm_gtk_hit_store_new:
 *
 * Builds a #GtkListStore that encapsulates a #MMHitCollection in a
 * #GtkTreeModel fashion.
 *
 * Return value: an empty #MMGtkHitStore.
 */

MMGtkHitStore*
mm_gtk_hit_store_new (void)
{
  MMGtkHitStore * self;

  self = MM_GTK_HIT_STORE (g_object_new (MM_GTK_TYPE_HIT_STORE, NULL));

  return self;
}

/**
 * mm_gtk_hit_store_set_collection:
 * @self: a #MMGtkHitStore.
 * @collection: a #MMHitCollection.
 *
 * Sets the collection to be stored inside @self. If there's already a
 * #MMHitCollection stored inside the model, the model will be cleared first.
 */

void
mm_gtk_hit_store_set_collection (MMGtkHitStore *self, MMHitCollection *collection)
{
  g_return_if_fail (MM_GTK_IS_HIT_STORE (self));
  g_return_if_fail (MM_IS_HIT_COLLECTION (collection));

  gtk_list_store_clear (GTK_LIST_STORE (self));
  populate_from_hit_collection (self, collection);
}
