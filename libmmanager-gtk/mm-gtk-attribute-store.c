/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "mm-gtk-attribute-store.h"

#include "libmmanager/mm-attribute-manager.h"
#include "libmmanager/mm-attribute.h"

#include <gtk/gtk.h>
#include <glib.h>
#include <glib-object.h>

/* our parent's model iface */
static GtkTreeModelIface parent_iface = { 0, };
static void mm_gtk_attribute_store_tree_model_iface_init (GtkTreeModelIface *iface);

G_DEFINE_TYPE_EXTENDED (MMGtkAttributeStore, mm_gtk_attribute_store,
                        GTK_TYPE_LIST_STORE, 0,
                        G_IMPLEMENT_INTERFACE (GTK_TYPE_TREE_MODEL,
                                               mm_gtk_attribute_store_tree_model_iface_init));

static void
mm_gtk_attribute_store_class_init (MMGtkAttributeStoreClass *klass)
{
  /* do nothing */
}

static void
mm_gtk_attribute_store_init (MMGtkAttributeStore *self)
{
  GType types[] = { G_TYPE_POINTER };

  gtk_list_store_set_column_types (GTK_LIST_STORE (self), 1,
                                   types);
}

/* interface methods implementations */

static GType
impl_get_column_type (GtkTreeModel *self,
                      gint column)
{
  GType types [] = {
    G_TYPE_POINTER,
    G_TYPE_STRING,
    G_TYPE_STRING,
    G_TYPE_STRING,
    G_TYPE_GTYPE
  };

  g_return_val_if_fail (MM_GTK_IS_ATTRIBUTE_STORE (self), G_TYPE_INVALID);
  g_return_val_if_fail (column >= 0 && column < MM_GTK_ATTR_STORE_N_COL, G_TYPE_INVALID);

  return types[column];
}

static MMAttribute *
get_attribute_from_store (GtkTreeModel *self, GtkTreeIter *iter)
{
  GValue val = { 0, };
  MMAttribute *attr;

  /* validate our parameters */
  g_return_val_if_fail (MM_GTK_IS_ATTRIBUTE_STORE (self), NULL);
  g_return_val_if_fail (iter != NULL, NULL);

  /* retreive the object using our parent's interface */
  parent_iface.get_value (self, iter, MM_GTK_ATTR_STORE_ATTR_COL, &val);

  attr = (MMAttribute *) g_value_get_pointer (&val);
  g_value_unset (&val);

  return attr;
}

static void
impl_get_value (GtkTreeModel *self, GtkTreeIter *iter,
                gint column, GValue *value)
{
  MMAttribute *attr;

  value = g_value_init (value, impl_get_column_type (self, column));
  attr = get_attribute_from_store (self, iter);

  switch (column) {
    case MM_GTK_ATTR_STORE_ATTR_COL:
      /* return the attr itself */
      g_value_set_pointer (value, attr);
      break;
    case MM_GTK_ATTR_STORE_ID_COL:
      g_value_set_string (value, mm_attribute_get_id (attr));
      break;
    case MM_GTK_ATTR_STORE_NAME_COL:
      g_value_set_string (value, mm_attribute_get_name (attr));
      break;
    case MM_GTK_ATTR_STORE_DESC_COL:
      g_value_set_string (value, mm_attribute_get_description (attr));
      break;
    case MM_GTK_ATTR_STORE_GTYPE_COL:
      g_value_set_gtype (value, mm_attribute_get_value_type (attr));
      break;
    default:
      g_assert_not_reached ();
      break;
  }
}

static gint
impl_get_n_columns (GtkTreeModel *self)
{
  g_return_val_if_fail (MM_GTK_IS_ATTRIBUTE_STORE (self), 0);

  return MM_GTK_ATTR_STORE_N_COL;
}

static void
mm_gtk_attribute_store_tree_model_iface_init (GtkTreeModelIface *iface)
{
  /* save a copy of our parent's iface */
  parent_iface = *iface;

  /* override the iface methods */
  iface->get_n_columns = impl_get_n_columns;
  iface->get_column_type = impl_get_column_type;
  iface->get_value = impl_get_value;
}

/* public methods */

/**
 * mm_gtk_attribute_store_new:
 *
 * Builds an empty #GtkListStore, which will allow to store #MMAttributes
 * in a #GtkTreeModel fashion.
 *
 * Return value: an empty #MMGtkAttributeStore.
 */

MMGtkAttributeStore*
mm_gtk_attribute_store_new (void)
{
  return g_object_new (MM_GTK_TYPE_ATTRIBUTE_STORE, NULL);
}

/**
 * mm_gtk_attribute_store_add_attribute:
 * @self: a #MMGtkAttributeStore.
 * @attr: a #MMAttriubte.
 *
 * Adds @attr to the model pointed by @self. This isn't very useful for
 * clients, you might want to use
 * #mm_gtk_attribute_store_set_from_attribute_manager instead.
 */

void
mm_gtk_attribute_store_add_attribute (MMGtkAttributeStore *self,
                                      MMAttribute *attr)
{
  GtkTreeIter iter;

  g_return_if_fail (MM_GTK_IS_ATTRIBUTE_STORE (self));

  gtk_list_store_append (GTK_LIST_STORE (self), &iter);
  gtk_list_store_set (GTK_LIST_STORE (self), &iter,
                      MM_GTK_ATTR_STORE_ATTR_COL, attr, -1);
}

/**
 * mm_gtk_attribute_store_set_from_attribute_manager:
 * @self: a #MMGtkAttributeStore.
 * @attr_manager: a #MMAttributeManager.
 *
 * Encapsulates all the attributes supported by @attr_manager into the
 * #GtkTreeModel implemented by @self.
 */

void
mm_gtk_attribute_store_set_from_attribute_manager (MMGtkAttributeStore *self,
                                                   MMAttributeManager *attr_manager)
{
  GList *attrs, *l;

  g_return_if_fail (MM_GTK_IS_ATTRIBUTE_STORE (self));
  g_return_if_fail (MM_IS_ATTRIBUTE_MANAGER (attr_manager));

  gtk_list_store_clear (GTK_LIST_STORE (self));

  attrs = mm_attribute_manager_get_supported_attributes (attr_manager);
  for (l = attrs; l; l = l->next) {
    mm_gtk_attribute_store_add_attribute (self, l->data);
  }
}
