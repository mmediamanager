/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_GTK_HIT_STORE_H__
#define __MM_GTK_HIT_STORE_H__

#include <libmmanager/mm-hit-collection.h>

#include <glib-object.h>
#include <gtk/gtk.h>

#define MM_GTK_TYPE_HIT_STORE mm_gtk_hit_store_get_type()
#define MM_GTK_HIT_STORE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MM_GTK_TYPE_HIT_STORE, MMGtkHitStore))
#define MM_GTK_HIT_STORE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MM_GTK_TYPE_HIT_STORE, MMGtkHitStoreClass))
#define MM_GTK_IS_HIT_STORE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MM_GTK_TYPE_HIT_STORE))
#define MM_GTK_IS_HIT_STORE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MM_GTK_TYPE_HIT_STORE))
#define MM_GTK_HIT_STORE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MM_GTK_TYPE_HIT_STORE, MMGtkHitStoreClass))

/**
 * SECTION:mm-gtk-hit-store
 * @short_description: stores #MMHit objects.
 *
 * #MMGtkHitStore is a #GtkListStore which holds #MMHit objects.
 */

/**
 * MMGtkHitStore:
 *
 * A #GtkListStore holding #MMHit<!-- -->s.
 */

typedef struct _MMGtkHitStore MMGtkHitStore;
typedef struct _MMGtkHitStoreClass MMGtkHitStoreClass;

struct _MMGtkHitStore {
  GtkListStore parent;
};

struct _MMGtkHitStoreClass {
  GtkListStoreClass parent_class;
};

enum {
  MM_GTK_HIT_STORE_HIT_COL,
  MM_GTK_HIT_STORE_FILE_COL,
  MM_GTK_HIT_STORE_NAME_COL,
  MM_GTK_HIT_STORE_ICON_COL,
  MM_GTK_HIT_STORE_N_COL
};

GType          mm_gtk_hit_store_get_type       (void);
MMGtkHitStore* mm_gtk_hit_store_new            (void);
void           mm_gtk_hit_store_set_collection (MMGtkHitStore *self,
                                                MMHitCollection *collection);

#endif /* __MM_GTK_HIT_STORE_H__ */
