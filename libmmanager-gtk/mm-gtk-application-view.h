/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_GTK_APPLICATION_VIEW__
#define __MM_GTK_APPLICATION_VIEW__

#include <libmmanager/mm-application.h>
#include <libmmanager/mm-types.h>
#include <libmmanager/mm-category.h>

#include <glib-object.h>
#include <gtk/gtk.h>

#define MM_GTK_TYPE_APPLICATION_VIEW mm_gtk_application_view_get_type()
#define MM_GTK_APPLICATION_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MM_GTK_TYPE_APPLICATION_VIEW, MMGtkApplicationView))
#define MM_GTK_APPLICATION_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MM_GTK_TYPE_APPLICATION_VIEW, MMGtkApplicationViewClass))
#define MM_GTK_IS_APPLICATION_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MM_GTK_TYPE_APPLICATION_VIEW))
#define MM_GTK_IS_APPLICATION_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MM_GTK_TYPE_APPLICATION_VIEW))
#define MM_GTK_APPLICATION_VIEW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MM_GTK_TYPE_APPLICATION_VIEW, MMGtkApplicationViewClass))

/**
 * SECTION:mm-gtk-application-view
 * @short_description: view #GtkWidget for #MMGtkApplicationStore
 *
 * #MMGtkApplicationView is a #GtkTreeView which shows the contents of
 * a #MMGtkApplicationStore in a nice tree similar to the Nautilus tree
 * sidebar.
 */
 
/**
 * MMGtkApplicationView:
 *
 * A #GtkTreeView made for showing a #MMGtkApplicationStore.
 */

typedef struct _MMGtkApplicationView        MMGtkApplicationView;
typedef struct _MMGtkApplicationViewClass   MMGtkApplicationViewClass;
typedef struct _MMGtkApplicationViewPrivate MMGtkApplicationViewPrivate;

struct _MMGtkApplicationView {
  GtkTreeView parent;
  MMGtkApplicationViewPrivate *details;
};

struct _MMGtkApplicationViewClass {
  GtkTreeViewClass parent_class;

  void (* application_selected) (MMGtkApplicationView *view,
                                 MMApplication        *application);
  void (* category_selected)    (MMGtkApplicationView *view,
                                 MMCategory           *category);
  void (* mmtype_selected)      (MMGtkApplicationView *view,
                                 MMApplicationType     type);
};

GType                 mm_gtk_application_view_get_type (void);
MMGtkApplicationView* mm_gtk_application_view_new (void);

#endif /* __MM_GTK_APPLICATION_VIEW__ */
