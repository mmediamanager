#include "mm.h"

int main (int argc, char ** argv)
{
  MMFilter *filter, *unserialized;
  MMFilterParam *fp;
  MMAttribute *attr;
  GValue *val;
  MMAttributeManager *am;
  char * serialized;
  GError *error = NULL;

  g_type_init ();

  am = mm_attribute_base_manager_get ();
  filter = mm_filter_new ();

  attr = mm_attribute_manager_lookup_attribute (am, "base::uri");
  val = mm_create_gvalue_for_attribute (attr);
  g_value_set_string (val, "file:///foo");
  fp = mm_filter_param_new (attr, val, MM_COMP_EQUAL);
  mm_filter_add_filtering_param (filter, fp);
  g_object_unref (fp);
  g_free (val);

  attr = mm_attribute_manager_lookup_attribute (am, "base::name");
  val = mm_create_gvalue_for_attribute (attr);
  g_value_set_string (val, "Pippo");
  fp = mm_filter_param_new (attr, val, MM_COMP_GREATER);
  mm_filter_add_filtering_param (filter, fp);
  g_object_unref (fp);
  g_free (val);

  serialized = mm_filter_serialize (filter, &error);
  g_print ("serialized filter \n%s\n", serialized);

  unserialized = mm_filter_unserialize (serialized, &error);

  g_free (serialized);
  g_object_unref (filter);

  return 0;
}
