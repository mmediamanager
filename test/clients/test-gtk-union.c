#include <gtk/gtk.h>
#include <glib.h>
#include <libmmanager/mm.h>
#include <libmmanager-gtk/mm-gtk.h>

static gboolean
filter_closed_cb (GtkWidget *toplevel,
                  GdkEvent *event,
                  GPtrArray *data)
{
  MMGtkFilterBuilder *builder = g_ptr_array_index (data, 0);
  MMCategory *cat = g_ptr_array_index (data, 1);
  MMFilter *filter;
  MMHitCollection *hits;
  MMGtkHitView *view;
  GtkWidget *window3;

  filter = mm_gtk_filter_builder_get_filter (builder);
  hits = mm_category_get_hits (cat, filter, NULL);
  view = mm_gtk_hit_view_new (hits);
  gtk_widget_show (GTK_WIDGET (view));

  window3 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_container_add (GTK_CONTAINER (window3), GTK_WIDGET (view));
  gtk_widget_show (window3);

  return FALSE;
}

static void
cat_selected_cb (MMGtkApplicationView *view,
                 MMCategory *cat,
                 GtkWidget *toplevel)
{
  MMGtkFilterBuilder *builder;
  GtkWidget *window2;
  GPtrArray *data;

  builder = mm_gtk_filter_builder_new (cat);
  gtk_widget_show (GTK_WIDGET (builder));
  window2 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_container_add (GTK_CONTAINER (window2), GTK_WIDGET (builder));
  gtk_widget_show (window2);

  data = g_ptr_array_sized_new (2);
  g_ptr_array_add (data, builder);
  g_ptr_array_add (data, cat);
  
  g_signal_connect (window2, "delete-event",
                    G_CALLBACK (filter_closed_cb),
                    data);
}

int main (int argc, char **argv)
{
  GtkWidget *toplevel;
  MMGtkApplicationView *view;

  gtk_init (&argc, &argv);

  toplevel = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (toplevel, "delete-event", G_CALLBACK (gtk_main_quit), NULL);
  view = mm_gtk_application_view_new ();
  g_signal_connect (view, "category-selected",
		    G_CALLBACK (cat_selected_cb), toplevel);
  gtk_widget_show (GTK_WIDGET (view));

  gtk_container_add (GTK_CONTAINER (toplevel), GTK_WIDGET (view));
  gtk_widget_show (toplevel);

  gtk_main ();
}
