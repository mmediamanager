#include <libmmanager-gtk/mm-gtk-filter-builder.h>
#include <libmmanager/mm.h>
#include <gtk/gtk.h>

int main (int argc, char **argv)
{
  GtkWidget *toplevel;
  MMGtkFilterBuilder *builder;
  MMManager *manager;
  GList *apps, *cats;
  MMCategory *cat;

  gtk_init (&argc, &argv);
  toplevel = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  manager = mm_manager_get ();
  apps = mm_manager_get_application_list (manager);
  cats = mm_application_get_categories (apps->data, NULL);

  builder = mm_gtk_filter_builder_new (cats->data);

  gtk_widget_show (GTK_WIDGET (builder));

  gtk_container_add (GTK_CONTAINER (toplevel),
                     GTK_WIDGET (builder));

  gtk_widget_show_all (GTK_WIDGET (toplevel));

  gtk_main ();

  return 0;
}
