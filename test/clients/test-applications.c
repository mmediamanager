/* test program for libmmanager */

#include "config.h"
#include <libmmanager/mm.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <glib.h>

static void
print_all (MMAttribute * attr,
           GValue *val,
           gpointer unused)
{
  const char *string;

  string = g_value_get_string (val);
  g_print ("attr %s, name %s, desc %s, value %s\n", mm_attribute_get_id (attr),
           mm_attribute_get_name (attr), mm_attribute_get_description (attr),
           string);
}

static void
print_all_hits (MMHitCollection *coll)
{
  MMHit *h;
  GHashTable *table;

  while ((h = mm_hit_collection_get_next_hit (coll)) != NULL) {
    table = mm_hit_get_all_values (h);
    g_hash_table_foreach (table, (GHFunc) print_all, NULL);
  }
}

static void
print_all_categories (MMApplication *app)
{
  GList *cats, *l;
  MMFilter *f;
  MMFilterParam *fp;
  MMAttribute *a;
  GValue *val;
  GError *error = NULL;

  cats = mm_application_get_categories (app, &error);
  f = g_object_new (MM_TYPE_FILTER, NULL);
  a = mm_attribute_manager_lookup_attribute (mm_attribute_base_manager_get (), MM_ATTRIBUTE_BASE_URI);
  val = mm_create_gvalue_for_attribute (a);
  g_value_set_string (val, "file:///home/anarki/blah");
  fp = mm_filter_param_new (a, val, MM_COMP_EQUAL);
  mm_filter_add_filtering_param (f, fp);
  g_object_unref (fp);
  g_free (val);
  for (l = cats; l; l = l->next) {
    g_print ("category name %s\n", mm_category_get_name (l->data));
    print_all_hits (mm_category_get_hits (l->data, f, &error));
  }

}
int main (int argc, char **argv)
{
  MMManager *manager;
  GList     *applications, *l;
  GAppInfo *info;
  MMApplication *app;

  g_type_init ();

  manager = mm_manager_get ();
  applications = mm_manager_get_application_list (manager);

  for (l = applications; l; l = l->next) {
    app = l->data;
    info = mm_application_get_app_info (app);
    g_print ("found application %s\n", g_app_info_get_name (G_APP_INFO (info)));
    print_all_categories (app);
  }
  
  g_print ("end of list\n");
  
  return 0;
}
