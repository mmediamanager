#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus-glib.h>

int main (int argc, char **argv)
{
  DBusGProxy *proxy;
  DBusGConnection *connection;
  GError *error = NULL;
  char **names, **paths = NULL;
  char *app;
  gboolean res;
  int i;

  g_type_init ();

  connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (!connection) {
    g_warning ("Error getting connection to the bus");
    g_error_free (error);
    return 0;
  }
  proxy = dbus_g_proxy_new_for_name (connection,
                                     "org.gnome.MediaManager",
                                     "/org/gnome/MediaManager/Manager",
                                     "org.gnome.MediaManager.Manager");
  if (!proxy) {
    g_warning ("Error creating proxy");
    g_error_free (error);
    return 0;
  }
  res = dbus_g_proxy_call (proxy,
                           "GetRegisteredApps", &error,
                           G_TYPE_INVALID,
                           G_TYPE_STRV,
                           &names,
                           G_TYPE_STRV,
                           &paths,
                           G_TYPE_INVALID);
  if (!res) {
    g_warning ("error while calling GetRegisteredApps %s", error->message);
    g_error_free (error);
    goto out;
  }

  for (i = 0; i < G_N_ELEMENTS (names); i++) {
    g_print ("app: %s on path %s\n", names[i], paths[i]);
  }

out:
  return 0;
}
