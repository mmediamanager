#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus-glib.h>

int main (int argc, char **argv)
{
  DBusGProxy *proxy;
  DBusGConnection *connection;
  GError *error = NULL;

  g_type_init ();
  g_thread_init (NULL);
  dbus_g_thread_init ();

  connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (!connection) {
    g_warning ("Error getting connection to the bus");
    g_error_free (error);
    return 0;
  }
  proxy = dbus_g_proxy_new_for_name (connection,
                                     "org.gnome.MediaManager",
                                     "/org/gnome/MediaManager/Manager",
                                     "org.gnome.MediaManager.Manager");
  if (!proxy) {
    g_warning ("Error creating proxy");
    g_error_free (error);
    return 0;
  }
  dbus_g_proxy_call (proxy, "RegisterApp", &error,
                     G_TYPE_STRING, "org.Foo.Bar",
                     G_TYPE_STRING, "/org/Foo/Bar",
                     G_TYPE_INVALID, G_TYPE_INVALID);
  return 0;
}
