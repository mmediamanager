#include "mm-gtk.h"
#include <gtk/gtk.h>

static void
app_selected_cb (MMGtkApplicationView *view,
                 MMApplication *app,
                 gpointer unused)
{
  g_debug ("application selected %p", app);
}

static void
mmtype_selected_cb (MMGtkApplicationView *view,
                    MMApplicationType type,
                    gpointer unused)
{
  g_debug ("mmtype selected %d", type);
}

static void
cat_selected_cb (MMGtkApplicationView *view,
                 MMCategory *cat,
                 gpointer unused)
{
  g_debug ("category selected %p", cat);
}

int main (int argc, char **argv)
{
  GtkWidget *toplevel;
  MMGtkApplicationView *view;

  g_type_init ();
  gtk_init (&argc, &argv);

  toplevel = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (toplevel, "delete-event", G_CALLBACK (gtk_main_quit), NULL);
  view = mm_gtk_application_view_new ();
  g_signal_connect (view, "application-selected",
		    G_CALLBACK (app_selected_cb), NULL);
  g_signal_connect (view, "mmtype-selected",
		    G_CALLBACK (mmtype_selected_cb), NULL);
  g_signal_connect (view, "category-selected",
		    G_CALLBACK (cat_selected_cb), NULL);
  gtk_widget_show (GTK_WIDGET (view));

  gtk_container_add (GTK_CONTAINER (toplevel), GTK_WIDGET (view));

  gtk_widget_show (toplevel);

  gtk_main ();

  return 0;
}
