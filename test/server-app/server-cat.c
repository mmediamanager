/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include <glib.h>
#include <libmmanager/mm.h>
#include <libmmanager/mm-so-category.h>

#include "server-cat.h"

static GObjectClass *server_cat_parent_class;
static GType server_cat_type = 0;

GList *
server_cat_get_categories (MMCategoryProvider *p,
                           MMApplication *app)
{
  GList *list = NULL;
  MMCategory *cat1, *cat2;

  cat1 = mm_so_category_new (MM_SO_APPLICATION (app), "Cat1", NULL);
  cat2 = mm_so_category_new (MM_SO_APPLICATION (app), "Cat2", NULL);

  list = g_list_prepend (list, cat2);
  list = g_list_prepend (list, cat1);

  return list;
}

static void
server_cat_category_provider_iface_init (MMCategoryProviderIface *iface)
{
  /* implement interface methods */
  iface->get_categories = server_cat_get_categories;
}

static void
server_cat_instance_init (ServerCat *cat)
{
  /* do nothing */
}

static void
server_cat_class_init (ServerCatClass *klass)
{
  server_cat_parent_class = g_type_class_peek_parent (klass);
}

GType
server_cat_get_type (void)
{
  return server_cat_type;
}

void
server_cat_register_type (GTypeModule *module)
{
  static const GTypeInfo info = {
    sizeof (ServerCatClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) server_cat_class_init,
    NULL,
    NULL,
    sizeof (ServerCat),
    0,
    (GInstanceInitFunc) server_cat_instance_init,
  };

  static const GInterfaceInfo category_provider_iface_info = {
    (GInterfaceInitFunc) server_cat_category_provider_iface_init,
    NULL,
    NULL
  };

  server_cat_type = g_type_module_register_type (module,
                                                 G_TYPE_OBJECT,
                                                 "ServerCat",
                                                 &info, 0);

  g_type_module_add_interface (module,
                               server_cat_type,
                               MM_TYPE_CATEGORY_PROVIDER,
                               &category_provider_iface_info);
}
