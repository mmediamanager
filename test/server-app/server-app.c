/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include <glib.h>
#include <gio/gdesktopappinfo.h>
#include <libmmanager/mm.h>

#include "server-app.h"

static GObjectClass *server_app_parent_class;
static GType server_app_type = 0;

MMApplication *
server_app_get_application (MMApplicationProvider *provider)
{
  MMApplication *app;

  app = mm_so_application_new ("foo.desktop", MM_APPLICATION_PHOTO, "server-app");

  return app;
}

static void
server_app_application_provider_iface_init (MMApplicationProviderIface *iface)
{
  /* implement interface methods */
  iface->get_application = server_app_get_application;
}

static void
server_app_instance_init (ServerApp *app)
{
  /* do nothing */
}

static void
server_app_class_init (ServerAppClass *klass)
{
  server_app_parent_class = g_type_class_peek_parent (klass);
}

GType
server_app_get_type (void)
{
  return server_app_type;
}

void
server_app_register_type (GTypeModule *module)
{
  static const GTypeInfo info = {
    sizeof (ServerAppClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) server_app_class_init,
    NULL,
    NULL,
    sizeof (ServerApp),
    0,
    (GInstanceInitFunc) server_app_instance_init,
  };

  static const GInterfaceInfo application_provider_iface_info = {
    (GInterfaceInitFunc) server_app_application_provider_iface_init,
    NULL,
    NULL
  };

  server_app_type = g_type_module_register_type (module,
                                                 G_TYPE_OBJECT,
                                                 "ServerApp",
                                                 &info, 0);

  g_type_module_add_interface (module,
                               server_app_type,
                               MM_TYPE_APPLICATION_PROVIDER,
                               &application_provider_iface_info);
}
