/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include <glib.h>
#include <libmmanager/mm.h>

#include "server-coll.h"

static GObjectClass *server_coll_parent_class;
static GType server_coll_type = 0;

MMHitCollection *
server_coll_get_hits (MMHitCollectionProvider *p,
                      MMCategory *belonging_category,
                      MMFilter *f)
{
  MMHitCollection *coll;
  MMHit *hit1;
  MMAttribute *a;
  MMAttributeManager *base_manager;
  GList *l;

  coll = mm_hit_collection_new ();
  hit1 = g_object_new (MM_TYPE_HIT, NULL);

  base_manager = mm_attribute_base_manager_get ();
  for (l = mm_attribute_manager_get_supported_attributes (base_manager); l; l = l->next) {
    a = l->data;
    g_print ("attr supported desc %s\n", mm_attribute_get_description (a));
  }
  a = mm_attribute_manager_lookup_attribute (base_manager,
                                             MM_ATTRIBUTE_BASE_URI);
  if (a) {
    GValue v = { 0, };
    g_value_init (&v, G_TYPE_STRING);
    g_value_set_string (&v, "file:///home/anarki/foo1");
    mm_hit_set_value (hit1, a, &v);
    g_value_unset (&v);
  }
  a = mm_attribute_manager_lookup_attribute (base_manager,
                                             MM_ATTRIBUTE_BASE_NAME);
  if (a) {
    GValue v = { 0, };
    g_value_init (&v, G_TYPE_STRING);
    g_value_set_string (&v, "Foo1 res");
    mm_hit_set_value (hit1, a, &v);
    g_value_unset (&v);
  }
  mm_hit_collection_add_hit (coll, hit1);   

  return coll;
}

static void
server_coll_hit_collection_provider_iface_init (MMHitCollectionProviderIface *iface)
{
  /* implement interface methods */
  iface->get_hits = server_coll_get_hits;
}

static void
server_coll_instance_init (ServerColl *coll)
{
  /* do nothing */
}

static void
server_coll_class_init (ServerCollClass *klass)
{
  server_coll_parent_class = g_type_class_peek_parent (klass);
}

GType
server_coll_get_type (void)
{
  return server_coll_type;
}

void
server_coll_register_type (GTypeModule *module)
{
  static const GTypeInfo info = {
    sizeof (ServerCollClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) server_coll_class_init,
    NULL,
    NULL,
    sizeof (ServerColl),
    0,
    (GInstanceInitFunc) server_coll_instance_init,
  };

  static const GInterfaceInfo hit_collection_provider_iface_info = {
    (GInterfaceInitFunc) server_coll_hit_collection_provider_iface_init,
    NULL,
    NULL
  };

  server_coll_type = g_type_module_register_type (module,
                                                  G_TYPE_OBJECT,
                                                  "ServerColl",
                                                  &info, 0);

  g_type_module_add_interface (module,
                               server_coll_type,
                               MM_TYPE_HIT_COLLECTION_PROVIDER,
                               &hit_collection_provider_iface_info);
}
