/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __SERVER_COLL_H__
#define __SERVER_COLL_H__

#include <glib-object.h>

#define SERVER_TYPE_COLL            (server_coll_get_type())
#define SERVER_COLL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                     SERVER_TYPE_COLL, ServerColl))
#define SERVER_IS_COLL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                     SERVER_TYPE_COLL))
                                    
typedef struct _ServerColl ServerColl;
typedef struct _ServerCollClass ServerCollClass;

struct _ServerColl {
  GObject parent_obj;
};

struct _ServerCollClass {
  GObjectClass parent_class;
};

GType server_coll_get_type (void);
void  server_coll_register_type (GTypeModule *module);

#endif /* __SERVER_COLL_H__ */
