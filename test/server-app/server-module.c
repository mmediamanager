/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <glib-object.h>

#include "server-app.h"
#include "server-cat.h"
#include "server-coll.h"

void
mm_module_initialize (GTypeModule *gtm)
{
  server_app_register_type (gtm);
  server_cat_register_type (gtm);
  server_coll_register_type (gtm);
}

void
mm_module_shutdown (void)
{

}

void
mm_module_get_types (GType **types)
{
  static GType type_list [3];

  type_list[0] = SERVER_TYPE_APP;
  type_list[1] = SERVER_TYPE_CAT;
  type_list[2] = SERVER_TYPE_COLL;

  *types = type_list;
}

void
mm_module_get_name (char **name)
{
  static char *myname = "server-app";

  *name = myname;
}
