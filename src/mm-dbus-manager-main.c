/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>
#include "mm-dbus-manager.h"

static DBusGConnection *
get_session_bus (void)
{
  GError          *error;
  DBusGConnection *bus;
  DBusConnection  *connection;

  error = NULL;
  bus = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (bus == NULL) {
    g_warning ("Couldn't connect to session bus: %s",
               error->message);
    g_error_free (error);
    goto out;
  }

  connection = dbus_g_connection_get_connection (bus);
  dbus_connection_set_exit_on_disconnect (connection, FALSE);

out:
  return bus;
}

static DBusGProxy *
get_bus_proxy (DBusGConnection *connection)
{
  DBusGProxy *bus_proxy;

  bus_proxy = dbus_g_proxy_new_for_name (connection,
                                         DBUS_SERVICE_DBUS,
                                         DBUS_PATH_DBUS,
                                         DBUS_INTERFACE_DBUS);
  return bus_proxy;
}

static gboolean
get_name_on_bus (DBusGProxy *proxy)
{
  gboolean ret = FALSE;
  GError *error = NULL;
  guint result;
  gboolean res;

  g_return_val_if_fail (proxy != NULL, FALSE);

  res = dbus_g_proxy_call (proxy,
                           "RequestName",
                           &error,
                           G_TYPE_STRING, "org.gnome.MediaManager",
                           G_TYPE_UINT, 0,
                           G_TYPE_INVALID,
                           G_TYPE_UINT, &result,
                           G_TYPE_INVALID);
  if (!res) {
    g_warning ("Failed to acquire the name on the bus: %s", error->message);
    g_error_free (error);

    goto out;
  }

  if (result != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) {
    g_warning ("Failed to acquire the name on the bus: %s", error->message);
    g_error_free (error);

    goto out;
  }

  ret = TRUE;

out:
  return ret;
}

static void
bus_proxy_destroyed_cb (DBusGProxy *bus_proxy,
                        GMainLoop *mainloop)
{
  g_main_loop_quit (mainloop);
}

int main (int argc, char **argv)
{
  MMDBusManager *manager;
  GMainLoop *mainloop;
  DBusGConnection *connection;
  DBusGProxy *bus_proxy;
  int ret = 1;

  if (!g_thread_supported ()) {
    g_thread_init (NULL);
  }
  dbus_g_thread_init ();
  g_type_init ();
  
  connection = get_session_bus ();
  if (!connection) {
    goto out;
  }

  bus_proxy = get_bus_proxy (connection);
  if (!bus_proxy) {
    g_warning ("Error creating the bus proxy, exiting");
    goto out;
  }

  if (!get_name_on_bus (bus_proxy)) {
    g_warning ("Error getting the name on the bus, exiting");
    goto out;
  }

  /* this creates the manager object and registers it on the session bus */
  manager = g_object_new (MM_TYPE_DBUS_MANAGER, NULL);

  if (!manager) {
    goto out;
  }

  mainloop = g_main_loop_new (NULL, FALSE);

  g_signal_connect (bus_proxy, "destroy",
                    G_CALLBACK (bus_proxy_destroyed_cb),
                    mainloop);

  g_main_loop_run (mainloop);

  if (manager != NULL) {
    g_object_unref (manager);
  }

  g_main_loop_unref (mainloop);

  ret = 0;

out:
  return ret;
}
