/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_DBUS_MANAGER_H__
#define __MM_DBUS_MANAGER_H__
 
#include <glib-object.h>
#include <glib.h>
#include <dbus/dbus-glib.h>
 
#define MM_TYPE_DBUS_MANAGER mm_dbus_manager_get_type()
#define MM_DBUS_MANAGER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MM_TYPE_DBUS_MANAGER, MMDBusManager))
#define MM_DBUS_MANAGER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MM_TYPE_DBUS_MANAGER, MMDBusManagerClass))
#define MM_IS_DBUS_MANAGER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MM_TYPE_DBUS_MANAGER))
#define MM_IS_DBUS_MANAGER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MM_TYPE_DBUS_MANAGER))
#define MM_DBUS_MANAGER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MM_TYPE_DBUS_MANAGER, MMDBusManagerClass))

typedef struct _MMDBusManager        MMDBusManager;
typedef struct _MMDBusManagerClass   MMDBusManagerClass;
typedef struct _MMDBusManagerDetails MMDBusManagerDetails;

/* paths */
#define MM_DBUS_PATH "/org/gnome/MediaManager"
#define MM_DBUS_MANAGER_PATH MM_DBUS_PATH "/Manager"

struct _MMDBusManager {
  GObject parent;
  MMDBusManagerDetails *details;
};

struct _MMDBusManagerClass {
  GObjectClass parent_class;
};

/* public methods */

GType           mm_dbus_manager_get_type (void);

#endif /* __MM_DBUS_MANAGER_H__ */
