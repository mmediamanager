/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>

#include "libmmanager/mm-error.h"
#include "libmmanager/mm-type-builtins.h"
#include "mm-dbus-manager.h"

gboolean mm_dbus_manager_register_app (MMDBusManager *manager, char *path,
                                       char *name, GError **error);
gboolean mm_dbus_manager_get_registered_apps (MMDBusManager *manager, char ***app_paths,
                                              char ***app_names, GError **error);

#include "mm-dbus-manager-server-bindings.h"

#define MM_DBUS_MANAGER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_DBUS_MANAGER, MMDBusManagerDetails))

struct _MMDBusManagerDetails {
  GPtrArray       *application_names;
  GPtrArray       *application_paths;
  DBusGProxy      *bus_proxy;
  DBusGConnection *connection;
};

G_DEFINE_TYPE (MMDBusManager, mm_dbus_manager, G_TYPE_OBJECT);

static void
mm_dbus_manager_finalize (GObject *o)
{
  MMDBusManager *m = MM_DBUS_MANAGER (o);

  g_ptr_array_foreach (m->details->application_paths,
                       (GFunc) g_free,
                       NULL);
  g_ptr_array_free (m->details->application_paths, TRUE);
  
  if (m->details->bus_proxy) {
    g_object_unref (m->details->bus_proxy);
  }

  G_OBJECT_CLASS (mm_dbus_manager_parent_class)->finalize (o);
}

static void
mm_dbus_manager_class_init (MMDBusManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mm_dbus_manager_finalize;

  dbus_g_object_type_install_info (MM_TYPE_DBUS_MANAGER,
                                   &dbus_glib_mm_dbus_manager_object_info);
  dbus_g_error_domain_register (MM_DBUS_ERROR_QUARK, NULL, MM_TYPE_MD_BUS_ERROR_ENUM);

  g_type_class_add_private (klass, sizeof (MMDBusManagerDetails));
}

static void
activate_applications (MMDBusManager *manager)
{
  GDir *dir;
  const char *filename;
  const char *dirname;
  GError *error = NULL;

  dirname = MMEDIAMANAGER_EXTENSIONDIR"/dbus";
  dir = g_dir_open (dirname, 0, &error);
  if (!dir) {
    g_warning ("Unable to open the DBus extensions directory %s: %s",
               dirname, error->message);
    g_error_free (error);
    return;
  }

  while ((filename = g_dir_read_name (dir)) != NULL) {
    char *path;
    GKeyFile *keyfile;
    char *executable;

    path = g_build_filename (dirname, filename, NULL);
    keyfile = g_key_file_new ();
    if (!g_key_file_load_from_file (keyfile, path, 0, &error)) {
      g_warning ("Unable to load extension descriptor %s: %s", path,
                 error->message);
      g_error_free (error);
      error = NULL;
      g_key_file_free (keyfile);
      g_free (path);

      /* move on to the next file */
      continue;
    }

    /* we are looking for the HelperPath line under the MediaManager group */
    executable = g_key_file_get_string (keyfile,
                                        "MediaManager",
                                        "HelperPath",
                                        &error);
    if (!executable) {
      g_warning ("Descriptor %s is malformed: %s", path, error->message);
      g_error_free (error);
      error = NULL;
      g_key_file_free (keyfile);
      g_free (path);

      continue;
    }

    if (!g_spawn_command_line_async (executable, &error)) {
      g_warning ("Unable to spawn the extension %s: %s", executable,
                 error->message);
      g_error_free (error);
      error = NULL;
    }

    g_key_file_free (keyfile);
    g_free (path);
    g_free (executable);
  }
}

static void
register_on_the_bus (MMDBusManager *manager)
{
  GError *error = NULL;
  MMDBusManagerDetails *details = manager->details;

  details->connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (!details->connection) {
    g_critical ("Error getting a connection to the session bus: %s",
                error->message);
    g_error_free (error);
    exit (1);
  }

  details->bus_proxy = dbus_g_proxy_new_for_name (details->connection,
                                                  DBUS_SERVICE_DBUS,
                                                  DBUS_PATH_DBUS,
                                                  DBUS_INTERFACE_DBUS);

  /* register the object */
  dbus_g_connection_register_g_object (details->connection,
                                       MM_DBUS_MANAGER_PATH,
                                       G_OBJECT (manager));
}

static void
mm_dbus_manager_init (MMDBusManager *manager)
{
  MMDBusManagerDetails *details = manager->details = MM_DBUS_MANAGER_GET_PRIVATE (manager);

  details->application_names = g_ptr_array_new ();
  details->application_paths = g_ptr_array_new ();
  register_on_the_bus (manager);
  activate_applications (manager);
}

/* implementation of dbus methods */

gboolean
mm_dbus_manager_register_app (MMDBusManager *manager, char *name,
                              char *path, GError **error)
{
  g_return_val_if_fail (MM_IS_DBUS_MANAGER (manager), FALSE);

  if (path == NULL) {
    /* set the error, the path can't be NULL */
    g_set_error (error, MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_NULL_ATTRIBUTE,
                 "Error: can't register an app with NULL path");
    return FALSE;
  }
  if (name == NULL) {
    /* set the error, the path can't be NULL */
    g_set_error (error, MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_NULL_ATTRIBUTE,
                 "Error: can't register an app with NULL name");
    return FALSE;
  }

  g_ptr_array_add (manager->details->application_names,
                   g_strdup (name));
  g_ptr_array_add (manager->details->application_paths,
                   g_strdup (path));
  return TRUE;
}

gboolean
mm_dbus_manager_get_registered_apps (MMDBusManager *manager,
                                     char ***app_names,
                                     char ***app_paths,
                                     GError **error)
{
  int index;
  int size = manager->details->application_paths->len;
  g_return_val_if_fail (MM_IS_DBUS_MANAGER (manager), FALSE);

  if (app_paths == NULL || app_names == NULL) {
    /* set the error, the client is doing something wrong */
    g_set_error (error, MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_NULL_ATTRIBUTE,
                 "Error: pointers passed to GetRegisteredApps can't be NULL");
    return FALSE;
  }

  *app_paths = g_new (gchar *, size + 1);
  (*app_paths) [size] = NULL;
  *app_names = g_new (gchar *, size + 1);
  (*app_names) [size] = NULL;

  for (index = 0; index < size; index++) {
    (*app_paths) [index] = 
      g_strdup (g_ptr_array_index (manager->details->application_paths, index));
    (*app_names) [index] = 
      g_strdup (g_ptr_array_index (manager->details->application_names, index));
  }

  return TRUE;
}
