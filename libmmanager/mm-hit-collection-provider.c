/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <config.h>
#include <glib-object.h>
#include "mm-hit-collection-provider.h"
#include "mm-category.h"
#include "mm-filter.h"
#include "mm-hit-collection.h"

static void
mm_hit_collection_provider_base_init (gpointer klass)
{

}

GType
mm_hit_collection_provider_get_type (void)
{
  static GType type = 0;

  if (!type) {
    const GTypeInfo info = {
      sizeof (MMHitCollectionProviderIface),
      mm_hit_collection_provider_base_init,
      NULL,
      NULL,
      NULL,
      NULL,
      0,
      0,
      NULL
    };

    type = g_type_register_static (G_TYPE_INTERFACE, 
                                   "MMHitCollectionProvider",
                                   &info, 0);
    g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
  }

  return type;
}

/* public method */

/**
 * mm_hit_collection_provider_get_hits:
 * @provider: a #MMHitCollectionProvider.
 * @category: a #MMCategory.
 * @filter: a #MMFilter.
 *
 * Gets the #MMHitCollection provided by @provider, passing to the module
 * the category and the filter for the query.
 *
 * Return value: a #MMHitCollection.
 */

MMHitCollection *
mm_hit_collection_provider_get_hits (MMHitCollectionProvider *provider,
                                     MMCategory              *category,
                                     MMFilter                *filter)
{
  g_return_val_if_fail (MM_IS_HIT_COLLECTION_PROVIDER (provider), NULL);
  g_return_val_if_fail (MM_IS_CATEGORY (category), NULL);
  g_return_val_if_fail (MM_IS_FILTER (filter), NULL);
  g_return_val_if_fail (MM_HIT_COLLECTION_PROVIDER_GET_IFACE (provider)->get_hits != NULL, NULL);

  return MM_HIT_COLLECTION_PROVIDER_GET_IFACE (provider)->get_hits (provider,
                                                                    category,
                                                                    filter);
}
