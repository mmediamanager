/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_MANAGER_H__
#define __MM_MANAGER_H__

#include <glib.h>
#include "mm-types.h"
#include "mm-application-provider.h"
#include "mm-module-manager.h"

#define MM_TYPE_MANAGER            (mm_manager_get_type())
#define MM_MANAGER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                    MM_TYPE_MANAGER, MMManager))
#define MM_MANAGER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                    MM_TYPE_MANAGER, MMManagerClass))
#define MM_IS_MANAGER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                    MM_TYPE_MANAGER))
#define MM_IS_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                    MM_TYPE_MANAGER))
#define MM_MANAGER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                    MM_TYPE_MANAGER, MMManagerClass))

/**
 * SECTION:mm-manager
 * @short_description: manager of the library.
 *
 * The #MMManager keeps track of all the registered applications, and offers
 * some methods to access them. The object is a singleton, owned by the
 * library.
 */
 
/**
 * MMManager:
 *
 * Base manager for the library.
 */

typedef struct _MMManager        MMManager;
typedef struct _MMManagerPrivate MMManagerPrivate;
typedef struct _MMManagerClass   MMManagerClass;

struct _MMManager {
  GObject parent_object;
  MMManagerPrivate *details;
};

struct _MMManagerClass {
  GObjectClass parent_class;
};

/* public methods */
MMManager* mm_manager_get                  (void);
GType      mm_manager_get_type             (void);

GList*	   mm_manager_get_application_list_for_type            (MMManager        *manager,
                                                                MMApplicationType type);
GList*     mm_manager_get_application_list                     (MMManager        *manager);
MMModuleManager *mm_manager_get_module_manager                 (MMManager        *manager);

#endif /* __MM_MANAGER_H__ */
