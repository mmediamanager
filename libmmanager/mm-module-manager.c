/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <glib-object.h>

#include "mm-module-manager.h"
#include "mm-application-provider.h"
#include "mm-category-provider.h"
#include "mm-hit-collection-provider.h"
#include "mm-module.h"

#define MM_MODULE_MANAGER_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_MODULE_MANAGER, MMModuleManagerPrivate))

G_DEFINE_TYPE (MMModuleManager, mm_module_manager, G_TYPE_OBJECT);

struct _MMModuleManagerPrivate {
  GList *modules;
  char  *path;
};

static void
load_modules (MMModuleManager *manager)
{
  MMModuleManagerPrivate *details = manager->details;
  GDir *dir;
  GError *error = NULL;

  dir = g_dir_open (details->path, 0, &error);
  if (!dir) {
    g_warning ("Unable to load modules from %s: %s",
               details->path, error->message);
  } else {
    const char *name;

    while ((name = g_dir_read_name (dir))) {
      if (g_str_has_suffix (name, "." G_MODULE_SUFFIX)) {
        char *filename;
        MMModule *module;

        filename = g_build_filename (details->path, name, NULL);
        module = mm_module_load_file (filename);
        g_free (filename);

        if (module) {
          details->modules = g_list_prepend (details->modules, module);
        }
      }
    }
    g_dir_close (dir);
  }
  if (error) {
    g_error_free (error);
  }
}

static void
mm_module_manager_finalize (GObject *o)
{
  MMModuleManager *manager = MM_MODULE_MANAGER (o);

  g_free (manager->details->path);
  g_list_free (manager->details->modules);
  
  G_OBJECT_CLASS (mm_module_manager_parent_class)->finalize (o);
}

static void
mm_module_manager_init (MMModuleManager *manager)
{
  MMModuleManagerPrivate *details = manager->details = 
    MM_MODULE_MANAGER_GET_PRIVATE (manager);

  details->path = g_strdup (MMEDIAMANAGER_EXTENSIONDIR);
  details->modules = NULL;
  load_modules (manager);
}

static void
mm_module_manager_class_init (MMModuleManagerClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = mm_module_manager_finalize;

  g_type_class_add_private (klass, sizeof (MMModuleManagerPrivate));
}

static MMModule *
find_module_for_name (MMModuleManager *manager,
                      const char *name)
{
  MMModule *module;
  char *module_name;
  GList *l;
  gboolean found = FALSE;

  for (l = manager->details->modules; l; l = l->next) {
    module = l->data;
    module_name = mm_module_get_name (module);
    if (g_strcmp0 (name, module_name) == 0) {
      found = TRUE;
    }
    if (found) {
      break;
    }
  }

  if (!found) {
    g_warning ("Can't find a module named %s", name);
    module = NULL;
  }

  return module;
}

/* public methods */

/**
 * mm_module_manager_get_all_application_providers:
 * @manager: a #MMModuleManager.
 *
 * Gets a list of all the #MMApplicationProvider objects known to @manager.
 *
 * Return value: a #GList of #MMApplicationProvider objects. Use #g_list_free
 * when done with it.
 */

GList *
mm_module_manager_get_all_application_providers (MMModuleManager *manager)
{
  GList *l;
  GList *application_providers = NULL;

  for (l = manager->details->modules; l; l = l->next) {
    application_providers = g_list_prepend (application_providers,
                                            mm_module_get_application_provider (MM_MODULE (l->data)));
  }

  return application_providers;
}

/**
 * mm_module_manager_get_category_provider_for_application:
 * @manager: a #MMModuleManager.
 * @id: the application id.
 *
 * Gets the #MMCategoryProvider for the application specified by @id.
 *
 * Return value: a #MMCategoryProvider or %NULL.
 */

MMCategoryProvider *
mm_module_manager_get_category_provider_for_application (MMModuleManager *manager,
                                                         const char *id)
{
  MMCategoryProvider *provider = NULL;
  MMModule *module;

  module = find_module_for_name (manager, id);
  if (!module) {
    return NULL;
  }

  provider = mm_module_get_category_provider (module);

  return provider;
}

/**
 * mm_module_manager_get_hit_collection_provider_for_application:
 * @manager: a #MMModuleManager.
 * @id: the application id.
 *
 * Gets the #MMHitCollectionProvider for the application specified by @id.
 *
 * Return value: a #MMHitCollectionProvider or %NULL.
 */

MMHitCollectionProvider *
mm_module_manager_get_hit_collection_provider_for_application (MMModuleManager *manager,
                                                               const char *id)
{
  MMHitCollectionProvider *provider = NULL;
  MMModule *module;

  module = find_module_for_name (manager, id);
  if (!module) {
    return NULL;
  }

  provider = mm_module_get_hit_collection_provider (module);

  return provider;
}
