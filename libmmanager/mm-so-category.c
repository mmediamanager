/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <glib-object.h>
#include "mm-so-category.h"
#include "mm-category.h"
#include "mm-so-application.h"
#include "mm-hit-collection-provider.h"
#include "mm-manager.h"
#include "mm-module-manager.h"
#include "mm-error.h"

G_DEFINE_TYPE (MMSoCategory, mm_so_category, MM_TYPE_CATEGORY);

static MMHitCollection *
mm_so_category_get_hits (MMCategory *c,
                         MMFilter *f,
                         GError **ret_error)
{
  MMHitCollectionProvider *provider;
  MMManager *manager;
  const char *app_id;
  MMApplication *app;

  g_return_val_if_fail (c != NULL, NULL);
  g_return_val_if_fail (MM_IS_CATEGORY (c), NULL);
  g_return_val_if_fail (f != NULL, NULL);
  g_return_val_if_fail (MM_IS_FILTER (f), NULL);

  manager = mm_manager_get ();
  app = mm_category_get_application (c);
  app_id = mm_application_get_id (app);
  provider = mm_module_manager_get_hit_collection_provider_for_application 
               (mm_manager_get_module_manager (manager), app_id);
  g_object_unref (app);
  if (!provider) {
    g_set_error (ret_error,
                 MM_SO_ERROR_QUARK,
                 MM_SO_ERROR_NULL_PROVIDER,
                 "Unable to get the hit collection provider for application %s",
                 app_id);
    return NULL;
  }

  return mm_hit_collection_provider_get_hits (provider, c, f);
}

static void
mm_so_category_class_init (MMSoCategoryClass *klass)
{
  MMCategoryClass *cat_class = MM_CATEGORY_CLASS (klass);

  cat_class->get_hits = mm_so_category_get_hits;
}

static void
mm_so_category_init (MMSoCategory *cat)
{
  /* empty */
}

/* public methods */

/**
 * mm_so_category_new:
 * @app: a #MMSoApplication.
 * @name: the name of the category.
 * @icon: the icon of the category.
 *
 * Builds a new #MMCategory with the specified name and icon. The new object
 * will own a reference to both the application and the icon.
 *
 * Return value: a #MMCategory.
 */

MMCategory *
mm_so_category_new (MMSoApplication *app,
                    const char *name,
                    GIcon *icon)
{
  MMCategory *category;

  category = MM_CATEGORY (g_object_new (MM_TYPE_SO_CATEGORY, NULL));
  mm_category_set_attributes (category, MM_APPLICATION (app), name, icon);

  return category;
}
