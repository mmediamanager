/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_SO_APPLICATION_H__
#define __MM_SO_APPLICATION_H__

#include <glib.h>
#include <glib-object.h>
#include "mm-application.h"

#define MM_TYPE_SO_APPLICATION            (mm_so_application_get_type())
#define MM_SO_APPLICATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                           MM_TYPE_SO_APPLICATION, MMSoApplication))
#define MM_SO_APPLICATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                           MM_TYPE_SO_APPLICATION, MMSoApplicationClass))
#define MM_IS_SO_APPLICATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                           MM_TYPE_SO_APPLICATION))
#define MM_IS_SO_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                           MM_TYPE_SO_APPLICATION))
#define MM_SO_APPLICATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                           MM_TYPE_SO_APPLICATION, MMSoApplicationClass))

/**
 * SECTION:mm-so-application
 * @short_description: Shared Object implementation of #MMApplication.
 *
 * A #MMSoApplication is an implementation of #MMApplication for applications
 * using the Shared Object interface of the library.
 */
 
/**
 * MMSoApplication:
 *
 * A Shared Object implementation of a #MMApplication.
 */

typedef struct _MMSoApplication MMSoApplication;

/**
 * MMSoApplicationClass:
 *
 * Class of #MMSoApplication objects.
 */
 
typedef struct _MMSoApplicationClass MMSoApplicationClass;

struct _MMSoApplication {
  MMApplication parent;
};

struct _MMSoApplicationClass {
  MMApplicationClass parent_class;
};

/* public methods */
GType           mm_so_application_get_type (void);
MMApplication * mm_so_application_new      (const char        *desktop_id,
                                            MMApplicationType  supported_type,
                                            const char        *id);

#endif /* __MM_SO_APPLICATION_H__ */
