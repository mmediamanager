/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <glib-object.h>
#include <gmodule.h>
#include "mm-application-provider.h"
#include "mm-category-provider.h"
#include "mm-hit-collection-provider.h"
#include "mm-module.h"

#define MM_MODULE_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_MODULE, MMModulePrivate))

G_DEFINE_TYPE (MMModule, mm_module, G_TYPE_TYPE_MODULE);

struct _MMModulePrivate {
  /* library objects */
  char    *filename;
  GModule *library;

  /* providers implemented by the module */
  MMApplicationProvider   *app_provider;
  MMCategoryProvider      *category_provider;
  MMHitCollectionProvider *collection_provider;

  /* name */
  char    *name;
};


static gboolean mm_module_load (GTypeModule *gtm);
static void mm_module_unload (GTypeModule *gtm);

static void
mm_module_finalize (GObject *o)
{
  MMModule *module = MM_MODULE (o);

  g_free (module->details->filename);
  G_OBJECT_CLASS (mm_module_parent_class)->finalize (o);
}

static void
mm_module_class_init (MMModuleClass *klass)
{
  GTypeModuleClass *type_module_class = G_TYPE_MODULE_CLASS (klass);

  type_module_class->load = mm_module_load;
  type_module_class->unload = mm_module_unload;
  G_OBJECT_CLASS (klass)->finalize = mm_module_finalize;

  g_type_class_add_private (klass, sizeof (MMModulePrivate));
}

static void
mm_module_init (MMModule *module)
{
  MMModulePrivate *details =  module->details = MM_MODULE_GET_PRIVATE (module);

  details->filename = NULL;
  details->library = NULL;
  details->name = NULL;
}

static gboolean
mm_module_load (GTypeModule *gtm)
{
  MMModule *module = MM_MODULE (gtm);
  MMModulePrivate *details = module->details;
  gpointer m_initialize, m_shutdown, m_get_types, m_get_name;

  details->library = g_module_open (details->filename,
                                    G_MODULE_BIND_LAZY |
                                    G_MODULE_BIND_LOCAL);
  if (!details->library) {
    g_warning ("error while loading module %s: %s",
               details->filename, g_module_error ());
    return FALSE;
  }

  if (!g_module_symbol (details->library,
                        "mm_module_initialize",
                        &m_initialize) ||
      !g_module_symbol (details->library,
                        "mm_module_shutdown",
                        &m_shutdown) ||
      !g_module_symbol (details->library,
                        "mm_module_get_types",
                        &m_get_types) ||
      !g_module_symbol (details->library,
                        "mm_module_get_name",
                        &m_get_name)) {
    g_warning ("Unable to resolve symbols inside the module %s: %s",
               details->filename, g_module_error ());
    g_module_close (details->library);

    return FALSE;                      
  }

  module->initialize = m_initialize;
  module->shutdown = m_shutdown;
  module->get_types = m_get_types;
  module->get_name = m_get_name;

  module->initialize (gtm);

  return TRUE;
}

static void
mm_module_unload (GTypeModule *gtm)
{
	MMModule *module = MM_MODULE (gtm);

  module->shutdown ();

  g_module_close (module->details->library);
  
  module->initialize = NULL;
  module->shutdown = NULL;
  module->get_types = NULL;
}

static GObject *
mm_module_create_provider (MMModule *module, GType type)
{
  return g_object_new (type, NULL);
}

static void
mm_module_add_types (MMModule *module, GType *types)
{
  /* types[0] implements MMApplicationProvider
   * types[1] implements MMCategoryProvider
   * types[2] implements MMHitCollectionProvider
   */
  MMModulePrivate *details = module->details;

  details->app_provider = 
    MM_APPLICATION_PROVIDER (mm_module_create_provider (module, types[0]));
  details->category_provider = 
    MM_CATEGORY_PROVIDER (mm_module_create_provider (module, types[1]));
  details->collection_provider = 
    MM_HIT_COLLECTION_PROVIDER (mm_module_create_provider (module, types[2]));
}

static void
add_module_objects (MMModule *module)
{
  GType *gtypes;

  module->get_types (&gtypes);

  mm_module_add_types (module, gtypes);
}

/* public methods */

/**
 * mm_module_load_file:
 * @path: the path of the module to load.
 *
 * Creates a #MMModule object for the specified path.
 *
 * Return value: a #MMModule object or %NULL.
 */

MMModule *
mm_module_load_file (const char *path)
{
  MMModule *module;
  
  module = g_object_new (MM_TYPE_MODULE, NULL);
  module->details->filename = g_strdup (path);

  if (g_type_module_use (G_TYPE_MODULE (module))) {
    add_module_objects (module);
    g_type_module_unuse (G_TYPE_MODULE (module));
    return module;
  } else {
    g_object_unref (module);
    return NULL;
  }
}

/**
 * mm_module_get_application_provider:
 * @module: a #MMModule.
 *
 * Gets the #MMApplicationProvider for the module.
 *
 * Return value: a #MMApplicationProvider object or %NULL.
 */

MMApplicationProvider *
mm_module_get_application_provider (MMModule *module)
{
  g_return_val_if_fail (module != NULL, NULL);

  return module->details->app_provider;
}

/**
 * mm_module_get_category_provider:
 * @module: a #MMModule.
 *
 * Gets the #MMCategoryProvider for the module.
 *
 * Return value: a #MMCategoryProvider object or %NULL.
 */

MMCategoryProvider *
mm_module_get_category_provider (MMModule *module)
{
  g_return_val_if_fail (module != NULL, NULL);

  return module->details->category_provider;
}

/**
 * mm_module_get_hit_collection_provider:
 * @module: a #MMModule.
 *
 * Gets the #MMHitCollectionProvider for the module.
 *
 * Return value: a #MMHitCollectionProvider object or %NULL.
 */

MMHitCollectionProvider *
mm_module_get_hit_collection_provider (MMModule *module)
{
  g_return_val_if_fail (module != NULL, NULL);

  return module->details->collection_provider;
}

/**
 * mm_module_get_name:
 * @module: a #MMModule.
 *
 * Gets the name of the module.
 *
 * Return value: a string containing the module name.
 */

char *
mm_module_get_name (MMModule *module)
{
  char *name;

  module->get_name (&name);

  return name;
}
