/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <glib-object.h>

#include "mm-hit-collection.h"

#define MM_HIT_COLLECTION_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_HIT_COLLECTION, MMHitCollectionPrivate))

G_DEFINE_TYPE (MMHitCollection, mm_hit_collection, G_TYPE_OBJECT);

struct _MMHitCollectionPrivate {
  GPtrArray *hits;
  guint current_offset;
};

static void
unref_hit (gpointer hit,
           gpointer unused)
{
  g_object_unref (hit);
}

static void
mm_hit_collection_finalize (GObject *o)
{
  MMHitCollection *c = MM_HIT_COLLECTION (o);

  /* unref all the hits first */
  g_ptr_array_foreach (c->details->hits, unref_hit, NULL);
  g_ptr_array_free (c->details->hits, FALSE);

  G_OBJECT_CLASS (mm_hit_collection_parent_class)->finalize (o);
}

static void
mm_hit_collection_class_init (MMHitCollectionClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = mm_hit_collection_finalize;

  g_type_class_add_private (klass, sizeof (MMHitCollectionPrivate));
}

static void
mm_hit_collection_init (MMHitCollection *c)
{
  MMHitCollectionPrivate *details = c->details = MM_HIT_COLLECTION_GET_PRIVATE (c);

  details->hits = g_ptr_array_new ();
  details->current_offset = 0;
}

/* public methods */

/**
 * mm_hit_collection_new:
 *
 * Builds an empty #MMHitCollection.
 *
 * Return value: an empty #MMHitCollection.
 */

MMHitCollection *
mm_hit_collection_new (void)
{
  MMHitCollection *c;

  c = g_object_new (MM_TYPE_HIT_COLLECTION, NULL);

  return c;
}

/**
 * mm_hit_collection_add_hit:
 * @collection: a #MMHitCollection.
 * @hit: a #MMHit.
 *
 * Adds @hit to @collection. The collection will own a reference to the
 * added hit.
 */

void
mm_hit_collection_add_hit (MMHitCollection *collection,
                           MMHit *hit)
{
  MMHitCollectionPrivate *details = collection->details;

  g_ptr_array_add (details->hits, g_object_ref (hit));
}

/**
 * mm_hit_collection_get_next_hit:
 * @collection: a #MMHitCollection.
 *
 * Gets a reference to the next hit inside @collection, or %NULL if there are no hits
 * left.
 *
 * Return value: a #MMHit or %NULL. Use #g_object_unref when done with the hit.
 */

MMHit *
mm_hit_collection_get_next_hit (MMHitCollection *collection)
{
  MMHitCollectionPrivate *details = collection->details;
  MMHit *hit;

  if (details->hits->len == 0 ||
      details->current_offset >= details->hits->len) {
    /* no hits in this collection or we already got all results */
    return NULL;
  } else {
    hit = g_ptr_array_index (details->hits, details->current_offset);
    details->current_offset++;
  }

  return g_object_ref (hit);
}
