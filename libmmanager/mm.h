/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_H__
#define __MM_H__

#include "mm-application-provider.h"
#include "mm-application.h"
#include "mm-attribute-base-manager.h"
#include "mm-attribute-manager.h"
#include "mm-attribute.h"
#include "mm-base-attributes.h"
#include "mm-category-provider.h"
#include "mm-category.h"
#include "mm-error.h"
#include "mm-filter-param.h"
#include "mm-hit-collection-provider.h"
#include "mm-hit.h"
#include "mm-manager.h"
#include "mm-module-manager.h"
#include "mm-module.h"
#include "mm-so-application.h"
#include "mm-so-category.h"
#include "mm-string-utils.h"
#include "mm-types.h"
#include "mm-utils.h"

#endif /* __MM_H__ */
