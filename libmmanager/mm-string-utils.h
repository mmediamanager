/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_STRING_UTILS_H__
#define __MM_STRING_UTILS_H__

#include <glib.h>
#include "mm-filter.h"
#include "mm-hit-collection.h"

/**
 * SECTION:mm-string-utils
 * @short_description: string utils.
 *
 * Utils to convert libmmanager and #GValue objects to string and viceversa.
 */

char *            mm_filter_serialize           (MMFilter *filter, GError **error);
MMFilter *        mm_filter_unserialize         (const char *s, GError **error);
char *            mm_hit_collection_serialize   (MMHitCollection *hc, GError **error);
MMHitCollection * mm_hit_collection_unserialize (const char *s, GError **error);
char *            mm_gvalue_to_string           (GValue *v);
gboolean          mm_gvalue_from_string         (const char *val_string, GType type,
                                                 GValue *v);

#endif /* __MM_STRING_UTILS_H__ */
