/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include <glib.h>

#include "mm-application.h"
#include "mm-application-provider.h"
#include "mm-category-provider.h"
#include "mm-module-manager.h"
#include "mm-manager.h"
#include "mm-type-builtins.h"

#define MM_APPLICATION_GET_PRIVATE(o) \
    (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_APPLICATION, MMApplicationPrivate))

G_DEFINE_TYPE (MMApplication, mm_application, G_TYPE_OBJECT);

enum {
  PROP_0,
  PROP_SUPPORTED_TYPE
};

struct _MMApplicationPrivate {
  GDesktopAppInfo *info;
  char *id;
  MMApplicationType supported_type;
};

static void
mm_application_finalize (GObject *o)
{
  g_object_unref (MM_APPLICATION (o)->details->info);
  g_free (MM_APPLICATION (o)->details->id);

  G_OBJECT_CLASS (mm_application_parent_class)->finalize (o);
}

static void
mm_application_get_property (GObject *o,
                             guint prop_id,
                             GValue *value,
                             GParamSpec *pspec)
{
  MMApplicationPrivate *details = MM_APPLICATION (o)->details;

  switch (prop_id) {
  case PROP_SUPPORTED_TYPE:
    g_value_set_enum (value, details->supported_type);
    break;
  default:
    break;
  }
}

static void
mm_application_set_property (GObject *o,
                             guint prop_id,
                             const GValue *value,
                             GParamSpec *pspec)
{
  MMApplicationPrivate *details = MM_APPLICATION (o)->details;

  switch (prop_id) {
  case PROP_SUPPORTED_TYPE:
    details->supported_type = g_value_get_enum (value);
    break;
  default:
    break;
  }
}

static void
mm_application_class_init (MMApplicationClass *klass)
{
  GObjectClass *oclass = G_OBJECT_CLASS (klass);

  oclass->get_property = mm_application_get_property;
  oclass->set_property = mm_application_set_property;
  oclass->finalize = mm_application_finalize;

  g_object_class_install_property  (oclass,
                                    PROP_SUPPORTED_TYPE,
                                    g_param_spec_enum ("supported-type",
                                                       "Supported Type",
                                                       "Media type supported by the application",
                                                       MM_TYPE_MAPPLICATION_TYPE,
                                                       MM_APPLICATION_NONE,
                                                       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (klass, sizeof (MMApplicationPrivate));
}

static void
mm_application_init (MMApplication *a)
{
  MMApplicationPrivate *details = a->details = MM_APPLICATION_GET_PRIVATE (a);

  details->info = NULL;
  details->supported_type = MM_APPLICATION_NONE;
}

/* public methods */

/**
 * mm_application_set_attributes:
 * @application: a #MMApplication.
 * @desktop_id: the desktop file identifier of the implementing application.
 * @supported_type: the multimedia format supported by @application.
 * @id: an unique identifier for @application, e.g. its name.
 *
 * Sets all the properties on @application to the specified values. This is
 * useful only for implementations.
 */

void
mm_application_set_attributes (MMApplication *application,
                               const char *desktop_id,
                               MMApplicationType supported_type,
                               const char *id)
{
  g_object_set (application,
                "supported-type", supported_type,
                NULL);

  application->details->info = g_desktop_app_info_new (desktop_id);
  application->details->id = g_strdup (id);
}

/**
 * mm_application_get_app_info:
 * @application: a #MMApplication.
 *
 * Gets the #GAppInfo object corresponding to @application.
 *
 * Return value: a #GAppInfo containing the information about @application.
 */

GAppInfo *
mm_application_get_app_info (MMApplication *application)
{
  g_return_val_if_fail (application != NULL, NULL);
  g_return_val_if_fail (MM_IS_APPLICATION (application), NULL);

  return g_object_ref (G_APP_INFO (application->details->info));
}

/**
 * mm_application_get_id:
 * @application: a #MMApplication.
 *
 * Gets the unique identifier of @application.
 *
 * Return value: a string containing the unique identifier of @application.
 */

const char *
mm_application_get_id (MMApplication *application)
{
  g_return_val_if_fail (application != NULL, NULL);
  g_return_val_if_fail (MM_IS_APPLICATION (application), NULL);

  return application->details->id;
}

/**
 * mm_application_get_categories:
 * @application: a #MMApplication.
 * @error: a #GError.
 *
 * Gets a list of all the #MMCategory objects exposed by @application.
 *
 * Return value: a #GList of #MMCategory objects. Use #g_list_free when done.
 */

GList *
mm_application_get_categories (MMApplication *application, GError **error)
{
  g_return_val_if_fail (application != NULL, NULL);
  g_return_val_if_fail (MM_IS_APPLICATION (application), NULL);

  /* return NULL if we can't find the method in the subclass */
  return (MM_APPLICATION_CLASS (G_OBJECT_GET_CLASS (application))->get_categories == NULL) ?
    NULL : ((* MM_APPLICATION_CLASS (G_OBJECT_GET_CLASS (application))->get_categories)
            (application, error));
}
