/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_FILTER_PARAM_H__
#define __MM_FILTER_PARAM_H__
 
#include <glib.h>
#include "mm-types.h"
#include "mm-attribute.h"

#define MM_TYPE_FILTER_PARAM            (mm_filter_param_get_type())
#define MM_FILTER_PARAM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                         MM_TYPE_FILTER_PARAM, MMFilterParam))
#define MM_FILTER_PARAM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                         MM_TYPE_FILTER_PARAM, MMFilterParamClass))
#define MM_IS_FILTER_PARAM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                         MM_TYPE_FILTER_PARAM))
#define MM_IS_FILTER_PARAM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                         MM_TYPE_FILTER_PARAM))
#define MM_FILTER_PARAM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                         MM_TYPE_FILTER_PARAM, MMFilterParamClass))

/**
 * SECTION:mm-filter-param
 * @short_description: parameter for a #MMFilter.
 *
 * #MMFilterParam is an object identifying a parameter for a #MMFilter.
 * Thus to make a query, you have to build #MMFilterParam objects and add them
 * to a #MMFilter. To create a #MMFilterParam object, you should provide a
 * #MMAttribute, a #GValue and a #MMComparisionOperator which specifies the
 * relation between them. #MMFilterParam objects are reusable, i.e. you can add
 * the same #MMFilterParam to more than a filter.
 */

/**
 * MMFilterParam:
 *
 * A parameter for a #MMFilter.
 */

typedef struct _MMFilterParam        MMFilterParam;
typedef struct _MMFilterParamPrivate MMFilterParamPrivate;
typedef struct _MMFilterParamClass   MMFilterParamClass;

struct _MMFilterParam {
  GObject parent;

  MMFilterParamPrivate *details;
};

struct _MMFilterParamClass {
  GObjectClass parent_class;
};

/* public methods */

GType                 mm_filter_param_get_type        (void);

MMFilterParam *       mm_filter_param_new             (MMAttribute          *attribute,
                                                       GValue               *value,
                                                       MMComparisionOperator op);
void                  mm_filter_param_set_attribute   (MMFilterParam        *filter_param,
                                                       MMAttribute          *attribute);
void                  mm_filter_param_set_comparision (MMFilterParam        *filter_param,
                                                       MMComparisionOperator op);
void                  mm_filter_param_set_value       (MMFilterParam        *filter_param,
                                                       GValue               *value);
GValue *              mm_filter_param_get_value       (MMFilterParam        *filter_param);
MMAttribute *         mm_filter_param_get_attribute   (MMFilterParam        *filter_param);
MMComparisionOperator mm_filter_param_get_operator    (MMFilterParam        *filter_param);
void                  mm_filter_param_clear           (MMFilterParam        *filter_param);
                                               
#endif /* __MM_FILTER_PARAM_H__ */
