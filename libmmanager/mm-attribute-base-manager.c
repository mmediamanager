/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>

#include "mm-attribute-manager.h"
#include "mm-attribute-base-manager.h"
#include "mm-base-attributes.h"

static MMAttributeManager *base_manager = NULL;
static GHashTable *cached_attrs = NULL;

static void mm_attribute_manager_iface_init (MMAttributeManagerIface *iface);

G_DEFINE_TYPE_WITH_CODE (MMAttributeBaseManager, mm_attribute_base_manager,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (MM_TYPE_ATTRIBUTE_MANAGER,
                                                mm_attribute_manager_iface_init));
/* base attributes */

static const char * attribute_ids [] = {
  MM_ATTRIBUTE_BASE_URI,
  MM_ATTRIBUTE_BASE_NAME
};

static const GType attribute_types [] = {
  G_TYPE_STRING,
  G_TYPE_STRING
};

static const char * attribute_names [] = {
  N_("URI"),
  N_("Name")
};

static const char * attribute_descriptions [] = {
  N_("URI of the resource"),
  N_("Name of the resource")
};

/* MMAttributeManager interface implementation */

static MMAttribute *
impl_lookup_attribute (MMAttributeManager *m,
                       const char *id)
{
  int idx;
  gboolean found = FALSE;
  MMAttribute *attr;
  
  for (idx = 0; idx < G_N_ELEMENTS (attribute_ids); idx++) {
    if (g_strcmp0 (attribute_ids[idx], id) == 0) {
      found = TRUE;
      break;
    }
  }

  if (found) {
    attr = g_hash_table_lookup (cached_attrs,
                                attribute_ids[idx]);
    if (!attr) {
      attr = mm_attribute_new (attribute_types[idx],
                               attribute_ids[idx],
                               attribute_names[idx],
                               attribute_descriptions[idx]);
      g_hash_table_insert (cached_attrs, (char *) attribute_ids [idx],
                           attr);
    }
    return attr;
  } else {
    return NULL;
  }
}

static GList *
impl_get_supported_attributes (MMAttributeManager *m)
{
  GList *attributes = NULL;
  int idx;
  MMAttribute *attr;

  for (idx = 0; idx < G_N_ELEMENTS (attribute_ids); idx++) {
    attr = g_hash_table_lookup (cached_attrs,
                                attribute_ids[idx]);
    if (!attr) {
      attr = mm_attribute_new (attribute_types[idx],
                               attribute_ids[idx],
                               attribute_names[idx],
                               attribute_descriptions[idx]);
      g_hash_table_insert (cached_attrs, (char *) attribute_ids[idx],
                           attr);
    }
    attributes = g_list_prepend (attributes, attr);
  }

  return g_list_reverse (attributes);
}

static void
mm_attribute_manager_iface_init (MMAttributeManagerIface *iface)
{
  iface->lookup_attribute = impl_lookup_attribute;
  iface->get_supported_attributes = impl_get_supported_attributes;
}

static void
mm_attribute_base_manager_finalize (GObject *o)
{
  g_hash_table_destroy (cached_attrs);

  G_OBJECT_CLASS (mm_attribute_base_manager_parent_class)->finalize (o);
}

static void
mm_attribute_base_manager_init (MMAttributeBaseManager *abm)
{
  if (!cached_attrs) {
    cached_attrs = g_hash_table_new_full (g_str_hash, g_str_equal,
                                          NULL, (GDestroyNotify) mm_attribute_free);
  } 
}

static void
mm_attribute_base_manager_class_init (MMAttributeBaseManagerClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = mm_attribute_base_manager_finalize;
}

/* public methods */

/**
 * mm_attribute_base_manager_get:
 *
 * Gets the #MMAttributeManager in charge of managing all the basic attributes,
 * like "name" or "URI". The attributes managed by this manager belong to the
 * "base" namespace and will have a "base::foo" id.
 *
 * Return value: a #MMAttributeManager. This is owned by the library and you
 * should not modify or unref it.
 */

MMAttributeManager *
mm_attribute_base_manager_get (void)
{
  if (!base_manager) {
    base_manager = MM_ATTRIBUTE_MANAGER (g_object_new (MM_TYPE_ATTRIBUTE_BASE_MANAGER, NULL));
  }

  return base_manager;
}
