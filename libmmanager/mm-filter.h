/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_FILTER_H__
#define __MM_FILTER_H__
 
#include <glib.h>

#include "mm-filter-param.h"

#define MM_TYPE_FILTER            (mm_filter_get_type())
#define MM_FILTER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                   MM_TYPE_FILTER, MMFilter))
#define MM_FILTER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                   MM_TYPE_FILTER, MMFilterClass))
#define MM_IS_FILTER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                   MM_TYPE_FILTER))
#define MM_IS_FILTER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                   MM_TYPE_FILTER))
#define MM_FILTER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                   MM_TYPE_FILTER, MMFilterClass))

/**
 * SECTION:mm-filter
 * @short_description: a filter.
 *
 * #MMFilter is an object representing a query. It is no more than a collection
 * of #MMFilterParam objects. You must pass a #MMFilter object to a #MMCategory
 * to get a #MMHitCollection.
 */

/**
 * MMFilter:
 *
 * A filter, i.e. an object representing a query.
 */

typedef struct _MMFilter        MMFilter;
typedef struct _MMFilterPrivate MMFilterPrivate;
typedef struct _MMFilterClass   MMFilterClass;

struct _MMFilter {
  GObject parent_object;
  MMFilterPrivate *details;
};

struct _MMFilterClass {
  GObjectClass parent_class;
};

/* public methods */
GType      mm_filter_get_type             (void);
void       mm_filter_add_filtering_param  (MMFilter        *filter,
                                           MMFilterParam   *fp);
void       mm_filter_add_filtering_params (MMFilter        *filter,
                                           GList           *fps);
void       mm_filter_clear                (MMFilter        *filter);
GList *    mm_filter_get_filtering_params (MMFilter        *filter);

MMFilter * mm_filter_new                  (void);

#endif /* __MM_FILTER_H__ */
