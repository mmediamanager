/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "mm-application.h"
#include "mm-dbus-application.h"
#include "mm-dbus-category.h"
#include "mm-error.h"
#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus-glib.h>

#define MM_DBUS_APPLICATION_GET_PRIVATE(o) \
    (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_DBUS_APPLICATION, MMDBusApplicationPrivate))

struct _MMDBusApplicationPrivate {
  char *path;
};

G_DEFINE_TYPE (MMDBusApplication, mm_dbus_application, MM_TYPE_APPLICATION);

static void
create_and_append_category (MMApplication *app,
                            GList *categories,
                            const char *name,
                            const char *icon_name)
{
  MMCategory *cat;

  cat = mm_dbus_category_new (MM_DBUS_APPLICATION (app), name, g_themed_icon_new (icon_name));
  categories = g_list_prepend (categories, cat);
}                            

static GList *
mm_dbus_application_get_categories (MMApplication *app, GError **ret_error)
{
  DBusGProxy *app_proxy;
  DBusGConnection *connection;
  char **cat_names, **cat_icon_names;
  gboolean ret;
  int idx;
  GList *categories = NULL;
  GError *error = NULL;
  const char *remote_name = mm_application_get_id (app);

  connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (!connection) {
    g_set_error (ret_error,
                 MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_BUS_UNAVAILABLE,
                 "Unable to get a connection to the session bus: %s", error->message);
    g_error_free (error);
    return NULL;
  }

  app_proxy = dbus_g_proxy_new_for_name (connection,
                                         remote_name,
                                         MM_DBUS_APPLICATION (app)->details->path,
                                         "org.gnome.MediaManager.Application");
  ret = dbus_g_proxy_call (app_proxy,
                           "GetCategories",
                           &error,
                           G_TYPE_INVALID,
                           G_TYPE_STRV,
                           &cat_names,
                           G_TYPE_STRV,
                           &cat_icon_names,
                           G_TYPE_INVALID);
  if (!ret) {
    g_set_error (ret_error,
                 MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_REMOTE_METHOD_FAILED,
                 "Error while calling GetCategories on %s: %s", remote_name,
                 error->message);
    g_error_free (error);
    goto out;
  }

  if (G_N_ELEMENTS (cat_names) != G_N_ELEMENTS (cat_icon_names)) {
    /* something must be broken, return */
    g_set_error (ret_error,
                 MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_REMOTE_METHOD_FAILED,
                 "Error in GetCategories on %s: the length of the returned "
                 "arrays do not match", remote_name);
    goto free;
  }

  /* create the category objects */
  for (idx = 0; idx < G_N_ELEMENTS (cat_names); idx++) {
    create_and_append_category (app, categories, cat_names[idx],
                                cat_icon_names[idx]);
  }

free:
  g_strfreev (cat_names);
  g_strfreev (cat_icon_names);

out:
  g_object_unref (app_proxy);
  return categories;
}

static void
mm_dbus_application_finalize (GObject *o)
{
  g_free (MM_DBUS_APPLICATION (o)->details->path);

  G_OBJECT_CLASS (mm_dbus_application_parent_class)->finalize (o);
}

static void
mm_dbus_application_class_init (MMDBusApplicationClass *klass)
{
  MMApplicationClass *app_class = MM_APPLICATION_CLASS (klass);

  app_class->get_categories = mm_dbus_application_get_categories;
  G_OBJECT_CLASS (klass)->finalize = mm_dbus_application_finalize;

  g_type_class_add_private (klass, sizeof (MMDBusApplicationPrivate));
}

static void
mm_dbus_application_init (MMDBusApplication *app)
{
  MMDBusApplicationPrivate *details = app->details = MM_DBUS_APPLICATION_GET_PRIVATE (app);
  details->path = NULL;
}

/* public methods */

/**
 * mm_dbus_application_new:
 * @desktop_id: the desktop file id for the application.
 * @supported_type: the media type supported by the application.
 * @name: the name of the application. This will be used as unique id.
 * @path: the path on the session bus of the requesting application.
 *
 * Builds a new #MMApplication object with the specified properties.
 *
 * Return value: a #MMApplication object.
 */

MMApplication *
mm_dbus_application_new (const char *desktop_id,
                         MMApplicationType supported_type,
                         const char *name,
                         const char *path)
{
  MMApplication *app;

  app = MM_APPLICATION (g_object_new (MM_TYPE_DBUS_APPLICATION, NULL));
  mm_application_set_attributes (app, desktop_id, supported_type, name);
  MM_DBUS_APPLICATION (app)->details->path = g_strdup (path);

  return app;
}

/**
 * mm_dbus_application_get_path:
 * @app: a #MMDBusApplication.
 *
 * Gets the path of the application on the session bus.
 *
 * Return value: a string.
 */

const char *
mm_dbus_application_get_path (MMDBusApplication *app)
{
  g_return_val_if_fail (MM_IS_DBUS_APPLICATION (app), NULL);

  return app->details->path;
}
