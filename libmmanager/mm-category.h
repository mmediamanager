/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_CATEGORY_H__
#define __MM_CATEGORY_H__

#include <glib.h>
#include "mm-hit-collection.h"
#include "mm-filter.h"
#include "mm-application.h"

#define MM_TYPE_CATEGORY            (mm_category_get_type())
#define MM_CATEGORY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                     MM_TYPE_CATEGORY, MMCategory))
#define MM_CATEGORY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                     MM_TYPE_CATEGORY, MMCategoryClass))
#define MM_IS_CATEGORY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                     MM_TYPE_CATEGORY))
#define MM_IS_CATEGORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                     MM_TYPE_CATEGORY))
#define MM_CATEGORY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                     MM_TYPE_CATEGORY, MMCategoryClass))

/**
 * SECTION:mm-category
 * @short_description: a category of a library.
 *
 * A category of an application's library. This could be e.g. "Last Roll"
 * for a photo application, a playlist for a video or music application or
 * something like "Podcasts". Each category has a name and an icon.
 */

/**
 * MMCategory:
 *
 * A basic type for a category object.
 */

typedef struct _MMCategory        MMCategory;
typedef struct _MMCategoryPrivate MMCategoryPrivate;
typedef struct _MMCategoryClass   MMCategoryClass;

struct _MMCategory {
  GObject parent_object;
  MMCategoryPrivate *details;
};

/**
 * MMCategoryClass:
 * @get_hits: Gets a #MMHitCollection for the specified filter.
 *
 * Class of a #MMCategory object.
 */

struct _MMCategoryClass {
  GObjectClass parent_class;

  MMHitCollection * (* get_hits) (MMCategory  *category,
                                  MMFilter    *filter,
                                  GError     **error);
};

GType             mm_category_get_type        (void);

void              mm_category_set_attributes  (MMCategory    *category,
                                               MMApplication *app,
                                               const char    *name,
                                               GIcon         *icon);
GIcon*            mm_category_get_icon        (MMCategory    *category);
const char*       mm_category_get_name        (MMCategory    *category);
MMApplication *   mm_category_get_application (MMCategory    *category);
MMHitCollection*  mm_category_get_hits        (MMCategory    *category,
                                               MMFilter      *filter,
                                               GError       **error);

#endif /* __MM_CATEGORY_H__ */
