/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_ATTRIBUTE_BASE_MANAGER_H__
#define __MM_ATTRIBUTE_BASE_MANAGER_H__

#include "mm-attribute-manager.h"
#include "mm-base-attributes.h"
#include <glib.h>
#include <glib-object.h>

#define MM_TYPE_ATTRIBUTE_BASE_MANAGER            (mm_attribute_base_manager_get_type())
#define MM_ATTRIBUTE_BASE_MANAGER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                                   MM_TYPE_ATTRIBUTE_BASE_MANAGER, MMAttributeBaseManager))
#define MM_ATTRIBUTE_BASE_MANAGER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                                   MM_TYPE_ATTRIBUTE_BASE_MANAGER, MMAttributeBaseManagerClass))
#define MM_IS_ATTRIBUTE_BASE_MANAGER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                                   MM_TYPE_ATTRIBUTE_BASE_MANAGER))
#define MM_IS_ATTRIBUTE_BASE_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                                   MM_TYPE_ATTRIBUTE_BASE_MANAGER))
#define MM_ATTRIBUTE_BASE_MANAGER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                                   MM_TYPE_ATTRIBUTE_BASE_MANAGER, MMAttributeBaseManagerClass))

/**
 * SECTION:mm-attribute-base-manager
 * @short_description: manager implementation for base attributes.
 *
 * Implementation of a #MMAttributeManager for basic attributes. Currently the
 * only supported attributes are "uri" and "name", but this could be easily
 * extended with basically no effort. The #MMAttribute<!-- -->s managed by
 * #MMAttributeBaseManager have the prefix "base" in their id.
 */

/**
 * MMAttributeBaseManager:
 *
 * An implementation of the #MMAttributeMangaer interface for basic attributes.
 */

typedef struct _MMAttributeBaseManager MMAttributeBaseManager;
typedef struct _MMAttributeBaseManagerClass MMAttributeBaseManagerClass;

struct _MMAttributeBaseManager {
  GObject parent;
};

struct _MMAttributeBaseManagerClass {
  GObjectClass parent_class;
};

/* public methods */

GType                    mm_attribute_base_manager_get_type (void);
MMAttributeManager *     mm_attribute_base_manager_get      (void);

#endif /* __MM_ATTRIBUTE_BASE_MANAGER_H__ */
