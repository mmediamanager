/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_TYPES_H__
#define __MM_TYPES_H__

/**
 * SECTION:mm-types
 * @short_description: enum types.
 *
 * Enumeration types used across the library when needed.
 */

/**
 * MMApplicationType:
 * @MM_APPLICATION_NONE: initializer.
 * @MM_APPLICATION_PHOTO: application supports photo management.
 * @MM_APPLICATION_MUSIC: application supports music management.
 * @MM_APPLICATION_VIDEO: application supports video management.
 *
 * Media types that applications can support.
 */

typedef enum {
  MM_APPLICATION_NONE,
  MM_APPLICATION_PHOTO,
  MM_APPLICATION_MUSIC,
  MM_APPLICATION_VIDEO
} MMApplicationType;

/**
 * MMComparisionOperator:
 * @MM_COMP_NONE: initializer.
 * @MM_COMP_EQUAL: equal to.
 * @MM_COMP_GREATER: greater than.
 * @MM_COMP_LESS: less than.
 * @MM_COMP_GREATER_EQUAL: greater or equal than.
 * @MM_COMP_LESS_EQUAL: less or equal than.
 *
 * Comparision operators.
 */

typedef enum {
  MM_COMP_NONE,
  MM_COMP_EQUAL,
  MM_COMP_GREATER,
  MM_COMP_LESS,
  MM_COMP_GREATER_EQUAL,
  MM_COMP_LESS_EQUAL
} MMComparisionOperator;

#endif /* __MM_TYPES_H__ */
