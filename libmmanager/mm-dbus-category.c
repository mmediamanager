/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus-glib.h>
#include "mm-dbus-category.h"
#include "mm-category.h"
#include "mm-dbus-application.h"
#include "mm-string-utils.h"
#include "mm-error.h"

G_DEFINE_TYPE (MMDBusCategory, mm_dbus_category, MM_TYPE_CATEGORY);

static MMHitCollection *
mm_dbus_category_get_hits (MMCategory *c,
                           MMFilter *f,
                           GError **ret_error)
{
  DBusGConnection *connection;
  DBusGProxy *app_proxy;
  MMApplication *app;
  MMHitCollection *hit_collection = NULL;
  GError *error = NULL;
  gboolean ret;
  char *serial_filter;
  char *serial_hit_collection = NULL;

  g_return_val_if_fail (c != NULL, NULL);
  g_return_val_if_fail (MM_IS_CATEGORY (c), NULL);
  g_return_val_if_fail (f != NULL, NULL);
  g_return_val_if_fail (MM_IS_FILTER (f), NULL);

  connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (!connection) {
    g_set_error (ret_error,
                 MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_BUS_UNAVAILABLE,
                 "Unable to get a connection to the session bus: %s", error->message);
    g_error_free (error);
    return NULL;
  }

  app = mm_category_get_application (c);
  app_proxy = dbus_g_proxy_new_for_name (connection,
                                         mm_application_get_id (app),
                                         mm_dbus_application_get_path (MM_DBUS_APPLICATION (app)),
                                         "org.gnome.MediaManager.Application");
  serial_filter = mm_filter_serialize (f, &error);
  if (error) {
    g_set_error (ret_error,
                 MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_SERIALIZE_FAIL,
                 "Unable to serialize the filter object: %s", error->message);
    g_error_free (error);
    goto out;
  }
  ret = dbus_g_proxy_call (app_proxy,
                           "GetHits",
                           &error,
                           G_TYPE_STRING,
                           mm_category_get_name (c),
                           G_TYPE_STRING,
                           serial_filter,
                           G_TYPE_INVALID,
                           G_TYPE_STRING,
                           &serial_hit_collection,
                           G_TYPE_INVALID);
  if (!ret) {
    g_set_error (ret_error,
                 MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_REMOTE_METHOD_FAILED,
                 "Error while calling GetHits on %s: %s", mm_application_get_id (app),
                 error->message);
    g_error_free (error);
    goto out;
  }

  hit_collection = mm_hit_collection_unserialize (serial_hit_collection, &error);
  if (error) {
    g_set_error (ret_error,
                 MM_DBUS_ERROR_QUARK,
                 MM_DBUS_ERROR_UNSERIALIZE_FAIL,
                 "Unable to unserialize the HitCollection: %s", error->message);
    g_error_free (error);
    goto out;
  }

out:
  g_free (serial_filter);
  g_free (serial_hit_collection);
  g_object_unref (app_proxy);
  g_object_unref (app);

  return hit_collection;
}

static void
mm_dbus_category_class_init (MMDBusCategoryClass *klass)
{
  MMCategoryClass *cat_class = MM_CATEGORY_CLASS (klass);

  cat_class->get_hits = mm_dbus_category_get_hits;
}

static void
mm_dbus_category_init (MMDBusCategory *cat)
{
  /* empty */
}

/* public methods */

/**
 * mm_dbus_category_new:
 * @app: a #MMDBusApplication.
 * @name: the name of the category.
 * @icon: the icon of the category.
 *
 * Builds a new #MMCategory with the specified name and icon. The new object
 * will own a reference to both the application and the icon.
 *
 * Return value: a #MMCategory.
 */

MMCategory *
mm_dbus_category_new (MMDBusApplication *app,
                      const char *name,
                      GIcon *icon)
{
  MMCategory *category;

  category = MM_CATEGORY (g_object_new (MM_TYPE_DBUS_CATEGORY, NULL));
  mm_category_set_attributes (category, MM_APPLICATION (app), name, icon);

  return category;
}
