/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib-object.h>
#include <glib.h>

#include "mm-category.h"
#include "mm-hit-collection-provider.h"
#include "mm-manager.h"
#include "mm-filter.h"


#define MM_CATEGORY_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_CATEGORY, MMCategoryPrivate))

G_DEFINE_TYPE (MMCategory, mm_category, G_TYPE_OBJECT);

struct _MMCategoryPrivate {
  char *name;
  GIcon *icon;
  MMApplication *application;
};

static void
mm_category_finalize (GObject *o)
{
  MMCategoryPrivate *details = MM_CATEGORY (o)->details;

  g_free (details->name);

  if (details->icon) {
    g_object_unref (details->icon);
  }
  if (details->application) {
    g_object_unref (details->application);
  }

  G_OBJECT_CLASS (mm_category_parent_class)->finalize (o);  
}

static void
mm_category_class_init (MMCategoryClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = mm_category_finalize;

  g_type_class_add_private (klass, sizeof (MMCategoryPrivate));
}

static void
mm_category_init (MMCategory *c)
{
  MMCategoryPrivate *details = c->details = MM_CATEGORY_GET_PRIVATE (c);

  details->name = NULL;
  details->icon = NULL;
  details->application = NULL;
}

/* public methods */

/**
 * mm_category_set_attributes:
 * @category: a #MMCategory.
 * @app: a #MMApplication.
 * @name: a string containing the name for the category.
 * @icon: a #GIcon containing the icon for the category.
 *
 * Sets all the specified properties and attributes to @category. This is useful
 * only for #MMCategory implementations.
 */

void
mm_category_set_attributes (MMCategory *category,
                            MMApplication *app,
                            const char *name,
                            GIcon *icon)
{
  MMCategoryPrivate *details = category->details;

  details->name = g_strdup (name);
  details->application = g_object_ref (app);
  if (icon) {
    details->icon = g_object_ref (icon);
  }
}

/**
 * mm_category_get_icon:
 * @category: a #MMCategory.
 *
 * Gets the category's icon.
 *
 * Return value: a #GIcon containing the icon of the category or %NULL. Unref it
 * when done.
 */

GIcon *
mm_category_get_icon (MMCategory *category)
{
  g_return_val_if_fail (category != NULL, NULL);
  g_return_val_if_fail (MM_IS_CATEGORY (category), NULL);

  return category->details->icon ? g_object_ref (category->details->icon) : NULL;
}

/**
 * mm_category_get_name:
 * @category: a #MMCategory.
 *
 * Gets the name of the category.
 *
 * Return value: a string containing the name of the category.
 */

const char *
mm_category_get_name (MMCategory *category)
{
  g_return_val_if_fail (category != NULL, NULL);
  g_return_val_if_fail (MM_IS_CATEGORY (category), NULL);

  return category->details->name;
}

/**
 * mm_category_get_hits:
 * @category: a #MMCategory.
 * @filter: a #MMFilter.
 * @error: a #GError.
 *
 * Gets a #MMHitCollection of all the hits contained in @category and satisfying the
 * requirements of @filter.
 *
 * Return value: a #MMHitCollection or %NULL (in this case, the #GError is set).
 */

MMHitCollection *
mm_category_get_hits (MMCategory *category,
                      MMFilter *filter,
                      GError **error)
{
  g_return_val_if_fail (category != NULL, NULL);
  g_return_val_if_fail (filter != NULL, NULL);
  g_return_val_if_fail (MM_IS_CATEGORY (category), NULL);
  g_return_val_if_fail (MM_IS_FILTER (filter), NULL);

  /* return NULL if we can't find the method in the subclass */
  return (MM_CATEGORY_CLASS (G_OBJECT_GET_CLASS (category))->get_hits == NULL) ?
    NULL : ((* MM_CATEGORY_CLASS (G_OBJECT_GET_CLASS (category))->get_hits)
            (category, filter, error));
}

/**
 * mm_category_get_application:
 * @category: a #MMCategory.
 *
 * Gets the #MMApplication to which @category belong.
 *
 * Return value: a #MMApplication. Unref it when done.
 */

MMApplication *
mm_category_get_application (MMCategory *category)
{
  g_return_val_if_fail (category != NULL, NULL);
  g_return_val_if_fail (MM_IS_CATEGORY (category), NULL);

  return g_object_ref (category->details->application);
}
