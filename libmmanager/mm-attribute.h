/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_ATTRIBUTE_H__
#define __MM_ATTRIBUTE_H__

#include <glib.h>
#include <glib-object.h>

/**
 * SECTION:mm-attribute
 * @short_description: an attribute.
 *
 * Structure defining an attribute which the #MMCollection can be queried on.
 * This structure is used both to build #MMFilterParam and #MMHit objects.
 * The attributes should always be static and owned by the library, and you
 * should use a #MMAttributeManager (either a stock one or a custom
 * implementation of the interface) to get the #MMAttribute.
 */

/**
 * MMAttribute:
 *
 * An opaque structure identifying an attribute.
 */

typedef struct _MMAttribute      MMAttribute;

/* public methods */

MMAttribute * mm_attribute_new             (GType type,
                                            const char *id,
                                            const char *name,
                                            const char *description);
void          mm_attribute_free            (MMAttribute *attr);
const char *  mm_attribute_get_id          (MMAttribute *attr);
const char *  mm_attribute_get_name        (MMAttribute *attr);
const char *  mm_attribute_get_description (MMAttribute *attr);
GType         mm_attribute_get_value_type  (MMAttribute *attr);

#endif /* __MM_ATTRIBUTE_H__ */
