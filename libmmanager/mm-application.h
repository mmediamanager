/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_APPLICATION_H__
#define __MM_APPLICATION_H__
 
#include <glib.h>
#include <gio/gdesktopappinfo.h>
#include "mm-types.h"

#define MM_TYPE_APPLICATION            (mm_application_get_type())
#define MM_APPLICATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                        MM_TYPE_APPLICATION, MMApplication))
#define MM_APPLICATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                        MM_TYPE_APPLICATION, MMApplicationClass))
#define MM_IS_APPLICATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                        MM_TYPE_APPLICATION))
#define MM_IS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                        MM_TYPE_APPLICATION))
#define MM_APPLICATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                        MM_TYPE_APPLICATION, MMApplicationClass))
/**
 * SECTION:mm-application
 * @short_description: an application object.
 *
 * An object which identifies an application inside the library. This object
 * has two implementations: #MMSoApplication and #MMDBusApplication, which
 * allow a library client to implement the object either using #MMModule
 * or DBus facilities.
 */

/**
 * MMApplication:
 *
 * Basic type identifying an application object.
 */

typedef struct _MMApplication        MMApplication;
typedef struct _MMApplicationPrivate MMApplicationPrivate;
typedef struct _MMApplicationClass   MMApplicationClass;

struct _MMApplication {
  GObject parent_object;
  MMApplicationPrivate *details;
};

/**
 * MMApplicationClass:
 * @parent_class: the parent class.
 * @get_categories: gets all the categories inside the application.
 *
 * The class of a #MMApplication object.
 */

struct _MMApplicationClass {
  GObjectClass parent_class;

  GList *  (* get_categories) (MMApplication  *application,
                               GError        **error);
};

GType                 mm_application_get_type          (void);

GAppInfo*             mm_application_get_app_info      (MMApplication     *application);
const char *          mm_application_get_id            (MMApplication     *application);
void                  mm_application_set_attributes    (MMApplication     *application,
                                                        const char        *desktop_id,
                                                        MMApplicationType  supported_type,
                                                        const char        *id);
GList*                mm_application_get_categories    (MMApplication     *application,
                                                        GError           **error);

#endif /* __MM_APPLICATION_H__ */
