/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_CATEGORY_PROVIDER_H__
#define __MM_CATEGORY_PROVIDER_H__

#include <glib-object.h>

#include "mm-application.h"

#define MM_TYPE_CATEGORY_PROVIDER           (mm_category_provider_get_type ())
#define MM_CATEGORY_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                             MM_TYPE_CATEGORY_PROVIDER, MMCategoryProvider))
#define MM_IS_CATEGORY_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                             MM_TYPE_CATEGORY_PROVIDER))
#define MM_CATEGORY_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj),\
                                             MM_TYPE_CATEGORY_PROVIDER, MMCategoryProviderIface))

/**
 * SECTION:mm-category-provider
 * @short_description: provides #MMCategory objects.
 *
 * A handle to an object implementing the #MMCategoryProviderIface iterface.
 * You need this to get a list of #MMCategory objects for a #MMSoApplication,
 * i.e. if you're using #MMModule.
 */

/**
 * MMCategoryProvider:
 *
 * An abstract type that provides categories.
 */

typedef struct _MMCategoryProvider      MMCategoryProvider;
typedef struct _MMCategoryProviderIface MMCategoryProviderIface;

/**
 * MMCategoryProviderIface:
 * @interface: the parent interface.
 * @get_categories: Gets a #GList of #MMCategory objects provided by the 
 * application.
 *
 * Interface to build #MMCategory objects with #MMModule.
 */

struct _MMCategoryProviderIface {
  GTypeInterface interface;

  GList* (* get_categories) (MMCategoryProvider *provider,
                             MMApplication      *application);
};

/* Interface functions */
GType	        mm_category_provider_get_type        (void);
GList*        mm_category_provider_get_categories  (MMCategoryProvider *provider,
                                                    MMApplication      *application);

#endif /* __MM_CATEGORY_PROVIDER_H__ */
