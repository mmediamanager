/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_MODULE_H__
#define __MM_MODULE_H__

#include <glib-object.h>
#include <glib.h>

#include "mm-hit-collection-provider.h"
#include "mm-application-provider.h"
#include "mm-category-provider.h"

#define MM_TYPE_MODULE            (mm_module_get_type ())
#define MM_MODULE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                   MM_TYPE_MODULE, MMModule))
#define MM_MODULE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                   MM_TYPE_MODULE, MMModuleClass))
#define MM_IS_MODULE(obj)         (G_TYPE_INSTANCE_CHECK_TYPE ((obj),\
                                   MM_TYPE_MODULE))
#define MM_IS_MODULE_CLASS(klass) (G_TYPE_CLASS_CHECK_CLASS_TYPE ((klass),\
                                   MM_TYPE_MODULE))

/**
 * SECTION:mm-module
 * @short_description: module.
 *
 * #MMModule is a #GTypeModule for a registered application. An application
 * which wants to use the the library has to implement all the methods of the
 * interface, and create his own implementations for all the three providers.
 */

typedef struct _MMModule MMModule;
typedef struct _MMModulePrivate MMModulePrivate;
typedef struct _MMModuleClass MMModuleClass;

/**
 * MMModule:
 * @initialize: function that will be called to initialize the module.
 * @shutdown: function that will be called to shutdown the module.
 * @get_types: returns an array of the #GType of provider implementations.
 * @get_name: returns the module name.
 *
 * Class for extending libmmanager with new applications.
 */

struct _MMModule {
  GTypeModule parent;

  MMModulePrivate *details;

  /* module symbols here */
  void (* initialize) (GTypeModule *module);
  void (* shutdown)   (void);
  void (* get_types)  (GType      **gtypes);
  void (* get_name )  (char       **name);
};

struct _MMModuleClass {
  GTypeModuleClass parent_class;
};

/* public methods */
GType                      mm_module_get_type                    (void);
MMModule*                  mm_module_load_file                   (const char *path);

MMApplicationProvider*     mm_module_get_application_provider    (MMModule *module);
MMCategoryProvider*        mm_module_get_category_provider       (MMModule *module);
MMHitCollectionProvider*   mm_module_get_hit_collection_provider (MMModule *module);
char*                      mm_module_get_name                    (MMModule *module);

#endif /* __MM_MODULE_H__ */
