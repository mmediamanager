/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "mm-application.h"
#include "mm-so-application.h"
#include "mm-manager.h"
#include "mm-category-provider.h"
#include "mm-error.h"
#include <glib.h>
#include <glib-object.h>

G_DEFINE_TYPE (MMSoApplication, mm_so_application, MM_TYPE_APPLICATION);

static GList *
mm_so_application_get_categories (MMApplication *a, GError **ret_error)
{
  MMManager *manager;
  MMCategoryProvider *cat_provider;
  const char *app_id;

  g_return_val_if_fail (a != NULL, NULL);
  g_return_val_if_fail (MM_IS_APPLICATION (a), NULL);

  manager = mm_manager_get ();
  app_id = mm_application_get_id (a);
  cat_provider = mm_module_manager_get_category_provider_for_application (mm_manager_get_module_manager (manager),
                                                                          app_id);
  if (!cat_provider) {
    g_set_error (ret_error,
                 MM_SO_ERROR_QUARK,
                 MM_SO_ERROR_NULL_PROVIDER,
                 "Unable to get the category provider for application %s",
                 app_id);
    return NULL;
  }
 
  return mm_category_provider_get_categories (cat_provider, a);
}

static void
mm_so_application_class_init (MMSoApplicationClass *klass)
{
  MMApplicationClass *app_class = MM_APPLICATION_CLASS (klass);

  app_class->get_categories = mm_so_application_get_categories;
}

static void
mm_so_application_init (MMSoApplication *app)
{
  /* empty */
}

/* public methods */

/**
 * mm_so_application_new:
 * @desktop_id: the desktop file id for the application.
 * @supported_type: the media type supported by the application.
 * @id: an unique id identifying the application (e.g. its name).
 *
 * Builds a new #MMApplication object with the specified properties.
 *
 * Return value: a #MMApplication object.
 */

MMApplication *
mm_so_application_new (const char *desktop_id,
                       MMApplicationType supported_type,
                       const char *id)
{
  MMApplication *app;

  app = MM_APPLICATION (g_object_new (MM_TYPE_SO_APPLICATION, NULL));
  mm_application_set_attributes (app, desktop_id, supported_type, id);

  return app;
}
