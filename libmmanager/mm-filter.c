/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <glib-object.h>

#include "mm-filter.h"

#define MM_FILTER_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_FILTER, MMFilterPrivate))

struct _MMFilterPrivate {
  GList *filter_params;
};

G_DEFINE_TYPE (MMFilter, mm_filter, G_TYPE_OBJECT);

static void
unref_obj (gpointer data,
           gpointer _unused)
{
  g_object_unref (data);
}

static void
unref_all_fps (MMFilter *f)
{
  MMFilterPrivate *details = f->details;
  g_list_foreach (details->filter_params, unref_obj, NULL);
}

static void
add_to_filter_params (gpointer _fp,
                      gpointer _filter)
{
  MMFilter *filter = _filter;
  MMFilterParam *fp = _fp;

  mm_filter_add_filtering_param (filter, fp);
}

static void
mm_filter_finalize (GObject *o)
{
  unref_all_fps (MM_FILTER (o));
  g_list_free (MM_FILTER (o)->details->filter_params);

  G_OBJECT_CLASS (mm_filter_parent_class)->finalize (o);
}

static void
mm_filter_init (MMFilter *f)
{
  MMFilterPrivate *details = f->details = MM_FILTER_GET_PRIVATE (f);
  details->filter_params = NULL;
}

static void
mm_filter_class_init (MMFilterClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = mm_filter_finalize;

  g_type_class_add_private (klass, sizeof (MMFilterPrivate));
}

/* public methods */

/**
 * mm_filter_new:
 *
 * Builds an empty #MMFilter object.
 *
 * Return value: an empty #MMFilter.
 */

MMFilter *
mm_filter_new (void)
{
  return g_object_new (MM_TYPE_FILTER, NULL);
}

/**
 * mm_filter_add_filtering_param:
 * @filter: a #MMFilter
 * @fp: a #MMFilterParam
 *
 * Adds @fp to @filter. @filter will keep a reference to @fp.
 */

void
mm_filter_add_filtering_param (MMFilter *filter,
                               MMFilterParam *fp)
{
  MMFilterPrivate *details = filter->details;

  g_return_if_fail (MM_IS_FILTER_PARAM (fp));

  details->filter_params = g_list_prepend (details->filter_params, 
                                           g_object_ref (fp));
}

/**
 * mm_filter_add_filtering_params:
 * @filter: a #MMFilter.
 * @fps: a #GList of #MMFilterParam objects.
 *
 * Adds the #MMFilterParam objects inside the list to @filter. @filter
 * will keep a reference to all the #MMFilterParam objects.
 */

void
mm_filter_add_filtering_params (MMFilter *filter,
                                GList *fps)
{
  g_list_foreach (fps, add_to_filter_params, filter);
}

/**
 * mm_filter_clear:
 * @filter: a #MMFilter.
 *
 * Clears all the #MMFilterParam<!-- -->s from @filter.
 */

void
mm_filter_clear (MMFilter *filter)
{
  unref_all_fps (filter);
  g_list_free (filter->details->filter_params);
  filter->details->filter_params = NULL;
}

/**
 * mm_filter_get_filtering_params:
 * @filter: a #MMFilter.
 *
 * Gets a #GList of all the #MMFilterParam<!-- -->s added to @filter.
 *
 * Return value: a #GList of #MMFilterParam objects. Use #g_list_free when done
 * with the list.
 */

GList *
mm_filter_get_filtering_params (MMFilter *filter)
{
  return g_list_reverse (filter->details->filter_params);
}
