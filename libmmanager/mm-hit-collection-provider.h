/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_HIT_COLLECTION_PROVIDER_H__
#define __MM_HIT_COLLECTION_PROVIDER_H__

#include <glib-object.h>
#include "mm-category.h"
#include "mm-filter.h"

#define MM_TYPE_HIT_COLLECTION_PROVIDER           (mm_hit_collection_provider_get_type ())
#define MM_HIT_COLLECTION_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                                   MM_TYPE_HIT_COLLECTION_PROVIDER, MMHitCollectionProvider))
#define MM_IS_HIT_COLLECTION_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                                   MM_TYPE_HIT_COLLECTION_PROVIDER))
#define MM_HIT_COLLECTION_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj),\
                                                   MM_TYPE_HIT_COLLECTION_PROVIDER, MMHitCollectionProviderIface))

/**
 * SECTION:mm-hit-collection-provider
 * @short_description: provides #MMHitCollection objects.
 *
 * A handle to an object implementing the #MMHitCollectionProvider iterface.
 * You need this to get a #MMHitCollection for a #MMSoCategory,
 * i.e. if you're using #MMModule.
 */

/**
 * MMHitCollectionProvider:
 *
 * An abstract type that provides hit collections.
 */

typedef struct _MMHitCollectionProvider      MMHitCollectionProvider;
typedef struct _MMHitCollectionProviderIface MMHitCollectionProviderIface;

/**
 * MMHitCollectionProviderIface:
 * @interface: the parent interface.
 * @get_hits: Gets a #MMHitCollection for the specified category and filter.
 *
 * Interface to build #MMHitCollection objects with #MMModule.
 */

struct _MMHitCollectionProviderIface {
  GTypeInterface interface;

  MMHitCollection* (*get_hits) (MMHitCollectionProvider *provider,
                                MMCategory              *category,
                                MMFilter                *filter);
};

/* Interface functions */
GType             mm_hit_collection_provider_get_type  (void);
MMHitCollection*  mm_hit_collection_provider_get_hits  (MMHitCollectionProvider *provider,
                                                        MMCategory              *category,
                                                        MMFilter                *filter);

#endif /* __MM_HIT_COLLECTION_PROVIDER_H__ */
