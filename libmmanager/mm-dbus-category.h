/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_DBUS_CATEGORY_H__
#define __MM_DBUS_CATEGORY_H__

#include <glib.h>
#include <glib-object.h>
#include "mm-dbus-application.h"
#include "mm-category.h"
 
#define MM_TYPE_DBUS_CATEGORY            (mm_dbus_category_get_type())
#define MM_DBUS_CATEGORY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                          MM_TYPE_DBUS_CATEGORY, MMDBusCategory))
#define MM_DBUS_CATEGORY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                          MM_TYPE_DBUS_CATEGORY, MMDBusCategoryClass))
#define MM_IS_DBUS_CATEGORY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                          MM_TYPE_DBUS_CATEGORY))
#define MM_IS_DBUS_CATEGORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                          MM_TYPE_DBUS_CATEGORY))
#define MM_DBUS_CATEGORY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                          MM_TYPE_DBUS_CATEGORY, MMDBusCategoryClass))

/**
 * SECTION:mm-dbus-category
 * @short_description: DBus implementation of #MMCategory.
 *
 * A #MMDBusCategory is an implementation of #MMCategory for categories
 * using the DBus interface of the library.
 */

/**
 * MMDBusCategory:
 *
 * A DBus implementation of a #MMCategory.
 */

typedef struct _MMDBusCategory      MMDBusCategory;

/**
 * MMDBusCategoryClass:
 *
 * Class of #MMDBusCategory objects.
 */

typedef struct _MMDBusCategoryClass MMDBusCategoryClass;

struct _MMDBusCategory {
  MMCategory parent;
};

struct _MMDBusCategoryClass {
  MMCategoryClass parent_class;
};

/* public methods */
GType             mm_dbus_category_get_type (void);
MMCategory*       mm_dbus_category_new      (MMDBusApplication *app,
                                             const char *name,
                                             GIcon      *icon);
#endif /* __MM_DBUS_CATEGORY_H__ */
