/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib-object.h>
#include <glib.h>
#include <dbus/dbus-glib.h>
#include "mm-application.h"
#include "mm-dbus-application.h"

#include "mm-manager.h"

G_DEFINE_TYPE (MMManager, mm_manager, G_TYPE_OBJECT);

#define MM_MANAGER_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_MANAGER, MMManagerPrivate))

MMManager* mm_manager_singleton = NULL;

struct _MMManagerPrivate {
  MMModuleManager *module_manager;
  GHashTable *applications;
};

static void
mm_manager_finalize (GObject *o)
{
  MMManager *manager = MM_MANAGER (o);

  g_hash_table_destroy (manager->details->applications);

  G_OBJECT_CLASS (mm_manager_parent_class)->finalize (o);
}

static void
mm_manager_class_init (MMManagerClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = mm_manager_finalize;

  g_type_class_add_private (klass, sizeof (MMManagerPrivate));
}

static void
insert_dbus_application (GHashTable *applications,
                         DBusGConnection *connection,
                         const char *name,
                         const char *path)
{
  DBusGProxy *app_proxy;
  guint supported_type;
  char *desktop_id = NULL;
  gboolean res;
  MMApplication *app;
  GError *error = NULL;

  /* validate our args */
  g_return_if_fail (name != NULL);
  g_return_if_fail (path != NULL);

  app_proxy = dbus_g_proxy_new_for_name (connection,
                                         name, path,
                                         "org.gnome.MediaManager.Application");
  res = dbus_g_proxy_call (app_proxy,
                           "GetApplicationInfo",
                           &error,
                           G_TYPE_INVALID,
                           G_TYPE_UINT,
                           &supported_type,
                           G_TYPE_STRING,
                           &desktop_id,
                           G_TYPE_INVALID);
  if (!res) {
    g_warning ("Unable to get application info for the app at path %s: %s", path,
               error->message);
    g_object_unref (app_proxy);
    g_error_free (error);
    return;
  }

  app = mm_dbus_application_new (desktop_id, supported_type,
                                 name, path);
  g_hash_table_insert (applications,
                       g_strdup (name), /* key   */
                       app);            /* value */
  g_free (desktop_id);
  g_object_unref (app_proxy);
}

static void
insert_dbus_applications (GHashTable *applications)
{
  DBusGConnection *connection;
  DBusGProxy *dbus_manager_proxy;
  GError *error = NULL;
  gboolean res;
  char **registered_names = NULL;
  char **registered_paths = NULL;
  int idx, total_apps;

  connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (error) {
    g_warning ("Error while connecting to the session bus: %s", error->message);
    g_error_free (error);
    return;
  }

  dbus_manager_proxy = dbus_g_proxy_new_for_name (connection,
                                                  "org.gnome.MediaManager",
                                                  "/org/gnome/MediaManager/Manager",
                                                  "org.gnome.MediaManager.Manager");
  res = dbus_g_proxy_call (dbus_manager_proxy,
                           "GetRegisteredApps",
                           &error,
                           G_TYPE_INVALID,
                           G_TYPE_STRV,
                           &registered_names,
                           G_TYPE_STRV,
                           &registered_paths,
                           G_TYPE_INVALID);
  if (!res) {
    g_warning ("Error while calling GetRegisteredApps on the dbus manager: %s",
               error->message);
    g_object_unref (dbus_manager_proxy);
    g_error_free (error);
    return;
  }

  if ((total_apps = G_N_ELEMENTS (registered_names)) != G_N_ELEMENTS (registered_paths)) {
    /* wtf, something is broken here, we'd better return */
    goto out;
  }

  if (total_apps == 1) {
    /* array is NULL-terminated, so in this case there are no
     * applications.
     */
    goto out;
  }

  for (idx = 0; idx < total_apps - 1; idx++) {
    insert_dbus_application (applications, connection,
                             registered_names[idx], registered_paths[idx]);
  }

out:
  g_strfreev (registered_names);
  g_strfreev (registered_paths);
  g_object_unref (dbus_manager_proxy);
}

static void
populate_applications_table (MMManager *manager)
{
  GHashTable *applications = manager->details->applications;
  GList *app_providers, *l;
  MMApplication *application;
  MMApplicationProvider *provider;
  const char *id;

  app_providers = mm_module_manager_get_all_application_providers (manager->details->module_manager);
  for (l = app_providers; l; l = l->next) {
    provider = MM_APPLICATION_PROVIDER (l->data);
    application = mm_application_provider_get_application (provider);
    id = mm_application_get_id (application);
    g_hash_table_insert (applications,
                         g_strdup (id), /* key   */
                         application);  /* value */
  }

  g_list_free (app_providers);

  /* now insert all the application registered on the DBus manager */
  insert_dbus_applications (applications);
}

static void
mm_manager_init (MMManager *manager)
{
  MMManagerPrivate *details = manager->details = MM_MANAGER_GET_PRIVATE (manager);

  details->module_manager = MM_MODULE_MANAGER (g_object_new (MM_TYPE_MODULE_MANAGER, NULL));
  details->applications = g_hash_table_new_full (g_str_hash, g_str_equal,
                                                 (GDestroyNotify) g_free,
                                                 NULL);

  populate_applications_table (manager);
}

/* public methods */

/**
 * mm_manager_get:
 * 
 * Gets the #MMManager singleton instance.
 *
 * Return value: a #MMManager object. The object is owned by the library and
 * you should not modify or unref it.
 */

MMManager *
mm_manager_get (void)
{
  if (!mm_manager_singleton) {
    mm_manager_singleton = MM_MANAGER (g_object_new (MM_TYPE_MANAGER, NULL));
  }

  return mm_manager_singleton;
}

/**
 * mm_manager_get_application_list_for_type:
 * @manager: a #MMManager.
 * @type: the media type you're interested in.
 *
 * Gets a #GList of #MMApplication objects supporting the specified type.
 *
 * Return value: a #GList of #MMApplication objects. Use #g_list_free when
 * done with it.
 */

GList *
mm_manager_get_application_list_for_type (MMManager *manager,
                                          MMApplicationType type)
{
  GList *all_apps, *l;
  GList *app_list = NULL;
  MMApplicationType supported_type;
  MMApplication *app;
  
  g_return_val_if_fail (manager != NULL, NULL);
  g_return_val_if_fail (MM_IS_MANAGER (manager), NULL);

  /* this should be fine anyway, as the hash table is created only when
   * initializing the manager.
   */
  all_apps = g_hash_table_get_values (manager->details->applications);
  for (l = all_apps; l; l = l->next) {
    app = l->data;
    g_object_get (app,
                  "supported-type", &supported_type,
                  NULL);
    if (supported_type == type) {
      app_list = g_list_prepend (app_list, app);
    }
  }

  g_list_free (all_apps);

  return app_list;
}

/**
 * mm_manager_get_application_list:
 * @manager: a #MMManager.
 *
 * Gets a list of all the #MMApplication objects known to the manager.
 *
 * Return value: a #GList of #MMApplication objects. Use #g_list_free when done
 * with it.
 */

GList *
mm_manager_get_application_list (MMManager *manager)
{
  g_return_val_if_fail (manager != NULL, NULL);
  g_return_val_if_fail (MM_IS_MANAGER (manager), NULL);

  /* see the comment in _get_application_list_for_type () */
  return g_hash_table_get_values (manager->details->applications);
}

/**
 * mm_manager_get_module_manager:
 * @manager: a #MMManager.
 *
 * Gets the #MMModuleManager singleton instance.
 *
 * Return value: a #MMModuleManager. The object is owned by the library and
 * you should not unref or modify it.
 */

MMModuleManager *
mm_manager_get_module_manager (MMManager *manager)
{
  g_return_val_if_fail (manager != NULL, NULL);
  g_return_val_if_fail (MM_IS_MANAGER (manager), NULL);

  return manager->details->module_manager;
}
