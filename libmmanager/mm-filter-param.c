/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include "mm-filter-param.h"
#include "mm-types.h"
#include "mm-attribute.h"

#include <glib.h>

#define MM_FILTER_PARAM_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_FILTER_PARAM, MMFilterParamPrivate))

struct _MMFilterParamPrivate {
  GValue *value;
  MMAttribute *attribute;
  MMComparisionOperator op;
};

G_DEFINE_TYPE (MMFilterParam, mm_filter_param, G_TYPE_OBJECT);

void
mm_filter_param_finalize (GObject *o)
{
  mm_filter_param_clear (MM_FILTER_PARAM (o));
  g_free (MM_FILTER_PARAM (o)->details->value);

  G_OBJECT_CLASS (mm_filter_param_parent_class)->finalize (o);
}

static void
mm_filter_param_init (MMFilterParam *fp)
{
  MMFilterParamPrivate *details = fp->details = MM_FILTER_PARAM_GET_PRIVATE (fp);

  details->value = NULL;
  details->attribute = NULL;
  details->op = MM_COMP_NONE;
}

static void
mm_filter_param_class_init (MMFilterParamClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = mm_filter_param_finalize;

  g_type_class_add_private (klass, sizeof (MMFilterParamPrivate));
}

/* public methods */

/**
 * mm_filter_param_get_attribute:
 * @filter_param: a #MMFilterParam.
 *
 * Gets the attribute stored in @filter_param.
 *
 * Return value: a #MMAttribute. Do not modify it.
 */

MMAttribute *
mm_filter_param_get_attribute (MMFilterParam *filter_param)
{
  return filter_param->details->attribute;
}

/**
 * mm_filter_param_get_operator:
 * @filter_param: a #MMFilterParam.
 *
 * Gets the operator stored in @filter_param.
 *
 * Return value: a #MMComparisionOperator.
 */

MMComparisionOperator
mm_filter_param_get_operator (MMFilterParam *filter_param)
{
  return filter_param->details->op;
}

/**
 * mm_filter_param_get_value:
 * @filter_param: a #MMFilterParam.
 *
 * Gets the value stored in @filter_param.
 *
 * Return value: a #GValue. Do not modify it.
 */

GValue *
mm_filter_param_get_value (MMFilterParam *filter_param)
{
  return filter_param->details->value;
}

/**
 * mm_filter_param_set_attribute:
 * @filter_param: a #MMFilterParam.
 * @attribute: a #MMAttribute.
 *
 * Sets the attribute inside @filter_param to be @attribute. This will also
 * reset the value of @filter_param if @attribute specifies a #GType different
 * from the old one.
 */

void
mm_filter_param_set_attribute (MMFilterParam *filter_param,
                               MMAttribute *attribute)
{
  GType new_attr_type = mm_attribute_get_value_type (attribute);
  GType old_attr_type = mm_attribute_get_value_type (attribute);
  
  if (new_attr_type != old_attr_type) {
    /* we're changing attribute to a new tipe, change value type */
    g_value_unset (filter_param->details->value);
    g_value_init (filter_param->details->value, new_attr_type);
  }

  filter_param->details->attribute = attribute;
}

/**
 * mm_filter_param_set_value:
 * @filter_param: a #MMFilterParam.
 * @value: a #GValue.
 *
 * Sets the value inside @filter_param to be @value. The #GType of @value must
 * be the same of the #MMAttribute inside @filter_param.
 */

void
mm_filter_param_set_value (MMFilterParam *filter_param,
                           GValue *value)
{
  GType attribute_type = mm_attribute_get_value_type
    (filter_param->details->attribute);

  if (attribute_type != G_VALUE_TYPE (value)) {
    g_warning ("Trying to set an invalid value for attribute %s",
               mm_attribute_get_name (filter_param->details->attribute));
    return;
  }

  g_value_reset (filter_param->details->value);
  g_value_copy (value, filter_param->details->value);
}

/**
 * mm_filter_param_set_comparision:
 * @filter_param: a #MMFilterParam.
 * @op: a #MMComparisionOperator.
 *
 * Sets the comparision operator inside @filter_param to be @op.
 */

void
mm_filter_param_set_comparision (MMFilterParam *filter_param,
                                 MMComparisionOperator op)
{
  filter_param->details->op = op;
}

/**
 * mm_filter_param_clear:
 * @filter_param: a #MMFilterParam.
 *
 * Clears all the fields of @filter_param, leaving it empty.
 */

void
mm_filter_param_clear (MMFilterParam *filter_param)
{
  filter_param->details->op = MM_COMP_NONE;
  filter_param->details->attribute = NULL;
  g_value_unset (filter_param->details->value);
}

/**
 * mm_filter_param_new:
 * @attribute: a #MMAttribute.
 * @value: a #GValue.
 * @op: a #MMComparisionOperator.
 *
 * Builds a new #MMFilterParam, which would filter for @attribute<!-- -->'s
 * value being in the relation specified by @op with @value.
 *
 * Return value: a #MMFilterParam object.
 */

MMFilterParam *
mm_filter_param_new (MMAttribute *attribute,
                     GValue      *value,
                     MMComparisionOperator op)
{
  MMFilterParam *fp;
  GType attribute_type = mm_attribute_get_value_type (attribute);

  if (attribute_type != G_VALUE_TYPE (value)) {
    g_warning ("Trying to set an incompatible value to attribute %s",
               mm_attribute_get_name (attribute));
    return NULL;
  }
  
  fp = g_object_new (MM_TYPE_FILTER_PARAM, NULL);

  fp->details->op = op;
  fp->details->attribute = attribute;
  fp->details->value = g_new0 (GValue, 1);
  g_value_init (fp->details->value, attribute_type);
  g_value_copy (value, fp->details->value);

  return fp;
}
