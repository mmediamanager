/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_APPLICATION_PROVIDER_H__
#define __MM_APPLICATION_PROVIDER_H__

#include <glib-object.h>

#include "mm-application.h"

#define MM_TYPE_APPLICATION_PROVIDER           (mm_application_provider_get_type ())
#define MM_APPLICATION_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                                MM_TYPE_APPLICATION_PROVIDER, MMApplicationProvider))
#define MM_IS_APPLICATION_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                                MM_TYPE_APPLICATION_PROVIDER))
#define MM_APPLICATION_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj),\
                                                MM_TYPE_APPLICATION_PROVIDER, MMApplicationProviderIface))

/**
 * SECTION:mm-application-provider
 * @short_description: provides #MMApplication objects.
 * 
 * A handle to an object implementing the #MMApplicationProviderIface iterface.
 * You need this to build a #MMApplication object if you're using #MMModule.
 */

/**
 * MMApplicationProvider:
 *
 * An abstract type that specifies an application provider.
 */

typedef struct _MMApplicationProvider MMApplicationProvider;
typedef struct _MMApplicationProviderIface MMApplicationProviderIface;

/**
 * MMApplicationProviderIface:
 * @interface: the parent interface.
 * @get_application: Gets the implemented #MMApplication object.
 *
 * Interface to build #MMApplication objects with #MMModule.
 */

struct _MMApplicationProviderIface {
  GTypeInterface interface;

  MMApplication* (* get_application) (MMApplicationProvider *provider);
};

/* Interface functions */

GType           mm_application_provider_get_type        (void);
MMApplication*  mm_application_provider_get_application (MMApplicationProvider *provider);

#endif /* __MM_APPLICATION_PROVIDER_H__ */
