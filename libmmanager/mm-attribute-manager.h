/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_ATTRIBUTE_MANAGER_H__
#define __MM_ATTRIBUTE_MANAGER_H__

#include <glib.h>
#include <glib-object.h>

#include "mm-attribute.h"

#define MM_TYPE_ATTRIBUTE_MANAGER            (mm_attribute_manager_get_type())
#define MM_ATTRIBUTE_MANAGER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                              MM_TYPE_ATTRIBUTE_MANAGER, MMAttributeManager))
#define MM_IS_ATTRIBUTE_MANAGER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                              MM_TYPE_ATTRIBUTE_MANAGER))
#define MM_ATTRIBUTE_MANAGER_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE((obj),\
                                              MM_TYPE_ATTRIBUTE_MANAGER, MMAttributeManagerIface))

/**
 * SECTION:mm-attribute-manager
 * @short_description: attribute manager.
 *
 * An object implementing the #MMAttributeManagerIface interface. It manages and
 * owns a number of attributes, offering the chance to look them up by their
 * unique id.
 */
 

/**
 * MMAttributeManager:
 *
 * Abstract type for attribute managers.
 */

typedef struct _MMAttributeManager      MMAttributeManager;
typedef struct _MMAttributeManagerIface MMAttributeManagerIface;

/**
 * MMAttributeManagerIface:
 * @base_iface: parent interface.
 * @get_supported_attributes: Gets a #GList of all the supported attributes.
 * @lookup_attribute: Gets the attribute with the specified id.
 *
 * Interface to manage #MMAttribute objects.
 */

struct _MMAttributeManagerIface {
  GTypeInterface base_iface;

  GList *       (*get_supported_attributes) (MMAttributeManager *attr_manager);
  MMAttribute * (*lookup_attribute)         (MMAttributeManager *attr_manager,
                                             const char         *attr_id);
};

/* TODO: add a method to validate attributes */

/* public methods */

GType         mm_attribute_manager_get_type                 (void);
GList *       mm_attribute_manager_get_supported_attributes (MMAttributeManager *attr_manager);
MMAttribute * mm_attribute_manager_lookup_attribute         (MMAttributeManager *attr_manager,
                                                             const char         *attr_id);

#endif /* __MM_ATTRIBUTE_MANAGER_H__ */
