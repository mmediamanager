/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <string.h>
#include <libxml/tree.h>
#include <libxml/xmlwriter.h>
#include <libxml/xmlreader.h>
#include <libxml/xmlerror.h>
#include "mm-string-utils.h"
#include "mm-utils.h"
#include "mm-hit.h"
#include "mm-error.h"

typedef struct {
  MMComparisionOperator op;
  const char *string;
} OperatorGrid;

typedef struct {
  xmlTextWriterPtr writer;
  GError **error;
} SerializeData;

static OperatorGrid operator_grid[] = 
{
  { MM_COMP_EQUAL, "EQ" },
  { MM_COMP_GREATER, "GR" },
  { MM_COMP_GREATER_EQUAL, "GRQ" },
  { MM_COMP_LESS, "LS" },
  { MM_COMP_LESS_EQUAL, "LSQ" },
  { MM_COMP_NONE, "" }
};

static void
set_error (int res, GError **error, const char *obj_name)
{
  xmlErrorPtr xml_error;

  if (res == -1) {
    xml_error = xmlGetLastError ();
    g_set_error (error, MM_XML_ERROR_QUARK, MM_XML_ERROR_UNSERIALIZE_FAILED,
                 "Error while parsing the serialized xml %s: %s",
                 obj_name, (xml_error != NULL) ? (xml_error->message) : "");
  } else if (res == 0) {
    g_set_error (error, MM_XML_ERROR_QUARK, MM_XML_ERROR_UNEXPECTED_EOF,
                 "Error while parsing the serialized xml %s: unexpected "
                 "end of the xml buffer.", obj_name);
  }
}

static void
set_hit_error (int res, GError **error)
{
  set_error (res, error, "Hit");
}

static void
set_hit_collection_error (int res, GError **error)
{
  set_error (res, error, "HitCollection");
}

static void
set_value_error (int res, GError **error)
{
  set_error (res, error, "Value");
}

static void
set_filter_param_error (int res, GError **error)
{
  set_error (res, error, "FilterParam");
}

static void
set_operator_error (int res, GError **error)
{
  set_error (res, error, "ComparisionOperator");
}

static void
set_filter_error (int res, GError **error)
{
  set_error (res, error, "Filter");
}

static void
set_pair_error (int res, GError **error)
{
  set_error (res, error, "Pair");
}

static void
set_attribute_error (int res, GError **error)
{
  set_error (res, error, "Attribute");
}

static void
serialize_value (xmlTextWriterPtr writer, GValue *v, GError **error)
{
  xmlChar *safe;
  int res;
  char *ret = NULL;

  ret = mm_gvalue_to_string (v);
  if (!ret) {
    return;
  }

  safe = xmlCharStrdup (ret);
  g_free (ret);

  res = xmlTextWriterWriteElement (writer, BAD_CAST ("value"), safe);
  g_free (safe);
  if (res == -1) {
    set_value_error (res, error);
    return;
  }
}

static void
serialize_op (xmlTextWriterPtr writer, MMComparisionOperator op, GError **error)
{
  int idx, res;
  xmlChar *str = NULL;

  for (idx = 0; idx < G_N_ELEMENTS (operator_grid); idx++) {
    if (op == operator_grid[idx].op) {
      str = xmlCharStrdup (operator_grid[idx].string);
    }
  }

  if (str) {
    res = xmlTextWriterWriteElement (writer, BAD_CAST ("op"), str);
    g_free (str);
    if (res == -1) {
      set_operator_error (res, error);
      return;
    }
  }
}

static void
serialize_attribute (xmlTextWriterPtr writer,
                     MMAttribute *attribute,
                     GError **error)
{
  xmlChar *safe_str;
  int res;

  res = xmlTextWriterStartElement (writer, BAD_CAST ("attribute"));
  if (res == -1) {
    set_attribute_error (res, error);
    return;
  }

  safe_str = xmlCharStrdup (mm_attribute_get_id (attribute));
  res = xmlTextWriterWriteAttribute (writer, BAD_CAST ("id"), safe_str);
  g_free (safe_str);
  if (res == -1) {
    set_attribute_error (res, error);
    return;
  }

  safe_str = xmlCharStrdup (mm_attribute_get_name (attribute));
  xmlTextWriterWriteAttribute (writer, BAD_CAST ("name"), safe_str);
  g_free (safe_str);
  if (res == -1) {
    set_attribute_error (res, error);
    return;
  }

  safe_str = xmlCharStrdup (mm_attribute_get_description (attribute));
  xmlTextWriterWriteAttribute (writer, BAD_CAST ("description"), safe_str);
  g_free (safe_str);
  if (res == -1) {
    set_attribute_error (res, error);
    return;
  }

  res = xmlTextWriterWriteAttribute (writer, BAD_CAST ("type"),
                                     BAD_CAST (g_type_name (mm_attribute_get_value_type (attribute))));
  if (res == -1) {
    set_attribute_error (res, error);
    return;
  }

  /* close "attribute" */
  res = xmlTextWriterEndElement (writer);
  if (res == -1) {
    set_attribute_error (res, error);
    return;
  }
}

static void
serialize_filter_param (MMFilterParam *fp,
                        SerializeData *data)
{
  MMAttribute *attribute;
  GValue *val;
  MMComparisionOperator op;
  int res;
  xmlTextWriterPtr writer = data->writer;
  GError **error = data->error;

  attribute = mm_filter_param_get_attribute (fp);
  val = mm_filter_param_get_value (fp);
  op = mm_filter_param_get_operator (fp);

  res = xmlTextWriterStartElement (writer, BAD_CAST ("filter-param"));
  if (res == -1) {
    set_filter_param_error (res, error);
    return;
  }

  serialize_attribute (writer, attribute, error);
  if (*error) {
    return;
  }
  serialize_value (writer, val, error);
  if (*error) {
    return;
  }
  serialize_op (writer, op, error);
  if (*error) {
    return;
  }

  /* close "filter-param" */
  res = xmlTextWriterEndElement (writer);
  if (res == -1) {
    set_filter_param_error (res, error);
    return;
  }
}

static void
unserialize_operator (xmlTextReaderPtr reader, MMComparisionOperator *op,
                      GError **error)
{
  const xmlChar * op_string;
  gboolean found = FALSE;
  int idx, res;

  /* we should be on <op> */
  if (xmlStrcmp (xmlTextReaderConstName (reader), BAD_CAST ("op")) != 0) {
    g_set_error (error, MM_XML_ERROR_QUARK, MM_XML_ERROR_UNEXPECTED_NODE,
                 "Error while parsing the serialized xml opeator: the xml reader "
                 "does not point to a ComparisionOperator");
    return;
  }

  /* move on to the content */
  res = xmlTextReaderRead (reader);
  if (res <= 0) {
    set_operator_error (res, error);
    return;
  }

  op_string = xmlTextReaderConstValue (reader);
  for (idx = 0; idx < G_N_ELEMENTS (operator_grid); idx++) {
    if (xmlStrcmp (op_string, BAD_CAST (operator_grid[idx].string)) == 0) {
      found = TRUE;
      break;
    }
  }

  /* move to </op> */
  res = xmlTextReaderRead (reader);
  if (res <= 0) {
    set_operator_error (res, error);
    return;
  }

  if (found) {
    *op = operator_grid[idx].op;
  }
}

static void
unserialize_value (xmlTextReaderPtr reader, GValue *v, GError **error)
{
  int res;  
  GType type;
  const char *val_string;
  gboolean result;

  /* we should be on <value> */
  if (xmlStrcmp (xmlTextReaderConstName (reader), BAD_CAST ("value")) != 0) {
    g_set_error (error, MM_XML_ERROR_QUARK, MM_XML_ERROR_UNEXPECTED_NODE,
                 "Error while parsing the serialized xml value: the xml reader "
                 " does not point to a value");
    return;
  }

  /* move on to the content node */
  res = xmlTextReaderRead (reader);
  if (res <= 0) {
    set_value_error (res, error);
    return;
  }

  type = G_VALUE_TYPE (v);
  val_string = (const char *) xmlTextReaderConstValue (reader);

  result = mm_gvalue_from_string (val_string, type, v);
  if (!result) {
    g_set_error (error, MM_XML_ERROR_QUARK, MM_XML_ERROR_UNKNOWN_GTYPE,
                 "Can't convert the string to a value: unhandled type");
    return;
  }

  /* move over </value> */
  res = xmlTextReaderRead (reader);
  if (res <= 0) {
    set_value_error (res, error);
    g_value_unset (v);
    g_free (v);
    v = NULL;
  }
}

static void
unserialize_attribute (xmlTextReaderPtr reader, MMAttribute **attribute, GError **error)
{
  xmlChar *id, *name, *desc, *type_name;
  GType type;

  /* we should be on <attribute> */
  if (xmlStrcmp (xmlTextReaderConstName (reader), BAD_CAST ("attribute")) != 0) {
    g_set_error (error, MM_XML_ERROR_QUARK, MM_XML_ERROR_UNEXPECTED_NODE,
                 "Error while parsing the serialized xml attribute: the xml reader"
                 " does not point to an attribute.");
    return;
  }

  id = xmlTextReaderGetAttribute (reader, BAD_CAST ("id"));
  name = xmlTextReaderGetAttribute (reader, BAD_CAST ("name"));
  desc = xmlTextReaderGetAttribute (reader, BAD_CAST ("desc"));
  type_name = xmlTextReaderGetAttribute (reader, BAD_CAST ("type"));

  type = g_type_from_name ((const char *) type_name);
  if (type == 0) {
    g_set_error (error, MM_XML_ERROR_QUARK, MM_XML_ERROR_UNKNOWN_GTYPE,
                 "Error while parsing the serialized xml attribute: cannot get a GType"
                 " for the name %s.", (const char *) type_name);
    goto out;
  }

  *attribute = mm_attribute_new (type,
                                 (const char *) id,
                                 (const char *) name,
                                 (const char *) desc);
out:
  g_free (id);
  g_free (name);
  g_free (desc);
  g_free (type_name);
}

static MMFilterParam *
unserialize_filter_param (xmlTextReaderPtr reader, GError **error)
{
  int res;
  MMAttribute *attribute = NULL;
  MMComparisionOperator op = MM_COMP_NONE;
  GValue *val = NULL; 
  MMFilterParam *fp = NULL;

  res = xmlTextReaderRead (reader);
  if (res <= 0) {
    set_filter_param_error (res, error);
    goto out;
  }
 
  /* we're either on <attribute> or </filter-param> if the object is empty */
  while (!((xmlTextReaderNodeType (reader) == XML_READER_TYPE_END_ELEMENT) &&
           xmlStrcmp (xmlTextReaderConstName (reader), BAD_CAST ("filter-param")) == 0) && res > 0) {
    unserialize_attribute (reader, &attribute, error);
    if (*error) {
      goto out;
    }

    res = xmlTextReaderRead (reader);
    if (res <= 0) {
      set_filter_param_error (res, error);
      goto out;
    }
    /* we're now on <value> */
    val = mm_create_gvalue_for_attribute (attribute);
    unserialize_value (reader, val, error);
    if (*error) {
      goto out;
    }

    res = xmlTextReaderRead (reader);
    if (res <= 0) {
      set_filter_param_error (res, error);
      goto out;
    }
    /* we're now on <op> */
    unserialize_operator (reader, &op, error);
    if (*error) {
      goto out;
    }

    /* move after </op> */
    res = xmlTextReaderRead (reader);
    if (res <= 0) {
      set_filter_param_error (res, error);
      goto out;
    }    
  }

  /* if we're here, everything in the unserialize sub-operations went well */
  fp = mm_filter_param_new (attribute, val, op);

out:
  if (val) {
    g_value_unset (val);
    g_free (val);
  }

  return fp; 
}

static void
serialize_pair (MMAttribute *attr,
                GValue *val,
                SerializeData *pair_data)
{
  xmlTextWriterPtr writer = pair_data->writer;
  GError **error = pair_data->error;
  int res;

  if (*error) {
    /* an error occurred in a previous iteration of this function, don't do
     * anything.
     */
    return;
  }

  xmlTextWriterStartElement (writer, BAD_CAST ("pair"));
  serialize_attribute (writer, attr, error);
  if (*error) {
    return;
  }

  serialize_value (writer, val, error);
  if (*error) {
    return;
  }

  /* end "pair" */
  res = xmlTextWriterEndElement (writer);
  if (res == -1) {
    set_pair_error (res, error);
  }
}

static MMHit *
unserialize_hit (xmlTextReaderPtr reader, GError **error)
{
  int res;
  MMAttribute *attribute = NULL;
  GValue *v;
  MMHit *hit = NULL;

  hit = g_object_new (MM_TYPE_HIT, NULL);

  do {
    /* skip <pair> */
    res = xmlTextReaderRead (reader);
    if (res <= 0) {
      set_hit_error (res, error);
      g_object_unref (hit);
      hit = NULL;
      break;
    }
    res = xmlTextReaderRead (reader);
    /* now we should be on "attribute" */
    if (res <= 0) {
      set_hit_error (res, error);
      g_object_unref (hit);
      hit = NULL;
      break;
    }
    unserialize_attribute (reader, &attribute, error);
    if (*error) {
      g_object_unref (hit);
      hit = NULL;
      break;
    }
    res = xmlTextReaderRead (reader);
    /* we're now on "value" */
    if (res <= 0) {
      set_hit_error (res, error);
      g_object_unref (hit);
      hit = NULL;
      break;
    }

    v = mm_create_gvalue_for_attribute (attribute);
    unserialize_value (reader, v, error);
    if (*error) {
      g_object_unref (hit);
      hit = NULL;
      break;
    }

    mm_hit_set_value (hit, attribute, v);

    res = xmlTextReaderRead (reader);
    /* now we're on </pair> */
    if (res <= 0) {
      set_hit_error (res, error);
      g_object_unref (hit);
      hit = NULL;
      break;
    }

    res = xmlTextReaderRead (reader);
    if (res <= 0) {
      set_hit_error (res, error);
      g_object_unref (hit);
      hit = NULL;
      break;
    }    
    /* now we're either on <pair> again or </hit>. we must end they cycle
     * on </hit>.
     */
  } while (!((xmlTextReaderNodeType (reader) == XML_READER_TYPE_END_ELEMENT) &&
             xmlStrcmp (xmlTextReaderConstName (reader), BAD_CAST ("hit"))));

  return hit;
}

/* public functions */

/**
 * mm_filter_serialize:
 * @filter: a #MMFilter object.
 * @error: a #GError.
 *
 * Builds a serialized XML version of @filter.
 *
 * Return value: a newly allocated string buffer containing the serialized
 * #MMFilter.
 */

char *
mm_filter_serialize (MMFilter *filter, GError **error)
{
  char *serialized = NULL;
  xmlBufferPtr buffer;
  xmlTextWriterPtr writer;
  GList *filter_params;
  SerializeData *fp_data;
  int res;

  buffer = xmlBufferCreate ();
  writer = xmlNewTextWriterMemory (buffer, 0);

  res = xmlTextWriterStartDocument (writer, NULL, NULL, NULL);
  if (res == -1) {
    set_filter_error (res, error);
    goto out;
  }

  res = xmlTextWriterStartElement (writer, BAD_CAST ("filter"));
  if (res == -1) {
    set_filter_error (res, error);
    goto out;
  }

  filter_params = mm_filter_get_filtering_params (filter);
  fp_data = g_new0 (SerializeData, 1);
  fp_data->writer = writer;
  fp_data->error = error;
  g_list_foreach (filter_params, (GFunc) serialize_filter_param, fp_data);
  g_free (fp_data);
  if (*error) {
    goto out;
  }

  /* close "filter" */
  res = xmlTextWriterEndElement (writer);
  if (res == -1) {
    set_filter_error (res, error);
    goto out;
  }

  res = xmlTextWriterEndDocument (writer);
  if (res == -1) {
    set_filter_error (res, error);
    goto out;
  }

  /* everything went well */
  serialized = g_strdup ((char *) xmlBufferContent (buffer));

out:
  xmlFreeTextWriter (writer);
  xmlBufferFree (buffer);

  return serialized;
}

/**
 * mm_hit_collection_serialize:
 * @hc: a #MMHitCollection object.
 * @error: a #GError.
 *
 * Builds a serialized XML version of the #MMHitCollection.
 *
 * Return value: a newly allocated string buffer containing the serialized
 * #MMHitCollection.
 */

char *
mm_hit_collection_serialize (MMHitCollection *hc, GError **error)
{
  char *serialized = NULL;
  int res;
  xmlBufferPtr buffer;
  xmlTextWriterPtr writer;
  MMHit *hit;
  GHashTable *attrs_and_values;
  SerializeData *pair_data;

  buffer = xmlBufferCreate ();
  writer = xmlNewTextWriterMemory (buffer, 0);

  res = xmlTextWriterStartDocument (writer, NULL, NULL, NULL);
  if (res == -1) {
    set_hit_collection_error (res, error);
    goto out;
  }

  res = xmlTextWriterStartElement (writer, BAD_CAST ("hit-collection"));
  if (res == -1) {
    set_hit_collection_error (res, error);
    goto out;
  }

  while ((hit = mm_hit_collection_get_next_hit (hc)) != NULL) {
    res = xmlTextWriterStartElement (writer, BAD_CAST ("hit"));
    if (res == -1) {
      g_object_unref (hit);
      set_hit_collection_error (res, error);
      goto out;
    }

    attrs_and_values = mm_hit_get_all_values (hit);
    g_object_unref (hit);

    pair_data = g_slice_new0 (SerializeData);
    pair_data->writer = writer;
    pair_data->error = error;

    g_hash_table_foreach (attrs_and_values,
                          (GHFunc) serialize_pair,
                          pair_data);
    g_slice_free (SerializeData, pair_data);
    g_hash_table_destroy (attrs_and_values);

    if (*error) {
      /* an error occurred somewhere in one of the sub-functions */
      goto out;
    }

    /* close "hit" */
    res = xmlTextWriterEndElement (writer);
    if (res == -1) {
      set_hit_collection_error (res, error);
      goto out;
    }
  }

  /* close "hit-collection" */
  res = xmlTextWriterEndElement (writer);
  if (res == -1) {
    set_hit_collection_error (res, error);
    goto out;
  }
  res = xmlTextWriterEndDocument (writer);
  if (res == -1) {
    set_hit_collection_error (res, error);
    goto out;
  }

  /* it seems everything has gone well */
  serialized = g_strdup ((char *) xmlBufferContent (buffer));

out:
  xmlFreeTextWriter (writer);
  xmlBufferFree (buffer);

  return serialized;
}

/**
 * mm_filter_unserialize:
 * @s: a serialized #MMFilter, obtained with
 * #mm_filter_serialize.
 * @error: a #GError.
 *
 * Builds a #MMFilter object from a serialized XML string.
 *
 * Return value: a #MMFilter object. Unref it when done.
 */

MMFilter *
mm_filter_unserialize (const char *s, GError **error)
{
  MMFilter *f = NULL;
  MMFilterParam *fp;
  xmlTextReaderPtr reader;
  int res;
  const xmlChar *node_name;

  reader = xmlReaderForMemory (s, strlen (s), NULL, NULL, 0);

  /* cut all the elements before <filter> */
  do {
    res = xmlTextReaderRead (reader);
    node_name = xmlTextReaderConstName (reader);
  } while (xmlStrcmp (node_name, BAD_CAST ("filter")) != 0);

  /* do the error checking here and not in each iteration of the cycle */
  if (res <= 0) {
    set_filter_error (res, error);
    goto out;
  }  

  res = xmlTextReaderRead (reader);
  node_name = xmlTextReaderConstName (reader);  
  f = mm_filter_new ();

  /* we're either on the first <filter-param> or on </filter> if the
   * object is empty. cycle until we're on </filter>.
   */
  while (!((xmlTextReaderNodeType (reader) == XML_READER_TYPE_END_ELEMENT) &&
           xmlStrcmp (node_name, BAD_CAST ("filter")) == 0) && res > 0) {
    fp = unserialize_filter_param (reader, error);
    if (*error) {
      g_object_unref (f);
      f = NULL;
      goto out;
    }
    mm_filter_add_filtering_param (f, fp);
    g_object_unref (fp);
    res = xmlTextReaderRead (reader);
    node_name = xmlTextReaderConstName (reader);
  }

  if (res <= 0) {
    /* do not return an incomplete filter */
    set_filter_error (res, error);
    g_object_unref (f);
    f = NULL;
  }

out:
  xmlFreeTextReader (reader);

  return f;
}

/**
 * mm_hit_collection_unserialize:
 * @s: a serialized #MMHitCollection, obtained with
 * #mm_hit_collection_serialize.
 * @error: a #GError.
 *
 * Builds a #MMHitCollection object from a serialized XML string.
 *
 * Return value: a #MMHitCollection object. Unref it when done.
 */

MMHitCollection *
mm_hit_collection_unserialize (const char *s, GError **error)
{
  MMHitCollection *hc = NULL;
  MMHit *hit;
  xmlTextReaderPtr reader;
  int res;
  const xmlChar *node_name;

  reader = xmlReaderForMemory (s, strlen (s), NULL, NULL, 0);

  /* cut all the elements before <hit-collection> */
  do {
    res = xmlTextReaderRead (reader);
    node_name = xmlTextReaderConstName (reader);
  } while (xmlStrcmp (node_name, BAD_CAST ("hit-collection")) != 0);

  /* check for errors before <hit-collection> all at once, and not
   * in every iteration of the cycle.
   */
  if (res <= 0) {
    set_hit_collection_error (res, error);
    goto out;
  }

  hc = mm_hit_collection_new ();
  res = xmlTextReaderRead (reader);
  node_name = xmlTextReaderConstName (reader);

  /* we're either on the first <hit> or on </hit-collection> if the
   * object is empty. cycle until we're at the end of file.
   */
  while (!((xmlTextReaderNodeType (reader) == XML_READER_TYPE_END_ELEMENT) &&
           xmlStrcmp (node_name, BAD_CAST ("hit-collection")) == 0) && res > 0) {
    hit = unserialize_hit (reader, error);
    if (*error) {
      /* do not return an incomplete HitCollection */
      g_object_unref (hc);
      hc = NULL;
      goto out;
    }
    mm_hit_collection_add_hit (hc, hit);
    g_object_unref (hit);
    res = xmlTextReaderRead (reader);
    node_name = xmlTextReaderConstName (reader);
  }

  if (res <= 0) {
    /* do not return an incomplete HitCollection */
    set_hit_collection_error (res, error);
    g_object_unref (hc);
    hc = NULL;
    goto out;
  }

out:
  xmlFreeTextReader (reader);

  return hc;
}

/**
 * mm_gvalue_to_string:
 * @v: a #GValue
 *
 * Builds a serialized XML version of a #GValue.
 *
 * Return value: a newly allocated string buffer containing the serialized
 * #GValue.
 */

char *
mm_gvalue_to_string (GValue *v)
{
  char *ret = NULL;
  GType type = G_VALUE_TYPE (v);

  /* guess the most used cases and handle those first */
  if (type == G_TYPE_STRING) {
    ret = g_strdup (g_value_get_string (v));
  } else if (type == G_TYPE_BOOLEAN) {
    ret = g_strdup_printf ("%d", g_value_get_boolean (v));
  } else if (type == G_TYPE_INT) {
    ret = g_strdup_printf ("%d", g_value_get_int (v));
  } else if (type == G_TYPE_FLOAT) {
    ret = g_strdup_printf ("%f", g_value_get_float (v));
  } else if (type == G_TYPE_DOUBLE) {
    char double_buff[G_ASCII_DTOSTR_BUF_SIZE];
    ret = g_ascii_dtostr (double_buff, sizeof (double_buff),
                          g_value_get_double (v));
  } else if (type == G_TYPE_LONG) {
    ret = g_strdup_printf ("%ld", g_value_get_long (v));
  } else if (type == G_TYPE_INT64) {
    ret = g_strdup_printf ("%lld", g_value_get_int64 (v));
  } else if (type == G_TYPE_UINT) {
    ret = g_strdup_printf ("%u", g_value_get_uint (v));
  } else if (type == G_TYPE_ULONG) {
    ret = g_strdup_printf ("%lu", g_value_get_ulong (v));
  } else if (type == G_TYPE_UINT64) {
    ret = g_strdup_printf ("%llu", g_value_get_uint64 (v));
  } else if (G_VALUE_HOLDS_CHAR (v)) {
    ret = g_strdup_printf ("%c", g_value_get_char (v));
  } else if (G_VALUE_HOLDS_UCHAR (v)) {
    ret = g_strdup_printf ("%c", g_value_get_uchar (v));
  } else {
    g_warning ("Can't convert the value to string: unhandled type");
  }

  return ret;
}

/**
 * mm_gvalue_from_string:
 * @val_string: a string description of a #GValue. You can use
 * #mm_gvalue_to_string to have one.
 * @type: the type of the #GValue.
 * @v: a zero-filled uninitialized #GValue.
 *
 * Fills @v with the #GValue contained in the XML description @val_string.
 *
 * Return value: %TRUE in case of success, %FALSE in case of failure.
 */

gboolean
mm_gvalue_from_string (const char *val_string, GType type, GValue *v)
{
  gboolean retval = TRUE;

  g_value_init (v, type);

  if (type == G_TYPE_STRING) {
    g_value_set_string (v, val_string);
  } else if (type == G_TYPE_BOOLEAN) {
    gboolean bval;
    sscanf (val_string, "%d", &bval);
    g_value_set_boolean (v, bval);
  } else if (type == G_TYPE_INT) {
    gint ival;
    sscanf (val_string, "%d", &ival);
    g_value_set_int (v, ival);
  } else if (type == G_TYPE_FLOAT) {
    gfloat fval;
    sscanf (val_string, "%f", &fval);
    g_value_set_float (v, fval);
  } else if (type == G_TYPE_DOUBLE) {
    gdouble dval;
    dval = g_ascii_strtod (val_string, NULL);
    g_value_set_double (v, dval);
  } else if (type == G_TYPE_LONG) {
    glong lval;
    sscanf (val_string, "%ld", &lval);
    g_value_set_long (v, lval);
  } else if (type == G_TYPE_INT64) {
    g_value_set_int64 (v, g_ascii_strtoll (val_string, NULL, 0));
  } else if (type == G_TYPE_UINT) {
    guint uval;
    sscanf (val_string, "%u", &uval);
    g_value_set_uint (v, uval);
  } else if (type == G_TYPE_ULONG) {
    gulong ulval;
    sscanf (val_string, "%lu", &ulval);
    g_value_set_ulong (v, ulval);
  } else if (type == G_TYPE_UINT64) {
    g_value_set_uint64 (v, g_ascii_strtoull (val_string, NULL, 0));
  } else if (G_VALUE_HOLDS_CHAR (v)) {
    gchar cval;
    sscanf (val_string, "%c", &cval);    
    g_value_set_char (v, cval);
  } else if (G_VALUE_HOLDS_UCHAR (v)) {
    guchar ucval;
    sscanf (val_string, "%c", &ucval);
    g_value_set_uchar (v, ucval);
  } else {
    retval = FALSE;
  }

  return retval;
}
