/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_MODULE_MANAGER_H__
#define __MM_MODULE_MANAGER_H__

#include <glib.h>
#include <glib-object.h>

#include "mm-category-provider.h"
#include "mm-hit-collection-provider.h"
#include "mm-application-provider.h"

#define MM_TYPE_MODULE_MANAGER            (mm_module_manager_get_type ())
#define MM_MODULE_MANAGER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                           MM_TYPE_MODULE_MANAGER, MMModuleManager))
#define MM_MODULE_MANAGER_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                           MM_TYPE_MODULE_MANAGER, MMModuleManagerClass))
#define MM_IS_MODULE_MANAGER(obj)         (G_TYPE_INSTANCE_CHECK_TYPE ((obj),\
                                           MM_TYPE_MODULE_MANAGER))
#define MM_IS_MODULE_MANAGER_CLASS(klass) (G_TYPE_CLASS_CHECK_CLASS_TYPE ((klass),\
                                           MM_TYPE_MODULE_MANAGER))

/**
 * SECTION:mm-module-manager
 * @short_description: module manager.
 *
 * #MMModuleManager manages all the #MMModule objects registered in the library,
 * e.g. all the applications registered with .so files. You shouldn't need to
 * access this object directly. The object is a singleton owned by #MMManager.
 */

/**
 * MMModuleManager:
 *
 * Manager for #MMModule objects.
 */

typedef struct _MMModuleManager        MMModuleManager;
typedef struct _MMModuleManagerPrivate MMModuleManagerPrivate;
typedef struct _MMModuleManagerClass   MMModuleManagerClass;

struct _MMModuleManager {
  GObject parent_object;
  MMModuleManagerPrivate *details;
};

struct _MMModuleManagerClass {
  GObjectClass parent_class;
};

/* public methods */

GType       mm_module_manager_get_type (void);

GList *     mm_module_manager_get_all_application_providers               (MMModuleManager   *manager);
MMCategoryProvider*
            mm_module_manager_get_category_provider_for_application       (MMModuleManager  *manager,
                                                                           const char       *id);
MMHitCollectionProvider *
            mm_module_manager_get_hit_collection_provider_for_application (MMModuleManager  *manager,
                                                                           const char       *id);

#endif /* __MM_MODULE_MANAGER_H__ */
