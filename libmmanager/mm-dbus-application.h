/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_DBUS_APPLICATION_H__
#define __MM_DBUS_APPLICATION_H__

#include <glib.h>
#include <glib-object.h>
#include "mm-application.h"

#define MM_TYPE_DBUS_APPLICATION            (mm_dbus_application_get_type())
#define MM_DBUS_APPLICATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                             MM_TYPE_DBUS_APPLICATION, MMDBusApplication))
#define MM_DBUS_APPLICATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                             MM_TYPE_DBUS_APPLICATION, MMDBusApplicationClass))
#define MM_IS_DBUS_APPLICATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                             MM_TYPE_DBUS_APPLICATION))
#define MM_IS_DBUS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                             MM_TYPE_DBUS_APPLICATION))
#define MM_DBUS_APPLICATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                             MM_TYPE_DBUS_APPLICATION, MMDBusApplicationClass))

/**
 * SECTION:mm-dbus-application
 * @short_description: DBus implementation of #MMApplication.
 *
 * A #MMDBusApplication is an implementation of #MMApplication for applications
 * using the DBus interface of the library.
 */
 
/**
 * MMDBusApplication:
 *
 * A DBus implementation of a #MMApplication.
 */

typedef struct _MMDBusApplication MMDBusApplication;

/**
 * MMDBusApplicationClass:
 *
 * Class of #MMDBusApplication objects.
 */

typedef struct _MMDBusApplicationClass MMDBusApplicationClass;
typedef struct _MMDBusApplicationPrivate MMDBusApplicationPrivate;

struct _MMDBusApplication {
  MMApplication parent;
  MMDBusApplicationPrivate *details;
};

struct _MMDBusApplicationClass {
  MMApplicationClass parent_class;
};

/* public methods */
GType           mm_dbus_application_get_type (void);
MMApplication * mm_dbus_application_new      (const char        *desktop_id,
                                              MMApplicationType  supported_type,
                                              const char        *name,
                                              const char        *path);
const char *    mm_dbus_application_get_path (MMDBusApplication *app);

#endif /* __MM_DBUS_APPLICATION_H__ */
