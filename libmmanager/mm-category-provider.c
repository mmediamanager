/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <config.h>
#include <glib-object.h>
#include <glib.h>

#include "mm-category-provider.h"
#include "mm-application-provider.h"
#include "mm-application.h"

static void
mm_category_provider_base_init (gpointer klass)
{

}

GType                   
mm_category_provider_get_type (void)
{
  static GType type = 0;

  if (!type) {
    const GTypeInfo info = {
      sizeof (MMCategoryProviderIface),
      mm_category_provider_base_init,
      NULL,
      NULL,
      NULL,
      NULL,
      0,
      0,
      NULL
    };

    type = g_type_register_static (G_TYPE_INTERFACE, 
                                   "MMCategoryProvider",
                                   &info, 0);
    g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
  }

  return type;
}

/**
 * mm_category_provider_get_categories:
 * @provider: a #MMCategoryProvider.
 * @application: a #MMApplication.
 *
 * Gets the list of #MMCategory objects provided by @provider for
 * @application.
 *
 * Return value: a #GList of #MMCategory objects.
 */

GList *
mm_category_provider_get_categories (MMCategoryProvider *provider,
                                     MMApplication *application)
{
  g_return_val_if_fail (MM_IS_CATEGORY_PROVIDER (provider), NULL);
  g_return_val_if_fail (MM_CATEGORY_PROVIDER_GET_IFACE (provider)->get_categories != NULL, NULL);
  g_return_val_if_fail (MM_IS_APPLICATION (application), NULL);
  g_return_val_if_fail (application != NULL, NULL);

  return MM_CATEGORY_PROVIDER_GET_IFACE (provider)->get_categories (provider, application);
}
