/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_HIT_COLLECTION_H__
#define __MM_HIT_COLLECTION_H__
 
#include <glib.h>

#include "mm-hit.h"

#define MM_TYPE_HIT_COLLECTION            (mm_hit_collection_get_type())
#define MM_HIT_COLLECTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                           MM_TYPE_HIT_COLLECTION, MMHitCollection))
#define MM_HIT_COLLECTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                           MM_TYPE_HIT_COLLECTION, MMHitCollectionClass))
#define MM_IS_HIT_COLLECTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                           MM_TYPE_HIT_COLLECTION))
#define MM_IS_HIT_COLLECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                           MM_TYPE_HIT_COLLECTION))
#define MM_HIT_COLLECTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                           MM_TYPE_HIT_COLLECTION, MMHitCollectionClass))

/**
 * SECTION:mm-hit-collection
 * @short_description: a collection of #MMHit objects.
 *
 * #MMHitCollection is a simple collection of #MMHit objects, and encapsulates
 * the results of a query.
 */
 
/**
 * MMHitCollection:
 *
 * An object identifying a collection of #MMHit objects.
 */

typedef struct _MMHitCollection        MMHitCollection;
typedef struct _MMHitCollectionPrivate MMHitCollectionPrivate;
typedef struct _MMHitCollectionClass   MMHitCollectionClass;

struct _MMHitCollection {
  GObject parent_object;
  MMHitCollectionPrivate *details;
};

struct _MMHitCollectionClass {
  GObjectClass parent_class;
};

MMHitCollection *     mm_hit_collection_new          (void);
void                  mm_hit_collection_add_hit      (MMHitCollection *collection,
                                                      MMHit           *hit);
MMHit *               mm_hit_collection_get_next_hit (MMHitCollection *collection);

#endif /* __MM_HIT_COLLECTION_H__ */
