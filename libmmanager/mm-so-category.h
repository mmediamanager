/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_SO_CATEGORY_H__
#define __MM_SO_CATEGORY_H__

#include <glib.h>
#include <glib-object.h>
#include "mm-so-application.h"
#include "mm-category.h"
 
#define MM_TYPE_SO_CATEGORY            (mm_so_category_get_type())
#define MM_SO_CATEGORY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                        MM_TYPE_SO_CATEGORY, MMSoCategory))
#define MM_SO_CATEGORY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                        MM_TYPE_SO_CATEGORY, MMSoCategoryClass))
#define MM_IS_SO_CATEGORY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                        MM_TYPE_SO_CATEGORY))
#define MM_IS_SO_CATEGORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                        MM_TYPE_SO_CATEGORY))
#define MM_SO_CATEGORY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                        MM_TYPE_SO_CATEGORY, MMSoCategoryClass))

/**
 * SECTION:mm-so-category
 * @short_description: Shared Object implementation of #MMCategory.
 *
 * A #MMSoCategory is an implementation of #MMCategory for categories
 * using the Shared Object interface of the library.
 */
 
/**
 * MMSoCategory:
 *
 * A Shared Object implementation of a #MMCategory.
 */

typedef struct _MMSoCategory      MMSoCategory;

/**
 * MMSoCategoryClass:
 *
 * Class of #MMSoCategory objects.
 */

typedef struct _MMSoCategoryClass MMSoCategoryClass;

struct _MMSoCategory {
  MMCategory parent;
};

struct _MMSoCategoryClass {
  MMCategoryClass parent_class;
};

/* public methods */
GType             mm_so_category_get_type (void);
MMCategory*       mm_so_category_new      (MMSoApplication *app,
                                           const char *name,
                                           GIcon      *icon);
#endif /* __MM_SO_CATEGORY_H__ */
