/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include <glib.h>
#include <glib-object.h>

#include "mm-hit.h"
#include "mm-attribute.h"
#include "mm-utils.h"

#define MM_HIT_GET_PRIVATE(o) \
   (G_TYPE_INSTANCE_GET_PRIVATE ((o), MM_TYPE_HIT, MMHitPrivate))

G_DEFINE_TYPE (MMHit, mm_hit, G_TYPE_OBJECT);

struct _MMHitPrivate {
  GHashTable *attributes;
};

typedef struct {
  MMAttribute *attr;
  GValue *val;
} AttributeValuePair;

static void
mm_hit_finalize (GObject *o)
{
  MMHitPrivate *details = MM_HIT (o)->details;

  g_hash_table_destroy (details->attributes);

  G_OBJECT_CLASS (mm_hit_parent_class)->finalize (o);
}

static void
pair_free (gpointer data)
{
  AttributeValuePair *pair = data;

  g_value_unset (pair->val);
  g_free (pair->val);
  g_slice_free (AttributeValuePair, data);
}

static void
mm_hit_init (MMHit *h)
{
  MMHitPrivate *details = h->details = MM_HIT_GET_PRIVATE (h);

  details->attributes = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, pair_free);
}

static void
mm_hit_class_init (MMHitClass *klass)
{
  GObjectClass *oclass = G_OBJECT_CLASS (klass);

  oclass->finalize = mm_hit_finalize;

  g_type_class_add_private (klass, sizeof (MMHitPrivate));
}

static void
build_all_table_foreach (gpointer k,
                         gpointer v,
                         gpointer ud)
{
  GHashTable *table = ud;
  AttributeValuePair *pair = v;

  g_hash_table_insert (table, pair->attr, pair->val);
}

/* public methods */

/**
 * mm_hit_set_value:
 * @hit: a #MMHit.
 * @attribute: a #MMAttribute.
 * @value: a #GValue.
 *
 * Sets @value as value for @attribute inside the hit.
 */

void
mm_hit_set_value (MMHit *hit,
                  MMAttribute *attribute,
                  GValue *value)
{
  const char *attribute_id;
  AttributeValuePair *pair;

  g_return_if_fail (MM_IS_HIT (hit));

  if (G_VALUE_TYPE (value) != mm_attribute_get_value_type (attribute)) {
    g_warning ("Setting the wrong value type for attribute %s",
               mm_attribute_get_name (attribute));
    return;
  }

  pair = g_slice_new0 (AttributeValuePair);
  pair->attr = attribute;
  pair->val = mm_create_gvalue_for_attribute (attribute);
  g_value_copy (value, pair->val);

  attribute_id = mm_attribute_get_id (attribute);
  g_hash_table_insert (hit->details->attributes,
                       (char *) attribute_id,
                       pair);
}

/**
 * mm_hit_set_values:
 * @hit: a #MMHit.
 * @attributes: a list of #MMAttribute<!-- -->s.
 * @values: a list of #GValue<!-- -->s.
 *
 * Set on the hit the values specified in @values for the attributes specified
 * in @attributes. The two lists should be of identical order and length.
 */

void
mm_hit_set_values (MMHit *hit,
                   GList *attributes,
                   GList *values)
{
  GList *l, *m;

  g_return_if_fail (MM_IS_HIT (hit));
  g_return_if_fail (g_list_length (attributes) == g_list_length (values));
  
  for (l = attributes, m = values; l && m; l = l->next, m = m->next) {
    mm_hit_set_value (hit, l->data, m->data);
  }
}

/**
 * mm_hit_get_values:
 * @hit: a #MMHit.
 * @ids: a space-separated list of attribute ids.
 *
 * Similar to #mm_hit_get_all_values<!-- -->, but gets the values only
 * for the attributes specified by @ids. The lookup is done in a "best effort"
 * way, i.e. if an attribute id doesn't have a value in @hit, it won't be
 * there in the returned #GHashTable.
 *
 * Return value: a #GHashTable. Use #g_hash_table_destroy when done with it.
 */

GHashTable *
mm_hit_get_values (MMHit      *hit,
                   const char *ids)
{
  GHashTable *table;
  gchar **splitted_ids;
  int idx;
  AttributeValuePair *pair;

  g_return_val_if_fail (MM_IS_HIT (hit), NULL);
  table = g_hash_table_new (g_direct_hash, g_direct_equal);

  splitted_ids = g_strsplit (ids, " ", 0);
  for (idx = 0; idx < G_N_ELEMENTS (splitted_ids); idx++) {
    pair = g_hash_table_lookup (hit->details->attributes,
                                splitted_ids[idx]);
    if (!pair) {
      /* we can't find the relevant attribute, keep looking for the
       * others.
       */
      continue;
    }
    g_hash_table_insert (table, pair->attr, pair->val);
  }

  g_strfreev (splitted_ids);

  return table;
}

/**
 * mm_hit_get_all_values:
 * @hit: a #MMHit.
 *
 * Gets all the attributes and the relevant values set to the hit, organized
 * in a #GHashTable, where the keys are #MMAttribute structures and the values
 * are #GValue<!-- -->s. This works, as #MMAttribute structures are
 * always static.
 *
 * Return value: a #GHashTable. Use #g_hash_table_destroy when done with it.
 */

GHashTable *
mm_hit_get_all_values (MMHit *hit)
{
  GHashTable *table;

  g_return_val_if_fail (MM_IS_HIT (hit), NULL);

  table = g_hash_table_new (g_direct_hash, g_direct_equal);

  g_hash_table_foreach (hit->details->attributes,
                        build_all_table_foreach,
                        table);
  return table;
}
