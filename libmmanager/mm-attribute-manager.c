/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include "mm-attribute-manager.h"

#include <glib.h>
#include <glib-object.h>

static void
mm_attribute_manager_base_init (gpointer g_class)
{

}

GType
mm_attribute_manager_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (type == 0)) {
    const GTypeInfo our_info = {
      sizeof (MMAttributeManagerIface),
      mm_attribute_manager_base_init,
      NULL,
    };

    type = g_type_register_static (G_TYPE_INTERFACE,
                                   "MMAttributeManager",
                                   &our_info, (GTypeFlags) 0);
	}

  return type;
}

/* public methods */

/**
 * mm_attribute_manager_get_supported_attributes:
 * @attr_manager: a #MMAttributeManager.
 *
 * Gets a list of all the #MMAttribute<!-- -->s supported by @attr_manager.
 *
 * Return value: a #GList containing all the #MMAttribute<!-- -->s supported by
 * @attr_manager. Use #g_list_free when done with it.
 */

GList *
mm_attribute_manager_get_supported_attributes (MMAttributeManager *attr_manager)
{
  MMAttributeManagerIface *iface = MM_ATTRIBUTE_MANAGER_GET_IFACE (attr_manager);

  return iface->get_supported_attributes (attr_manager);
}

/**
 * mm_attribute_manager_lookup_attribute:
 * @attr_manager: a #MMAttributeManager.
 * @attr_id: a #MMAttribute unique identifier.
 *
 * Looks up the #MMAttribute identified by @id among those supported by
 * @attr_manager.
 *
 * Return value: a #MMAttribute if the attribute is found, %NULL else. You
 * must not modify or free the #MMAttribute got this way.
 */

MMAttribute *
mm_attribute_manager_lookup_attribute (MMAttributeManager *attr_manager,
                                       const char *attr_id)
{
  MMAttributeManagerIface *iface = MM_ATTRIBUTE_MANAGER_GET_IFACE (attr_manager);

  return iface->lookup_attribute (attr_manager, attr_id);
}
