/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib-object.h>
#include "mm-utils.h"

/**
 * mm_create_gvalue_for_attribute:
 * @attribute: a #MMAttribute
 *
 * Creates an initialized zero-filled #GValue initialized with the attribute's
 * #GType.
 *
 * Return value: an initialized zero-filled #GValue. Use #g_value_unset when
 * done.
 */

GValue *
mm_create_gvalue_for_attribute (MMAttribute *attribute)
{
  GValue *val;
  GType type;

  val = g_new0 (GValue, 1);
  type = mm_attribute_get_value_type (attribute);
  val = g_value_init (val, type);

  return val;
}
