/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_HIT_H__
#define __MM_HIT_H__
 
#include <glib.h>

#include "mm-attribute.h"

#define MM_TYPE_HIT            (mm_hit_get_type())
#define MM_HIT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
                                MM_TYPE_HIT, MMHit))
#define MM_HIT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),\
                                MM_TYPE_HIT, MMHitClass))
#define MM_IS_HIT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
                                MM_TYPE_HIT))
#define MM_IS_HIT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),\
                                MM_TYPE_HIT))
#define MM_HIT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),\
                                MM_TYPE_HIT, MMHitClass))

/**
 * SECTION:mm-hit
 * @short_description: a hit (i.e. a result of a query).
 *
 * #MMHit is a single result of a query. It's worth to note that #MMHit objects
 * are created by the application, so they could have some blank attribute, if
 * the application doesn't support it. More generally, a #MMHit should have all
 * the basic attributes and all those supported by its media type set, but it's 
 * up to the applications to fill them in.
 */
 
/**
 * MMHit:
 *
 * An object identifying a single result of a query.
 */

typedef struct _MMHit        MMHit;
typedef struct _MMHitPrivate MMHitPrivate;
typedef struct _MMHitClass   MMHitClass;

struct _MMHit {
  GObject parent_object;
  MMHitPrivate *details;
};

struct _MMHitClass {
  GObjectClass parent_class;
};

/* public methods */

GType        mm_hit_get_type       (void);

GHashTable * mm_hit_get_values     (MMHit       *hit,
                                    const char  *ids);
GHashTable * mm_hit_get_all_values (MMHit       *hit);

/* this should be private to module implementations */
void         mm_hit_set_values     (MMHit       *hit,
                                    GList       *attributes,
                                    GList       *values);
void         mm_hit_set_value      (MMHit       *hit,
                                    MMAttribute *attribute,
                                    GValue      *value);
#endif /* __MM_HIT_H__ */
