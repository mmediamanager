/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __MM_BASE_ATTRIBUTES_H__
#define __MM_BASE_ATTRIBUTES_H__

/**
 * SECTION:mm-base-attributes
 * @short_description: base attributes.
 *
 * Definitions for base attributes, to be used by #MMAttributeBaseManager.
 */

/**
 * MM_ATTRIBUTE_BASE_URI:
 *
 * A key in the "base" namespace corresponding to the URI attribute.
 */

#define MM_ATTRIBUTE_BASE_URI  "base::uri"

/**
 * MM_ATTRIBUTE_BASE_NAME:
 *
 * A key in the "base" namespace corresponding to the name attribute.
 */

#define MM_ATTRIBUTE_BASE_NAME "base::name"

#endif /* __MM_BASE_ATTRIBUTES_H__ */
