/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MM_ERROR_H__
#define __MM_ERROR_H__

#include <glib.h>

/**
 * SECTION:mm-error
 * @short_description: error codes.
 *
 * Enumerations for error codes to be used in libmmanager #GError<!-- -->s.
 */

/**
 * MM_XML_ERROR_QUARK:
 *
 * The #GQuark used for #MMXmlErrorEnum errors.
 */

#define MM_XML_ERROR_QUARK  g_quark_from_static_string ("MMXmlError")

/**
 * MM_DBUS_ERROR_QUARK:
 *
 * The #GQuark used for #MMDBusErrorEnum errors.
 */

#define MM_DBUS_ERROR_QUARK g_quark_from_static_string ("MMDBusError")

/**
 * MM_SO_ERROR_QUARK:
 *
 * The #GQuark used for #MMSoErrorEnum errors.
 */

#define MM_SO_ERROR_QUARK   g_quark_from_static_string ("MMSoError")

/**
 * MMXmlErrorEnum:
 * @MM_XML_ERROR_UNSERIALIZE_FAILED: An unspecified error occurred.
 * @MM_XML_ERROR_UNEXPECTED_EOF: The XML buffer ended unexpectedly.
 * @MM_XML_ERROR_UNEXPECTED_NODE: A node different from the expected one
 * was found while parsing the XML buffer.
 * @MM_XML_ERROR_UNKNOWN_GTYPE: Failed to go from XML to #GType because the
 * type seems to be unknown.
 *
 * Error codes for libmmanager XML operations.
 */

typedef enum {
  MM_XML_ERROR_UNSERIALIZE_FAILED,
  MM_XML_ERROR_UNEXPECTED_EOF,
  MM_XML_ERROR_UNEXPECTED_NODE,
  MM_XML_ERROR_UNKNOWN_GTYPE
} MMXmlErrorEnum;

/**
 * MMDBusErrorEnum:
 * @MM_DBUS_ERROR_FAILED: An unspecified error occurred.
 * @MM_DBUS_ERROR_BUS_UNAVAILABLE: Could not connect to the session bus.
 * @MM_DBUS_ERROR_NULL_ATTRIBUTE: Failed validating the arguments.
 * @MM_DBUS_ERROR_REMOTE_METHOD_FAILED: A remote method failed to execute.
 * @MM_DBUS_ERROR_SERIALIZE_FAIL: An error occurred while serializing an
 * object.
 * @MM_DBUS_ERROR_UNSERIALIZE_FAIL: An error occurred while unserializing an
 * object.
 *
 * Error codes for libmmanager DBus operations.
 */

typedef enum {
  MM_DBUS_ERROR_FAILED,
  MM_DBUS_ERROR_BUS_UNAVAILABLE,
  MM_DBUS_ERROR_NULL_ATTRIBUTE,
  MM_DBUS_ERROR_REMOTE_METHOD_FAILED,
  MM_DBUS_ERROR_SERIALIZE_FAIL,
  MM_DBUS_ERROR_UNSERIALIZE_FAIL
} MMDBusErrorEnum;

/**
 * MMSoErrorEnum:
 * @MM_SO_ERROR_FAILED: An unspecified error occurred.
 * @MM_SO_ERROR_NULL_PROVIDER: Error while validating a provider.
 *
 * Error codes for libmmanager Shared Object operations.
 */

typedef enum {
  MM_SO_ERROR_FAILED,
  MM_SO_ERROR_NULL_PROVIDER
} MMSoErrorEnum;

#endif /* __MM_ERROR_H__ */
