/* MManager - a Desktop wide manager for multimedia applications.
 *
 * Copyright (C) 2008 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#include "mm-attribute.h"

#include <glib.h>

struct _MMAttribute {
  char *id;
  char *name;
  char *description;
  GType type;
};

/* public methods */

/**
 * mm_attribute_get_name:
 * @attr: a #MMAttribute.
 *
 * Gets the name of @attr.
 *
 * Return value: a string containing the name of @attr.
 */

const char *
mm_attribute_get_name (MMAttribute *attr)
{
  return attr->name;
}

/**
 * mm_attribute_get_description:
 * @attr: a #MMAttribute.
 *
 * Gets a textual description of @attr.
 *
 * Return value: a string containing a description of @attr.
 */

const char *
mm_attribute_get_description (MMAttribute *attr)
{
  return attr->description;
}

/**
 * mm_attribute_get_value_type:
 * @attr: a #MMAttribute.
 *
 * Gets the #GType that a possible value for @attr should have.
 *
 * Return value: a #GType.
 */

GType
mm_attribute_get_value_type (MMAttribute *attr)
{
  return attr->type;
}

/**
 * mm_attribute_get_id:
 * @attr: a #MMAttribute.
 *
 * Gets the unique identifier of @attr.
 *
 * Return value: a string containing the unique identifier of @attr.
 */

const char *
mm_attribute_get_id (MMAttribute *attr)
{
  return attr->id;
}

/**
 * mm_attribute_free:
 * @attr: a #MMAttribute.
 *
 * Release the memory occupied by a #MMAttribute.
 */

void
mm_attribute_free (MMAttribute *attr)
{
  g_free (attr->name);
  g_free (attr->description);
  g_free (attr->id);

  g_slice_free (MMAttribute, attr);
}

/**
 * mm_attribute_new:
 * @type: the #GType that a possible value for this attribute should have.
 * @id: a string containing an unique id for the attribute.
 * @name: a string containing the attribute's name.
 * @description: a string containing the attribute's description.
 *
 * Builds a new #MMAttribute structure, with the specified properties. This is
 * useful only if you're implementing a #MMAttributeManager, as the attributes
 * are supposed to be static and owned by the manager.
 *
 * Return value: a newly allocated #MMAttribute structure. Free it with
 * #mm_attribute_free.
 */

MMAttribute *
mm_attribute_new (GType type,
                  const char *id,
                  const char *name,
                  const char *description)
{
  MMAttribute *a;

  a = g_slice_new0 (MMAttribute);
  a->id = g_strdup (id);
  a->name = g_strdup (name);
  a->description = g_strdup (description);
  a->type = type;

  return a;
}
